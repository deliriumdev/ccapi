<?php
require_once(($appDir = realpath(__DIR__ . '/..')) . '/app.php');
require_once($appDir . '/bootstrap.php');
$db = $app->getServices()->get('Pdo');
$monday = (1 === date('w')) ? time() : strtotime('previous monday');
// the earliest a menu could of been entered would be two weeks from prev or current monday, so give it 3 for a little of a buffer
$cutoff = strtotime('-3 weeks', $monday);
$cutoffDate = date('Y-m-d', $cutoff) . ' 00:00:00';
$data = $db->get_stack($sql = 'select id from wp_e650237bc8_rg_lead where form_id = 7 and date_created < ' . $db->quote($cutoffDate) . ' order by id asc');
var_dump($sql);
if (empty($data)) {
    var_dump($data);
    echo "No data found\n";
    exit;
}
printf("Starting to purge %d entries\n", count($data));
foreach ($data as $leadId) {
    //var_dump($leadId);
    //var_dump($db->get_column($sql = 'select count(*) FROM wp_e650237bc8_rg_lead_detail_long WHERE lead_detail_id IN(SELECT id FROM wp_e650237bc8_rg_lead_detail WHERE lead_id = ' . $leadId . ')'));
    $db->execute('DELETE FROM wp_e650237bc8_rg_lead_detail_long WHERE lead_detail_id IN(SELECT id FROM wp_e650237bc8_rg_lead_detail WHERE lead_id = ' . $leadId . ')');
   // var_dump($db->get_column($sql)); 
    //var_dump($db->get_column($sql = 'select count(*) FROM wp_e650237bc8_rg_lead_detail WHERE lead_id = ' . $leadId));
    $db->execute('DELETE FROM wp_e650237bc8_rg_lead_detail WHERE lead_id = ' . $leadId);
   // var_dump($db->get_column($sql));
  //  var_dump($db->get_column($sql = 'select count(*) FROM wp_e650237bc8_rg_lead_notes WHERE lead_id = ' . $leadId));
    $db->execute('DELETE FROM wp_e650237bc8_rg_lead_notes WHERE lead_id = ' . $leadId);
   // var_dump($db->get_column($sql));
   // var_dump($db->get_column($sql = 'select count(*) from wp_e650237bc8_rg_lead_meta WHERE lead_id = ' . $leadId));
    $db->execute('DELETE FROM wp_e650237bc8_rg_lead_meta WHERE lead_id = ' . $leadId);
   // var_dump($db->get_column($sql));
    $db->execute('delete from wp_e650237bc8_rg_lead where id = ' . $leadId);
}
echo "Done with purge, checking tables\n";
$gfTables = ['wp_e650237bc8_rg_lead', 'wp_e650237bc8_rg_lead_detail', 'wp_e650237bc8_rg_lead_detail_long'];
foreach ($gfTables as $table) {
    $overhead = $db->get_column($sql = "SELECT SUM(DATA_FREE) FROM INFORMATION_SCHEMA.PARTITIONS WHERE TABLE_SCHEMA = 'cclive-wp-KStzA0E6' AND TABLE_NAME = '$table'");
    printf("Table [%s] overhead: %s\n", $table, $overhead);
    if ('0' !== $overhead && '' !== $overhead) {
        // @note internally this will use exec which will result in a 2014 error on successive calls, so pass params to force internal use of query instead
        $db->execute('optimize table :table', [':table' => $table]);
        var_dump($db->err);
        print "Table optimized\n";
    }
    else {
        print "Skipping\n";
    }
}
echo "Done!\n";
