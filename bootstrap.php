<?php
error_reporting(E_ALL);
ini_set('display_errors', 'off');

date_default_timezone_set('America/Chicago');//for carbon
use Reo\Routing\Route;
use Gator\Pdo;
use Gator\Paginator;
use CampusCooks\Api;
$libPaths = array('lib' => __DIR__ . '/vendor', 'pear' => '/usr/share/php');//'vendor' => __DIR__ . '/lib/vendor', 
//require_once($libPaths['vendor'] . '/Reo/Autoload/Autoloader.php');
require_once($libPaths['lib'] . '/Reo/Autoload/Autoloader.php');
$loader = new Reo\Autoload\Autoloader($libPaths);

    $app = new CampusCooksApp($loader);
    $services = $app->getServices();
    //$services->get('Session');
    $configArray = array(
        'database' => array(
            'driver'    => 'mysql',
            'host'      => 'localhost',
            'database'  => 'cclive-wp-KStzA0E6',
            'username'  => 'vZQhNvFt4qoG',
            'password'  => '8zOXMBAozj2h7Dnx',
            'charset'   => 'utf8',
            'collation' => 'utf8',
            'prefix'    => 'wp_e650237bc8_',
        ),
        'logger' => array(
            'logdir' => __DIR__ . '/logs',
            'logfile' => __DIR__ . '/logs/api.log',
            'maillog' => __DIR__ . '/logs/api-mail.log'
        ),
        'api' => array(
            'key' => '5ce98515ff',
            'private_key' => '6498251fac801fc',
            'hmac_key' => '490d56eb9362ac75be477fe647ac306f64f7ebab50624b23cc916a6ff2362f22', // for verifying delete requests
            'start' => '2016-07-31',
            'report_hours' => ['lunch' => 10, 'dinner' => 16, 'breakfast' => 8],
            'daily_report_hour' => 20,
            'default_tz' => 'America/Chicago',
            'mail_template_dir' => __DIR__ . '/cron/template',
            'roles' => ['chefs' => 2, 'cod' => 8, 'campuscookssupervisors' => 4, 'trainer' => 16, 'cc_admin' => 32, 'administrator' => 64],
            'report_roles' => [2 => 'chefs', 8 => 'cod', 16 => 'trainer', 4 => 'campuscookssupervisors'], //@note bitwise keys for multiple roles per user
            'deadline_msg' => 'The deadline has passed for submitting %s. If you need help, please talk to your chef directly.', //a late plate
            'count_per_page' => 10, // default pagnation results per page
            'mobile_version' => '1.5.6',
            'upgrade_message' => 'Please upgrade to the latest version',
        ),
		/*
        'mailer' => [
            'transport' => 'localhost',
            'server'    => 'secure.emailsrvr.com',
            'user'      => 'app@campuscooks.com',
            'pass'      => 'Delirium101!',
            'fromEmail' => 'app@campuscooks.com',
            'fromName'  => 'CC App Reports',
        ],
		
		'mailer' => [
            'transport' => 'localhost',
            'server'    => 'smtp.office365.com',
            'user'      => 'mycampuscooks@campuscooks.com',
            'pass'      => 'Livethealpha101',
            'fromEmail' => 'mycampuscooks@campuscooks.com',
            'fromName'  => 'CC App Reports',
        ],
		*/
		'mailer' => [
            'transport' => 'localhost',
            'server'    => 'smtp.sendgrid.net',
            'user'      => 'apikey',
            'pass'      => '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',
            'fromEmail' => 'mycampuscooks@campuscooks.com',
            'fromName'  => 'CC App Reports',
        ],
    );

    $services->get('Config', $configArray);
    //@depricated moved to api
    //$services->subscribe(['route.nomatch' => ['NoMatch']]);//['route.before' => ['Routing']'route.matched' => ['RouteMatched', false, 1]
    $app->registerPackage('Pdo', 'Gator', __DIR__ . '/packages/Gator', function($c){
        $config = $c->get('Config')->database->toArray();
        return new Pdo(['db_host' => $config['host'], 'db_name' => $config['database'], 'db_user' => $config['username'], 'db_pass' => $config['password']]);
    });
    $app->registerPackage('Paginator', 'Gator', __DIR__ . '/packages/Gator', function($c){
        return new Paginator($c->get('Config')->api->count_per_page);
    });
    $app->registerPackage('Api', 'CampusCooks', __DIR__ . '/packages/CampusCooks', function($c){
        return new Api($c);
    }, true, ['route.before' => 'registerRoutes', 'route.nomatch' => 'handleBadRoute']);
return;
/**
 * Add routes
 * 
 * @depricated for letting the API register it's own routes
 */ 
    $services->register('Routing', function($c){
        $routeCollection = $c->get('RouteCollection');
        //@note this could be an anonymous function but the Api will always load so there is no Closure wrap here
        $routeCollection->add('submissions', new Route('/forms/[form=*s]', [$c->get('Api'), 'handleFormSubmission'], ['method' => 'POST', 'protocol' => 'https'])); 
        // $routeCollection->add('root', new Route('/', [$c->get('Api'), 'handleFormSubmission'], ['protocol' => 'https']));
        // $routeCollection->add('entries', new Route('/entries', function(){echo "Trigger Entry Handler\n";}));
    }, false);
    $services->register('NoMatch', function($c){
        $c->get('Api')->handleBadRoute();
    });

/**
 * Authenticate route if neccesary
 */ 
    $services->register('RouteMatched', function($c, $id, $event){
        //var_dump($event->get('name'));
        return false;
    }, false);
