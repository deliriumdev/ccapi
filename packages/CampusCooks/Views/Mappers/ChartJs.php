<?php
/**
 * Campus Cooks API
 */
namespace CampusCooks\Views\Mappers;

class ChartJs
{
    protected static $colors = ['#20a8d8', '#4dbd74', '#63c2de', '#f8cb00', '#f86c6b'];
    protected static $borderWidth = 2;
    protected static $backgroundColor = 'transparent';
    
    public static function map($data)
    {
        $xx = -1;
        foreach ($data as $key => $item) {
            $xx ++;
            if (!isset(self::$colors[$xx])) {
                $xx = 0;
            }
            $data[$key] = [
                'id'    => $item['budgetID'],
                'label' => $item['house']['name'],
                'total' => array_sum($item['amounts']),
                'backgroundColor' => self::$backgroundColor,
                'borderColor' => self::$colors[$xx],
                'pointHoverBackgroundColor' => '#fff',
                'borderWidth' => self::$borderWidth,
                'data' => $item['amounts'],
            ];
        }
        return $data;
    }

    public static function mapWeekOf($data)
    {
        $xx = -1;
        foreach ($data as $key => $item) {
            $xx ++;
            if (!isset(self::$colors[$xx])) {
                $xx = 0;
            }
            $totals = $weeks = [];
            foreach ($item as $row) {
                $label = $row['house'];// this will not change
                $id = $row['id'];
                $totals[] = $row['total'];
                $weeks[] = date('M d Y', strtotime($row['weekOf']));
            }
            $data[$key] = [
                'id'    => $id,
                'label' => $label,
                'total' => array_sum($totals),
                'backgroundColor' => self::$backgroundColor,
                'borderColor' => self::$colors[$xx],
                'pointHoverBackgroundColor' => '#fff',
                'borderWidth' => self::$borderWidth,
                'data' => $totals,
                'weeks' => $weeks,
            ];
        }
        return $data;
    }

    public static function mapPie($data)
    {
        $xx = -1;
        $mapped = ['datasets' => [['data' => [], 'backgroundColor' => [], 'label' => 'Weekly Spend']], 'labels' => [], 'total' => 0];
        foreach ($data as $item) {
            $xx ++;
            if (!isset(self::$colors[$xx])) {
                $xx = 0;
            }
            $mapped['datasets'][0]['data'][] = $item['total'];
            $mapped['datasets'][0]['backgroundColor'][] = self::$colors[$xx];
            $mapped['labels'][] = $item['vendor'];
            $mapped['total'] += $item['total'];
        }
        return $mapped;
    }

    public static function mapAvgLateplate($data)
    {
        $xx = -1;
        foreach ($data as $key => $item) {
            $xx ++;
            if (!isset(self::$colors[$xx])) {
                $xx = 0;
            }
            $data[$key] = [
                'label' => $item['house'],
                'total' => array_sum($item['data']),
                'backgroundColor' => self::$backgroundColor,
                'borderColor' => self::$colors[$xx],
                'pointHoverBackgroundColor' => '#fff',
                'borderWidth' => self::$borderWidth,
                'data' => $item['data'],
            ];
        }
        return $data;
    }

    public static function mapAttending($data)
    {
        $xx = -1;
        foreach ($data as $key => $item) {
            $xx ++;
            if (!isset(self::$colors[$xx])) {
                $xx = 0;
            }
            $house = $item['house'];
            $data[$key] = [
                'labels' => $item['meals'],
                'total' => array_sum($item['data']), // use this for the overall average for all semester, will be divided by number of weeks
                'backgroundColor' => self::$backgroundColor,
                'borderColor' => self::$colors[$xx],
                'pointHoverBackgroundColor' => '#fff',
                'borderWidth' => self::$borderWidth,
                'data' => isset($item['avg']) ? $item['avg'] : $item['data'], // put the daily average here for all semester
                'name' => (1 === $item['mealTime'] ? 'Lunch' : (3 === $item['mealTime'] ? 'Breakfast' : 'Dinner' )) . ' ' . $item['house'],
            ];
            if (isset($item['avg'])) { // avg for all semester
                $data[$key]['avgTotal'] = array_sum($item['avg']);
            }
        }
        return $data;
    }
}
