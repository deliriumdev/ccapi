<?php
/**
 * Campus Cooks API
 */
namespace CampusCooks\Views\Mappers;

class BudgetTable
{
    public function map($data, $type)
    {
        $mapped = [];
        if (3 == $type || 4 == $type) {
            foreach ($data as $item) {
                $mapped[] = [
                    $item['campus_name'],
                    $item['house_name'],
                    $item['weekOf'],
                    $item['invDate'],
                    $item['invoice_number'],
                    $item['vendor_name'],
                    $item['other_name'],
                    $item['amount'],
                    $item['eventType'],
                    $item['approvedBy'],
                    $item['card_name'],
                    $item['card_type'],
                ];
            }
        }
        else {
            foreach ($data as $item) {
                $mapped[] = [
                    $item['campus_name'],
                    $item['house_name'],
                    $item['weekOf'],
                    $item['invDate'],
                    $item['invoice_number'],
                    $item['vendor_name'],
                    $item['other_name'],
                    $item['amount'],
                    $item['card_name'],
                    $item['card_type'],
                ];
            }
        }
        return $mapped;
    }
}
