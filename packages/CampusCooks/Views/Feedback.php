<?php
/**
 * Campus Cooks API
 */
namespace CampusCooks\Views;

class Feedback
{
    protected $data;
    protected $options = [];

    public function __construct($data, array $options = null)
    {
        $this->data = $data;
        if (isset($options)) {
            $this->options = $options;
        }
    }

    public function getContent()
    {
        return $this->getTable();
    }

    public function getTable()
    {
        if ($this->data->isEmpty()) {
            return '<div class="alert alert-info" style="margin-top:15px">No Feedback Found</div>';
        }
        ob_start();
        ?>
<table class="table table-hover dataTable search-result" style="background-color:#f9f9f9">
  <thead>
    <tr>
      <th>Date</th>
      <th>Student</th>
      <th>House / Campus</th>
      <th>Comments</th>
    </tr>
  </thead>
  <tbody>
<?php	
	foreach ($this->data as $item) { //var_export($item);break;?>
    <tr>
      <th>
        <?php echo $item['feedback_date'];?>
      </th>
      <th style="white-space:nowrap">
        <?php echo $item['first_name'] . ' ' . $item['last_name'];?>
      </th>
      <th>
        <?php echo $item['house_name'];?>
      </th>
      <th style="font-weight:normal">
        <?php echo $item['comments'];?>
      </th>
    </tr>
<?php }?>
  </tbody>
</table>
<?php
        return ob_get_clean();
    }
}
