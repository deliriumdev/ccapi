<?php
/**
 * Campus Cooks API
 */
namespace CampusCooks\Views;

class Menu
{
    protected $data;
    protected $options = [];

    public function __construct($data, array $options = null)
    {
        $this->data = $data;
        if (isset($options)) {
            $this->options = $options;
        }
        
    }

    public function getContent()
    {
        if (empty($this->data)) {
            return '<div class="alert alert-info" style="margin-top:15px">No Menu Items Found</div>';
        }
        ob_start();
        foreach ($this->data as $item) {
            $this->renderItem($item);
        }
        $content = ob_get_clean();
        return  '<div class="search-result row" style="margin-top:15px">' . $content . '</div>';
    }

    public function renderItem($data)
    {?>
    <div class="col-md-2" style="border-right: 1px solid #ccc;">
        <ul class="list-group">
            <li class="list-group-item" style="border:none;padding:10px 5px 5px"><i class="fa fa-calendar"></i> <span><?php echo $data['schedule_date']?></span></li>
            <li class="list-group-item" style="border:none;padding:5px"><i class="fa fa-clock-o"></i> <span><?php echo(1 == $data['meal_time'] ? 'Lunch' : (3 == $data['meal_time'] ? 'Breakfast' : 'Dinner'));?></span></li>
           <!-- <li><i class="fa fa-tags"></i> <span>People</span></li> -->
        </ul>
    </div>
    <div class="col-md-10">
        <h4><?php echo $data['meal_name'];?></h4>
        <?php $sides = array_map(function($item){
            return $item->name;
        }, $data['menuItems']->getSides());
        ?>
        <p><?php echo('' !== ($entree = $data['menuItems']->getEntree()) ? $entree->name : 'n/a');?>, <?php echo(implode(', ', $sides));
        // if('0000-00-00' === $data['schedule_date']){var_dump($data);}
?> 
<?php if ('' !== ($dessert = $data['menuItems']->getDessert())) {?>
        , <span style="font-weight:bold">
            **<?php echo $dessert->name;?>**
        </span>
<?php }?>
        </p>
<? if (!empty($data['house'])) {?>
    <p> —  <?php echo $data['house']['name'];?></p>
<?php }?>
    </div>
    <div class="clearfix"></div>
<?php
    }

    public function getFullWeekContent()
    {
        if (empty($this->data)) {
            return '<div class="alert alert-info" style="margin-top:15px">No Menus Found</div>';
        }
        $url = '/menu-creation-area/menu-entry-menu-preview/';
        ob_start();
        ?>
<table class="table table-hover dataTable search-result" style="background-color:#f9f9f9">
  <thead>
    <tr>
      <th>Week Of</th>
      <th>Campus</th>
      <th>House</th>
    </tr>
  </thead>
  <tbody>
<?php	
	foreach ($this->data as $item) {?>
    <tr>
      <th>
        <a data-fancybox-type="iframe" class="chef-menu-archive-link" href="<?php echo $url;?>?week_set=<?php echo $item['weekDate'];?>&archive=1&user_house=<?php echo(urlencode($item['house']['name']));?>&house_id=<?php echo $item['house']['id'];?>"><?php echo $item['weekDate'];?></a>
      </th>
      <th>
        <?php echo $item['campus']['name'];?>
      </th>
      <th>
        <?php echo $item['house']['name'];?>
      </th>
    </tr>
<?php }?>
  </tbody>
</table>
<?php
        return ob_get_clean();
    }
}
