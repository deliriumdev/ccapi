<?php
/**
 * Campus Cooks API
 */
namespace CampusCooks\Views;

class Forum
{
    protected $data;
    protected $options = [];

    public function __construct($data, array $options = null)
    {
        $this->data = $data;
        if (isset($options)) {
            $this->options = $options;
        }
    }

    public function getContent()
    {
        return $this->getForum();
    }

    public function getForum()
    {
        if ($this->data->isEmpty()) {
            return '<div class="alert alert-info" style="margin-top:15px">No Forum Messages Found</div>';
        }
        ob_start();
        ?>
<style>
.forum.table > tbody > tr > td {
    vertical-align: middle;
}
.forum .fa {
    width: 1em;
    text-align: center;
}
.forum.table th.cell-stat {
    width: 5%;
}
.forum.table th.cell-stat-2x {
    width: 20%;
    white-space:nowrap;
}
.chat {
    width: 90%;
}

.bubble{
    background-color: #F2F2F2;
    border-radius: 5px;
    box-shadow: 0 0 6px #B2B2B2;
    display: inline-block;
    padding: 10px 18px;
    position: relative;
    vertical-align: top;
}

.bubble::before {
    background-color: #F2F2F2;
    content: "\00a0";
    display: block;
    height: 16px;
    position: absolute;
    top: 11px;
    transform:             rotate( 29deg ) skew( -35deg );
        -moz-transform:    rotate( 29deg ) skew( -35deg );
        -ms-transform:     rotate( 29deg ) skew( -35deg );
        -o-transform:      rotate( 29deg ) skew( -35deg );
        -webkit-transform: rotate( 29deg ) skew( -35deg );
    width:  20px;
}

.me {
    float: left;   
    margin: 5px 45px 5px 20px;         
}

.me::before {
    box-shadow: -2px 2px 2px 0 rgba( 178, 178, 178, .4 );
    left: -9px;           
}

.you {
    float: right;    
    margin: 5px 20px 5px 45px;         
}

.you::before {
    box-shadow: 2px -2px 2px 0 rgba( 178, 178, 178, .4 );
    right: -9px;    
}
</style>
<table class="table forum table-striped">
    <thead>
      <tr>
        <th class="cell-stat"></th>
        <th>
          Subject
        </th>
        <th class="cell-stat text-center">Posts</th>
        <th class="cell-stat-2x">Last Post</th>
      </tr>
    </thead>
    <tbody>
<?php foreach ($this->data as $item) {?>
      <tr>
        <td class="text-center"><i class="fa fa-exclamation fa-2x text-danger"></i></td>
        <td>
          <h4><?php echo $item['subject'];?></h4>
        </td>
        <td class="text-center">
          <a class="show-thread" href="javascript:;"><span class="badge badge-info">3</span></a>
        
        </td>
        <td>
          by <?php echo $item['first_name'] . ' ' . $item['last_name'];?><br/>
          <small><i class="fa fa-bank"></i> <?php echo $item['house_name'];?></small><br/>
          <small><i class="fa fa-clock-o"></i> <?php echo $item['forum_date'];?></small>
        </td>
      </tr>
<?php if (isset($item['threads']) && !$item['threads']->isEmpty()) {?>
      <tr class="thread-view">
        <td colspan="3">
          <div class="chat">
<?php $xx = 0; foreach ($item['threads'] as $thread) {?>
            <div class="<?php echo($dir = ($xx % 2) ? 'me' : 'you');?>">
                <small><i class="fa fa-user"></i> <?php echo $thread['first_name'] . ' ' . $thread['last_name'];?></small><br/>
                <small><i class="fa fa-clock-o"></i> <?php echo $thread['forum_date'];?></small>
            </div>
            <div style="min-width:60%" class="bubble <?php echo($dir);?>">
              <?php echo $thread['comments'];?>
            </div>
<?php $xx++;}?>
          </div>
        </td>
      </tr>
<?php } // end if threads
}// end loop?>
    </tbody>
  </table>
<?php 
        return ob_get_clean();
    }

    public function getTable()
    {
        if ($this->data->isEmpty()) {
            return '<div class="alert alert-info" style="margin-top:15px">No Forum Messages Found</div>';
        }
        ob_start();
?>
<table class="table table-hover dataTable search-result" style="background-color:#f9f9f9">
  <tbody>
<?php	
	foreach ($this->data as $item) { //var_export($item);break;?>
    <tr>
      <th style="white-space:nowrap;text-align:center">
        <?php echo $item['forum_date'];?><br/>
        <?php echo $item['first_name'] . ' ' . $item['last_name'];?><br/>
        <?php echo $item['house_name'];?>
      </th>
      <th style="font-weight:normal">
        <?php echo $item['subject'];?>
      </th>
    </tr>
<?php }?>
  </tbody>
</table>
<?php
        return ob_get_clean();
    }
}
