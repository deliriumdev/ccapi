<?php
/**
 * Campus Cooks API
 */
namespace CampusCooks\Utils;

class WpImport
{
    protected $db;
    protected $wpDbPrefix;
/**
 * @param timezones
 * @note the timzone values in the wp_groups_group_capability table
 */ 
    protected static $timezones = [97 => 'EST', 98 => 'CST', 99 => 'MST', 100 => 'PST'];

    public function __construct($db, $wpDbPrefix = 'wp_')
    {
        $this->db = $db;
        $this->wpDbPrefix = $wpDbPrefix;
    }

/**
 * importCampus
 * 
 * import campus from groups to custom tables
 * 
 * @param campusId int the campus id (from wp groups)
 */ 
    public function importCampus($campusId, $group = null)
    {
        if (empty($group)) {
            $sql = <<<EOD
select grp.*, grc.capability_id as tz from {$this->wpDbPrefix}groups_group grp inner join 
{$this->wpDbPrefix}groups_group_capability grc on grc.group_id = grp.group_id and grc.capability_id in (97, 98, 99, 100) where grp.group_id = :id
EOD;
            $group = $this->db->get_row($sql, $params = [':id' => $campusId]);
            if (empty($group)) {
                //var_dump($sql, $this->db->err, $params);
                return false;
            }
        }
        $params[':name'] = $group['name'];
        $params[':tz'] = $group['tz'] = self::$timezones[$group['tz']];
        // get the tz
        $id = $this->db->execute('insert into cc_campus(campus_id, campus_name, tz) values (:id, :name, :tz)', $params);
        if (empty($id)) {
            return $group;
        }
        return $group;
    }

/**
 * importHouse
 * 
 * import house from groups to custom tables
 * 
 * @param houseId int the house id (from wp groups)
 */ 
    public function importHouse($houseId)
    {
        $name = $this->db->get_column('select house_name from cc_house where house_id=:id', [':id' => $houseId]);
        if (!empty($name)) {
            return $name;
        }
        $group = $this->db->get_row($sql = 'select * from ' . $this->wpDbPrefix . 'groups_group where ' . $this->wpDbPrefix . 'groups_group.group_id = :id', $params = [':id' => $houseId]);
        if (empty($group)) {
            //var_dump($sql, $this->db->err, $params);
            return false;
        }
        $params[':name'] = $group['name'];
        $params[':campus_id'] = $group['parent_id'];
        $id = $this->db->execute($sql = 'insert into cc_house(house_id, campus_id, house_name) values (:id, :campus_id, :name)', $params);
        if (empty($id)) {
            //var_dump($sql, $this->db->err, $params);
            return false;
        }
        return $group['name'];
    }

/**
 * importUser
 * 
 * import user from wp users to custom tables
 * 
 * @param userId int the userid
 * @note the mobile app passes the entry_id for the user creation which is stored in user meta
 * The CC API uses the real user id, converting the passed user id to the WP user ID.
 */ 
    public function importUser($userId, $campusId, $houseId, $roles = 1)
    {
        // sometimes this runs more than once on the same user
        $data = $this->db->get_row('select first_name, last_name, email, roles from cc_user where user_id = :id', [':id' => $userId]);
        if (!empty($data)) {
            if ($roles != $data['roles']) {
                $this->db->execute('update cc_user set roles = :roles where user_id = :id', [':id' => $userId, ':roles' => $roles]);
                $data['roles'] = $roles;
            }
            return $data;
        }
        $user = $this->db->get_row($sql = 'select * from ' . $this->wpDbPrefix . 'users where ID = :id', $params = [':id' => $userId]);
        if (empty($user)) {
            // var_dump($sql, $this->db->err, $params);
            return false;
        }
        $meta = $this->db->get_array($sql = 'select * from ' . $this->wpDbPrefix . 'usermeta where user_id = :id and (meta_key=\'first_name\' or meta_key=\'last_name\')', $params = [':id' => $userId], 'meta_key');
        if (empty($meta)) {
            //var_dump($sql, $this->db->err, $params);
            return false;
        }
        $params[':campus_id'] = $campusId;
        $params[':house_id'] = $houseId;
        $params[':first_name'] = $meta['first_name']['meta_value'];
        $params[':last_name'] = $meta['last_name']['meta_value'];
        $params[':email'] = $user['user_email'];
        $params[':roles'] = $roles;
        $id = $this->db->execute($sql = 'insert into cc_user(user_id, campus_id, house_id, first_name, last_name, email, roles) values (:id, :campus_id, :house_id, :first_name, :last_name, :email, :roles)', $params);
        if (empty($id)) {
            //var_dump($sql, $this->db->err, $params);
            return false;
        }
        return ['first_name' => $params[':first_name'], 'last_name' => $params[':last_name'], 'email' => $params[':email'], 'roles' => $roles];
    }

/**
 * getGroupsByTz
 * 
 * gets campus (parent) groups by timezone
 */ 
    function getGroupsByTz($tz, $campus)
    {
        if (false === ($tzId = array_search($tz, self::$timezones))) {
            return false;
        }
        // gets the campuses in a certain timezone
        $sql = <<<EOD
select grp.* from {$this->wpDbPrefix}groups_group grp inner join 
{$this->wpDbPrefix}groups_group_capability grc on grc.group_id = grp.group_id and grc.capability_id in (:id) 
where grp.parent_id is null
EOD;
        $groups = $this->db->get_array($sql, $params = [':id' => $tzId]);
        if (empty($groups)) {
            var_dump($sql, $this->db->err, $params);
            return false;
        }

        $imports = array_filter($groups, function($item) use(&$campus){
            return empty($campus[$item['group_id']]);
        });

        if (empty($imports)) {
            return false;
        }
        foreach ($imports as $group) {
            $this->importCampus($group['group_id'], $group = null);
        }
        return true;
    }

    function getHouseUsers($houseId, $campusId, $roles = null)
    {
        $sql = <<<EOD
select wu.*, um.meta_value as roles from (({$this->wpDbPrefix}users wu inner join {$this->wpDbPrefix}groups_user_group ug on ug.user_id = wu.ID) 
left join {$this->wpDbPrefix}usermeta um on um.user_id = wu.ID and um.meta_key='{$this->wpDbPrefix}capabilities') where ug.group_id=:id
EOD;
        $users = $this->db->get_array($sql, $params = [':id' => $houseId], 'ID');
        if (empty($users)) {
            return false;
        }
        if (empty($roles)) {
            return $users;
        }
        $users = array_filter($users, function($item) use(&$roles) {
            foreach ($roles as $userRole) {
                // no need to unserialize
                if (strstr($item['roles'], $userRole)) {
                    return true;
                    break;
                }
            }
            return false;
        });

        if (empty($users)) {
            return false;
        }
        foreach ($users as $key => $item) {
            $data = $this->db->get_row('select * from cc_user where user_id=:id', [':id' => $item['ID']]);
            // get the bitwise role
            $userRoles = 0;
            foreach ($roles as $key => $userRole) {
                if (strstr($item['roles'], $userRole)) {
                    $userRoles += $key;
                }
            }
            if (empty($data)) {
                // var_dump($sql, $item['ID'], $campusId, $houseId, $this->db->err);
                $this->importUser($item['ID'], $campusId, $houseId, $userRoles);
            }
            else {
                // echo "Checking Data\n";
                $updates = [];
                if ($data['roles'] != $userRoles) {
                    $updates[] = 'roles = :roles';
                    $params[':roles'] = $userRoles;
                }
                if ($data['campus_id'] != $campusId) {
                    $updates[] = 'campus_id = :campus_id';
                    $params[':campus_id'] = $campusId;
                }
                if ($data['house_id'] != $houseId) {
                    $updates[] = 'house_id = :house_id';
                    $params[':house_id'] = $houseId;
                }
                if (!empty($updates)) {
                    $params[':id'] = $item['ID'];
                    $this->db->execute($sql = 'update cc_user set '. implode(',', $updates) . ' where user_id=:id', $params);
                    // var_dump($sql, $params, $this->db->err);
                }
            }
        }
        return $users;
    }

    public function syncUsers(array $users, $roles)
    {
        $sql = <<<EOD
select wu.*, um.meta_value as roles, ug.group_id
from (({$this->wpDbPrefix}users wu inner join {$this->wpDbPrefix}groups_user_group ug on ug.user_id = wu.ID) 
left join {$this->wpDbPrefix}usermeta um on um.user_id = wu.ID and um.meta_key='{$this->wpDbPrefix}capabilities') where wu.ID=:id and ug.group_id !=1
EOD;
        foreach ($users as $key => $data) {
            $item = $this->db->get_row($sql, $params = [':id' => $data['user_id']]);
            if (empty($item)) {
                continue;
            }
            $house = $this->db->get_row('select * from ' . $this->wpDbPrefix . 'groups_group where group_id = :group_id', [':group_id' => $item['group_id']]);
            if (empty($house)) {
                continue;
            }
            $userRoles = 0;
            foreach ($roles as $key => $userRole) {
                if (strstr($item['roles'], $userRole)) {
                    $userRoles += $key;
                }
            }
            // echo "Checking Data\n";
            $updates = [];
            if ($data['roles'] != $userRoles) {
                $updates[] = 'roles = :roles';
                $params[':roles'] = $userRoles;
            }
            if ($data['campus_id'] != $house['parent_id']) {
                $updates[] = 'campus_id = :campus_id';
                $params[':campus_id'] = $house['parent_id'];
            }
            if ($data['house_id'] != $house['group_id']) {
                $updates[] = 'house_id = :house_id';
                $params[':house_id'] = $house['group_id'];
            }
            if (!empty($updates)) {
                $params[':id'] = $item['ID'];
                $this->db->execute($sql = 'update cc_user set '. implode(',', $updates) . ' where user_id=:id', $params);
                //var_dump($sql, $params, $this->db->err);
            }
        }
        return true;
    }
}
