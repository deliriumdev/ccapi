<?php
/**
 * Campus Cooks API
 */
namespace CampusCooks\Utils;

class WpData
{
    protected $db;
    protected $wpdbPrefix;
    
    public function __construct($db, $wpdbPrefix)
    {
        $this->db = $db;
        $this->wpdbPrefix = $wpdbPrefix;
    }

    public function getVendors()
    {
        $json = $this->db->get_column('select post_content from ' . $this->wpdbPrefix . 'posts where post_title=\'Vendors\' and post_type=\'tablepress_table\'');
        if (empty($json)) {
            return [];
        }
        // this defaults to aoa, so map, filter and get 0 starting index array
        return array_values(array_filter(array_map(function($item){return ['value' => ($val = trim($item[0])), 'text' => $val];}, json_decode($json, true)), function($item){return !empty($item['value']);}));
    }

    public function getCards()
    {
        $json = $this->db->get_column('select post_content from ' . $this->wpdbPrefix . 'posts where post_title=\'Credit Cards\' and post_type=\'tablepress_table\'');
        if (empty($json)) {
            return [];
        }
        return array_values(array_filter(array_map(function($item){return ['value' => ($val = trim($item[0])), 'text' => $val];}, json_decode($json, true)), function($item){return !empty($item['value']) && 0 !== strpos($item['value'], 'Type of');}));
    }
}
