<?php
/**
 * Campus Cooks API
 */
namespace CampusCooks\Utils;

class Security
{
    protected $hmacKey;

    public function __construct($hmacKey)
    {
        $this->hmacKey = $hmacKey;
    }

    public function validateSignature($signature, $content, $algo = 'sha1')
    {
        $hash = base64_encode($raw = hash_hmac($algo, $content, $this->hmacKey));
        return $hash === $signature || $raw === $signature;
    }
}
