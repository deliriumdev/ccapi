<?php
/**
 * Campus Cooks API
 */
namespace CampusCooks\Utils;

class DateTime
{
/**
 * @note for test this is stubbed to a date that contains data - remove in production
 */ 
    public static function getCurrentMonday($ymd = true, $all = false)
    {
        /*$current = '2017-05-01';
        $time = strtotime($current);*/
        if ('1' === date('w')) {
            $current = date('Y-m-d');
            $time = time();
        }
        else {
            $current = date($ymd ? 'Y-m-d' : 'm/d/Y', $time = strtotime('previous monday'));
        }
        if (true !== $all) {
            return $current;
        }
        return [$current => date('M d Y', $time), date($ymd ? 'Y-m-d' : 'm/d/Y', $prevTime = strtotime('previous monday', $time - 86400)) => date('M d Y', $prevTime)];
    }

    public static function formatWeeksOf($weeks)
    {
        $data = [];
        foreach ($weeks as $date) {
            $data[$date] = date('M d Y', strtotime($date));
        }
        return $data;
    }

    public static function formatDate($date, $format = null, $default = '0000-00-00')
    {
        if (empty($date)) {
            return $default;
        }
        if (isset($format) && 'm/d/Y' !== $format && 'Y-m-d' !== $format) {
            // not needed but use strtotime here and send back formatted date
            return $default;
        }
        if (!isset($format)) {// default
            $tmp = explode('/', $date);
            if (3 !== count($tmp) || !checkdate($tmp[0], $tmp[1], $tmp[2])) {
                return $default;
            }
            return $tmp[2] . '-' . $tmp[0] . '-' . $tmp[1];
        }
        $tmp = explode('-', $date);
        if (3 !== count($tmp) || !checkdate($tmp[1], $tmp[2], $tmp[0])) {
            return $default;
        }
        return $tmp[1] . '/' . $tmp[2] . '/' . $tmp[0];
    }

    public static function ymdDate($date)
    {
        if (empty($date)) {
            return '0000-00-00';
        }
        $tmp = explode('/', $date);
        if (3 !== count($tmp) || !checkdate($tmp[0], $tmp[1], $tmp[2])) {
            return '0000-00-00';
        }
        return $tmp[2] . '-' . $tmp[0] . '-' . $tmp[1];
    }
}
