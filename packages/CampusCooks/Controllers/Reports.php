<?php
/**
 * Campus Cooks API
 */
namespace CampusCooks\Controllers;

use CampusCooks\RestResponseTrait;

class Reports
{
    use RestResponseTrait;

    protected $services;
    protected $errors = [];

    public function __construct($services)
    {
        $this->services = $services;
    }

    public function search($args)
    {
        $post = $this->services->get('Services')->get('Request')->request;// Services is the core services
        $form = $this->services->get('Services')->get('FormHandler')->setFields($fields = \CampusCooks\Models\Budget::getSearchFields()); // Budget fields work for this, its just picking up ids (house ids)
        $form->validate($this->mapInputFields($post, $fields), false);
        if ($form->hasViolations()) {
            //var_export($form->getViolations());
        }
        $data = $form->toArray();
        if (empty($data['signature']) || empty($data['houseIds'] = array_filter($data['houseIds'], function($item){return ctype_digit($item);}))) {
            $data = [
            'sEcho' => 1,
            'iTotalRecords' => 0,
            'iTotalDisplayRecords' => 0,
            'aaData' => [],
            ];
            $this->sendResponse($data);
            return true;
        }
        // @note the user array is the content
        if (!$this->services->get('Security')->validateSignature($data['signature'], $content = json_encode(['user' => $data['userId']]))) {
            // var_dump($content);exit;
            $this->errors[] = 'Not Authorized';
            $this->sendErrorResponse();
            return true;
        }
        $data = $this->services->get('ReportsEmail')->getData($data['houseIds']);
        $data = [
            'sEcho' => 1,
            'iTotalRecords' => $count = count($data),
            'iTotalDisplayRecords' => $count,
            'aaData' => $data,
        ];
        $this->sendResponse($data);
        return true;
    }

    //@todo add this to utils or form handler internally
    public function mapInputFields($data, $fields)
    {
        $map = [];
        foreach ($fields as $key => $value) {
            $map[$key] = (isset($data[$value['key']])) ? $data[$value['key']] : '';
        }
        return $map;
    }
}
