<?php
/**
 * Campus Cooks API
 */
namespace CampusCooks\Controllers;

use CampusCooks\Models\House;
use CampusCooks\RestResponseTrait;

class Roles
{
    use RestResponseTrait;

    protected $services;
    protected $errors = [];

    public function __construct($services)
    {
        $this->services = $services;
    }

/**
 * getData
 * 
 * get campus or house data based upon user perms
 */ 
    public function getData($args)
    {
        $post = $this->services->get('Services')->get('Request')->request;
        // @note this can use budget fields, we are only interested in user and signature
        $form = $this->services->get('Services')->get('FormHandler')->setFields($fields = \CampusCooks\Models\Budget::getSearchFields());
        $form->validate($this->mapInputFields($post, $fields), false);
        if ($form->hasViolations()) {
            //var_export($form->getViolations());
        }
        $data = $form->toArray();

        if (empty($data['userId']) || !$this->services->get('Security')->validateSignature($data['signature'], $content = json_encode(['user' => $data['userId']]))) {
            // var_dump($content);exit;
            $this->errors[] = 'Not Authorized';
            $this->sendErrorResponse();
            return true;
        }
        if (false !== ($campus = $this->services->get('Roles')->getCampusAccess($user = $this->services->get('Users')->get($data['userId'])))) {
            $this->sendResponseObjects($campus);
            return true;
        }
        if (false === ($houses = $this->services->get('Roles')->getHouseAccess($user)) || empty($houses)) {
            $result = ['houses' => []];
        }
        else {
            $result = ['houses' => $this->services->get('Houses')->getByNames($user->campus, true, true)];//array_map(function($item){},
        }
        $this->sendResponse($result);
        return true;
    }

    //@todo add this to utils or form handler internally
    public function mapInputFields($data, $fields)
    {
        $map = [];
        foreach ($fields as $key => $value) {
            $map[$key] = (isset($data[$value['key']])) ? $data[$value['key']] : '';
        }
        return $map;
    }
}
