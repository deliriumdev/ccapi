<?php
/**
 * Campus Cooks API
 */
namespace CampusCooks\Controllers;

use CampusCooks\Models\House;
use CampusCooks\RestResponseTrait;

class Lateplates
{
    use RestResponseTrait;

    protected $services;
    protected $errors = [];

    public function __construct($services)
    {
        $this->services = $services;
    }

    public function update($args)
    {
        $post = $this->services->get('Services')->get('Request')->request;// Services is the core services
        if (empty($args['id']) || empty($post['token']) || empty($post['id']) || !ctype_digit($post['id'])) {
            $this->errors[] = 'Invalid request parameters not found';
            $this->sendErrorResponse();
            return true;
        }
        // @note the user array is the content
        if (!$this->services->get('Security')->validateSignature($post['token'], json_encode(['user' => $post['id']]))) {
            $this->errors[] = 'Invalid request auth failure';
            $this->sendErrorResponse();
            return true;
        }
        $form = $this->services->get('Services')->get('FormHandler')->setFields($fields = House::getLateplateFields());
        $form->validate($post, false); // all search criteria is optional, do not care if there are violations, it will just skip
        if ($form->hasViolations()) {
            //var_export($form->getViolations());
        }
        $data = $form->toArray();
        $this->services->get('Houses')->updateSettings($args['id'], $data);
        $this->sendResponse(['success' => 1]);
        return true;
    }

    public function remove($args)
    {
        $post = $this->services->get('Services')->get('Request')->request;// Services is the core services
        if (empty($args['id']) || empty($post['token']) || empty($post['id']) || !ctype_digit($post['id'])) {
            $this->errors[] = 'Invalid Request';
            $this->sendErrorResponse();
            return true;
        }
        // @note the user array is the content
        if (!$this->services->get('Security')->validateSignature($post['token'], $content = json_encode(['user' => $post['id']]))) {
            // var_dump($content);exit;
            $this->errors[] = 'Not Authorized';
            $this->sendErrorResponse();
            return true;
        }
        // var_dump($content, $post['token']);exit;
        $this->services->get('Lateplate')->remove($args['id']);
        $this->sendResponse(['success' => 1]);
        $this->services->get('Services')->getNew('Logger', ['logDir' => $this->services->get('Config')->logger->logdir . '/remove.log'])->info(sprintf('Lateplate [%d] removed by [%d]', $args['id'], $post['id']));
        return true;
    }
}
