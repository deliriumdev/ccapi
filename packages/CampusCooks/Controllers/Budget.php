<?php
/**
 * Campus Cooks API
 */
namespace CampusCooks\Controllers;

use CampusCooks\RestResponseTrait;

class Budget
{
    use RestResponseTrait;

    protected $services;
    protected $errors = [];

    public function __construct($services)
    {
        $this->services = $services;
    }

    public function search($args)
    {
        $post = $this->services->get('Services')->get('Request')->request;// Services is the core services
        $form = $this->services->get('Services')->get('FormHandler')->setFields($fields = \CampusCooks\Models\Budget::getSearchFields());
        $form->validate($this->mapInputFields($post, $fields), false);
        if ($form->hasViolations()) {
            //var_export($form->getViolations());
        }
        $data = $form->toArray();
        if (!empty($data['budgetId']) || !empty($data['houseId'])) {
            // @note the user array is the content
            if (!$this->services->get('Security')->validateSignature($data['signature'], $content = json_encode(['user' => $data['userId']]))) {
                // var_dump($content);exit;
                $this->errors[] = 'Not Authorized';
                $this->sendErrorResponse();
                return true;
            }
            if (!empty($data['houseId'])) {
                // get the current budget for the house
                $weeks = \CampusCooks\Utils\DateTime::getCurrentMonday(true, true);
                $weekOf = empty($data['weekOf']) ? key($weeks) : $data['weekOf'];
                if (false === ($budget = $this->services->get('Budgets')->getByWeek($data['houseId'], $weekOf, true, empty($data['budgetType']) ? 'weekly' : $data['budgetType']))) {
                    $this->errors[] = 'No Budget Found';
                    $this->sendErrorResponse();
                    return true;
                }
                if (false === ($data = $this->services->get('Budgets')->getBudgetItems($budget['budgetID'])) || empty($data)) {
                    $this->errors[] = 'No Data Found';
                    $this->sendErrorResponse();
                    return true;
                }
                $this->sendResponseObjects($data);
                return true;
            }
            if (false === ($data = $this->services->get('Budgets')->getBudgetItems($data['budgetId'])) || empty($data)) {
                $this->errors[] = 'No Data Found';
                $this->sendErrorResponse();
            }
            $this->sendResponseObjects($data);
            return true;
            echo "Search for invoice!!!\n";exit;
        }
        if (empty($data['userId'])) {
            $houses = [0];
        }
        else {
            $houses = $this->services->get('Roles')->getHouseAccess($user = $this->services->get('Users')->get($data['userId']));
        }
        if (!empty($data['gfFormId'])) { // form archives request from front end
            if (empty($_POST['length']) || !ctype_digit($length = $_POST['length'])) {
                $length = 50;
            }
            $houses = $this->services->get('Roles')->getHouseAccess($user = $this->services->get('Users')->get($data['userId']));
            $type = 3 == $data['gfFormId'] ? 'billback' : (4 == $data['gfFormId'] ? 'donation' : '');
            $draw = isset($_POST['draw']) && ctype_digit($draw = $_POST['draw']) ? (int) $draw : 1;
            $page = !isset($_POST['start']) || !ctype_digit($page = $_POST['start']) ? 0 : (int) $page;
            $page = ($page / $length) + 1;

            if (false === ($archives = $this->services->get('Budgets')->getArchiveData($houses, $length, $page, $type))) {
                $this->sendResponse([]);
                return true;
            }
            $response = [
                'draw' => $draw,
                'recordsTotal' => $count = $this->services->get('Budgets')->getResultCount(),
                'recordsFiltered' => $count,
                'data' => \CampusCooks\Views\Mappers\BudgetTable::map($archives, $data['gfFormId']),
            ];
            $this->services->get('Services')->get('Response')->setContentType('application/json')->setContent(json_encode($response))->setStatusCode(200)->send();
            return true;
        }
        if (isset($data['currentWeek']) && '1' === $data['currentWeek']) {
            $data['weekOf'] = \CampusCooks\Utils\DateTime::getCurrentMonday();
        }
        // echo("houses perms\n");var_export($houses);
        if (false === ($result = $this->services->get('Budgets')->search(empty($data['weekOf']) ? null : $data['weekOf'], null, $houses))) {
            $result = [];
        }
        if (!empty($data['weekOf'])) {
            $result = array_values(array_filter($this->services->get('Budgets')->addBudgetItemSummary($result, $data['weekOf']), function($item){return !empty($item['amounts']);}));
        }
        if ('chart' === $data['dataType']) {
            $this->sendResponseObjects(\CampusCooks\Views\Mappers\ChartJs::map($result));
            return true;
        }
        $dt = array(
            'sEcho' => 1,
            'iTotalRecords' => $count = count($result),
            'iTotalDisplayRecords' => $count,
            'aaData' => $result,
        );
        $this->sendResponse($dt);
        return true;
    }

    public function update($args)
    {
        $post = $this->services->get('Services')->get('Request')->request;// Services is the core services
        $form = $this->services->get('Services')->get('FormHandler')->setFields($fields = \CampusCooks\Models\Budget::getUpdateFields());
        $form->validate($this->mapInputFields($post, $fields), false);
        if ($form->hasViolations()) {
            var_export($form->getViolations());exit;
        }
        $data = $form->toArray();
        if (!empty($data['itemId'])) {
            // @note the user array is the content
            if (!$this->services->get('Security')->validateSignature($data['signature'], $content = json_encode(['user' => $data['userId']]))) {
                // var_dump($content);exit;
                $this->errors[] = 'Not Authorized';
                $this->sendErrorResponse();
                return true;
            }
            if (false === ($this->services->get('Budgets')->updateBudgetItem($data['itemId'], $data['fieldName'], $data['value']))) {
                $this->errors[] = 'Could not update data';
                $this->sendErrorResponse();
            }
            $this->sendResponse([]);
            return true;
        }
        $this->errors[] = 'Could not update data';
        $this->sendErrorResponse();
    }

    public function remove($args)
    {
        $post = $this->services->get('Services')->get('Request')->request;// Services is the core services
        $form = $this->services->get('Services')->get('FormHandler')->setFields($fields = \CampusCooks\Models\Budget::getUpdateFields());
        $form->validate($this->mapInputFields($post, $fields), false);
        if ($form->hasViolations()) {
            var_export($form->getViolations());exit;
        }
        $data = $form->toArray();
        if (!empty($data['itemId'])) {
            // @note the user array is the content
            if (!$this->services->get('Security')->validateSignature($data['signature'], $content = json_encode(['user' => $data['userId']]))) {
                // var_dump($content);exit;
                $this->errors[] = 'Not Authorized';
                $this->sendErrorResponse();
                return true;
            }
            if (false === ($this->services->get('Budgets')->removeBudgetItem($data['itemId']))) {
                $this->errors[] = 'Could not remove data';
                $this->sendErrorResponse();
            }
            $this->sendResponse([]);
            return true;
        }
        $this->errors[] = 'Could not remove data';
        $this->sendErrorResponse();
    }

//@todo add this to utils or form handler internally
    public function mapInputFields($data, $fields)
    {
        $map = [];
        foreach ($fields as $key => $value) {
            $map[$key] = (isset($data[$value['key']])) ? $data[$value['key']] : '';
        }
        return $map;
    }
}
