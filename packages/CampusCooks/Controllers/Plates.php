<?php
/**
 * Campus Cooks API
 */
namespace CampusCooks\Controllers;

use CampusCooks\RestResponseTrait;

class Plates
{
    use RestResponseTrait;

    protected $services;
    protected $errors = [];

    public function __construct($services)
    {
        $this->services = $services;
    }

    public function getData($args)
    {
        $post = $this->services->get('Services')->get('Request')->request;// Services is the core services
        $form = $this->services->get('Services')->get('FormHandler')->setFields($fields = \CampusCooks\Models\Budget::getSearchFields());
        $form->validate($this->mapInputFields($post, $fields), false);
        if ($form->hasViolations()) {
            //var_export($form->getViolations());
        }
        $data = $form->toArray();
        // var_dump($data);exit;
        if (empty($data['houseId'])) {
            $this->errors[] = 'No House Specified';
            $this->sendErrorResponse();
            return true;
        }
        // @note the user array is the content
        if (!$this->services->get('Security')->validateSignature($data['signature'], $content = json_encode(['user' => $data['userId']]))) {
            // var_dump($content);exit;
            $this->errors[] = 'Not Authorized';
            $this->sendErrorResponse();
            return true;
        }

        if (!empty($data['dataType']) && 'all' === $data['dataType']) { // new table with menu
            if (false === ($result = $this->services->get('Attendance')->getPlateCtB($data['houseId'], $data['weekOf']))) {
                $this->sendResponseObjects([0]);
                return true;
            }
            $this->sendResponseObjects($result);
            return true;
        }
            
        if (false === ($result = $this->services->get('Attending')->getPlates($data['houseId'], $data['weekOf'], !empty($data['dataType']) && 'lp' === $data['dataType'])) || empty($result)) {
            $result = [0, 0, 0, 0, 0, 0, 0];
        }
        $this->sendResponseObjects($result);
        return true;
    }

    public function update($args)
    {
        $post = $this->services->get('Services')->get('Request')->request;// Services is the core services
        $form = $this->services->get('Services')->get('FormHandler')->setFields($fields = \CampusCooks\Models\Attending::getUpdateFields());
        $form->validate($this->mapInputFields($post, $fields), false);
        if ($form->hasViolations()) {
            var_export($form->getViolations());exit;
        }
        $data = $form->toArray();
        if (!empty($data['menuId'])) {
            if (empty($data['value']) || !ctype_digit($data['value'])) {
                $this->errors[] = 'Invalid Number';
                $this->sendErrorResponse();
                return true;
            }
            // security check
            if (!$this->services->get('Security')->validateSignature($data['signature'], $content = json_encode(['user' => $data['userId']]))) {
                // var_dump($content);exit;
                $this->errors[] = 'Not Authorized';
                $this->sendErrorResponse();
                return true;
            }
            $this->services->get('Attendance')->updatePlateCt($data['menuId'], $data['value'], !empty($data['isLateplate']));
            $this->sendResponse(['success' => 1]);
            return true;
        }
        if (empty($data['values']) || !is_array($data['values']) || 7 !== count($data['values'])) {
            $this->errors[] = 'Values not specified';
            $this->sendErrorResponse();
            return true;
        }
        if (empty($data['houseId'])) {
            $this->errors[] = 'House not specified';
            $this->sendErrorResponse();
            return true;
        }
        if (empty($data['weekOf'])) {
            $this->errors[] = 'Week not specified';
            $this->sendErrorResponse();
            return true;
        }
        // @note the user array is the content
        if (!$this->services->get('Security')->validateSignature($data['signature'], $content = json_encode(['user' => $data['userId']]))) {
            // var_dump($content);exit;
            $this->errors[] = 'Not Authorized';
            $this->sendErrorResponse();
            return true;
        }
        if (false === ($this->services->get('Attending')->updatePlateCounts($data['houseId'], $data['weekOf'], $data['values'], !empty($data['isLateplate']) && 'lp' === $data['isLateplate']))) {
            $this->errors[] = 'Could not update data';
            $this->sendErrorResponse();
        }
        $this->sendResponse([]);
        return true;
    }

//@todo add this to utils or form handler internally
    public function mapInputFields($data, $fields)
    {
        $map = [];
        foreach ($fields as $key => $value) {
            $map[$key] = (isset($data[$value['key']])) ? $data[$value['key']] : '';
        }
        return $map;
    }
}
