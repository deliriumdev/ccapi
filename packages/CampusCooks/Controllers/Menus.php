<?php
/**
 * Campus Cooks API
 */
namespace CampusCooks\Controllers;

use CampusCooks\RestResponseTrait;
use CampusCooks\Views;

class Menus
{
    use RestResponseTrait;

    protected $services;
    protected $errors = [];

    public function __construct($services)
    {
        $this->services = $services;
    }

    function search($args)
    {
        $menus = $this->services->get('Menus');
        // process the post
        $form = $this->services->get('Services')->get('FormHandler')->setFields($fields = \CampusCooks\Models\Menu::getFields());
        $form->validate($this->mapFormFields($_POST, $fields), false); // all search criteria is optional, do not care if there are violations, it will just skip
        if ($form->hasViolations()) {
            //var_export($form->getViolations());
        }
        $data = $form->toArray();
        if (!empty($data['houses'])) {
            $data['houses'] = array_filter($data['houses'], function($id){
                return ctype_digit($id);
            });
        }
        $results = $menus->search($data['keyword'], isset($data['campusId']) ? $data['campusId'] : false, $data['startDate'], $data['endDate'], empty($data['page']) ? ($data['page'] = 1) : ($data['page'] = (int) $data['page']), $isFullWeek = !empty($data['fullWeek']), empty($data['houses']) ? (isset($data['houseId']) ? $data['houseId'] : false) : $data['houses']);

        // at this point can either send data in json for the frontend to render or deliver the template from here
        $view = new Views\Menu($results);
        if ($isFullWeek) {
            $content = $view->getFullWeekContent();
        }
        else {
            $content = $view->getContent();
        }
        $this->sendResponse(['count' => $menus->getResultCount(), 'pageCount' => $menus->getPaginator()->last, 'page' => $data['page'], 'html' => $content]);
        return true;
    }

    public function getData($args)
    {
        $post = $this->services->get('Services')->get('Request')->request;// Services is the core services
        if (empty($post['weekOf']) || empty($post['houseId']) || !ctype_digit($post['houseId'])) {
            $this->errors[] = 'Invalid request';
            $this->sendErrorResponse();
        }
        
        if (false === ($data = $this->services->get('Menus')->getByWeek($post['weekOf'], $post['houseId']))) {
            $data = [];
        }
        $this->sendResponseObjects($data);
        return true;
    }
}
