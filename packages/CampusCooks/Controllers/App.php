<?php
/**
 * Campus Cooks API
 */
namespace CampusCooks\Controllers;

use CampusCooks\RestResponseTrait;

class App
{
    use RestResponseTrait;

    protected $services;
    protected $errors = [];
    protected $data = ['vendors' => []];

    public function __construct($services)
    {
        $this->services = $services;
    }

    public function getData($args)
    {
        $post = $this->services->get('Services')->get('Request')->request;// Services is the core services
        // basically just the auth fields so can use the budget model here
        $form = $this->services->get('Services')->get('FormHandler')->setFields($fields = \CampusCooks\Models\Budget::getSearchFields());
        $form->validate($this->mapInputFields($post, $fields), false);

        $data = $form->toArray();
        if (!$this->services->get('Security')->validateSignature($data['signature'], $content = json_encode(['user' => $data['userId']]))) {
            // var_dump($content);exit;
            $this->errors[] = 'Not Authorized';
            $this->sendErrorResponse();
            return true;
        }
        $this->data['vendors'] = $this->services->get('WpData')->getVendors();
        $this->data['cards'] = $this->services->get('WpData')->getCards();
        $this->sendResponse($this->data);
        return true;
    }
//@todo add this to utils or form handler internally
    public function mapInputFields($data, $fields)
    {
        $map = [];
        foreach ($fields as $key => $value) {
            $map[$key] = (isset($data[$value['key']])) ? $data[$value['key']] : '';
        }
        return $map;
    }
}
