<?php
/**
 * Campus Cooks API
 */
namespace CampusCooks\Controllers;

use CampusCooks\Models\House;
use CampusCooks\RestResponseTrait;

class Houses
{
    use RestResponseTrait;

    protected $services;
    protected $errors = [];

    public function __construct($services)
    {
        $this->services = $services;
    }

    public function getData($args)
    {
        $post = $this->services->get('Services')->get('Request')->request;// Services is the core services
        /*if (empty($args['id']) || empty($post['token']) || empty($post['id']) || !ctype_digit($post['id'])) {
            $this->errors[] = 'Invalid request';
            $this->sendErrorResponse();
            return true;
        }
        // @note the user array is the content
        if (!$this->services->get('Security')->validateSignature($post['token'], json_encode(['user' => (int)$post['id']]))) {
            $this->errors[] = 'Invalid request';
            $this->sendErrorResponse();
            return true;
        }*/
        if (empty($post['ids'])) {
            $this->errors[] = 'Invalid request';
            $this->sendErrorResponse();
        }
        $houseIds = array_filter($post['ids'], function($item){
            return ctype_digit($item);
        });
        if (empty($houseIds)) {
            $this->errors[] = 'Invalid request';
            $this->sendErrorResponse();
        }
        if (false === ($data = $this->services->get('Houses')->getData($houseIds))) {
            $data = [];
        }
        $this->sendResponseObjects($data);
        return true;
    }

    public function search($args)
    {
        $houses = $this->services->get('Houses');
        // process the post
        $form =  $this->services->get('Services')->get('FormHandler')->setFields($fields = House::getSearchFields());// both attending and lateplate have same fields
        $form->validate($this->mapInputFields($_REQUEST, $fields), false); // all search criteria is optional, do not care if there are violations, it will just skip
        if ($form->hasViolations()) {
            //var_export($form->getViolations());
        }
        $data = $form->toArray();
        if (!empty($data['campuses']) && is_array($data['campuses'])) {
            $results = $houses->getByNames($data['campuses']);
        }
        elseif (!empty($data['campusIds']) && is_array($data['campusIds'])) {
            $results = $houses->getByNames($data['campusIds'], true, true);
            if (isset($data['userId']) && isset($data['signature']) && $this->services->get('Security')->validateSignature($data['signature'], $content = json_encode(['user' => $data['userId']]))) { // limit to user houses
                if (false !== ($assignedHouses = $this->services->get('Roles')->getHouseAccess($user = $this->services->get('Users')->get($data['userId']))) && !empty($assignedHouses)) {
                    // for some reason the perms are giving all assigned, maybe needed for something else such as choices, so filter by user->houses
                    $results = array_filter($results, function($item) use ($user) {
                        return in_array($item['id'], $user->houses);
                    });
                }
            }
        }
        elseif (!empty($data['house'])) {
            $results = $houses->getByName($data['house']);
            $this->sendResponse($results);
            return true;
        }
        else {
            if (empty($data['campusName'])) {
                var_dump($data, $_POST, $_REQUEST);exit;
                $results = false;
            }
            else {
                $results = $houses->getByCampusName($data['campusName']);
            }
        }
        if (empty($results)) {
            $results = [];
        }
        $this->sendResponseObjects($results);
        return true;
    }

    //@todo add this to utils or form handler internally
    public function mapInputFields($data, $fields)
    {
        $map = [];
        foreach ($fields as $key => $value) {
            $map[$key] = (isset($data[$value['key']])) ? $data[$value['key']] : '';
        }
        return $map;
    }
}
