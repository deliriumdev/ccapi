<?php
/**
 * Campus Cooks API
 */
namespace CampusCooks\Controllers;

use CampusCooks\RestResponseTrait;

class Stats
{
    use RestResponseTrait;

    protected $services;
    protected $errors = [];
    protected $data = ['avgAttending' => 0, 'avgLateplate' => 0, 'weeklyBudget' => 0, 'studentCt' => 0, 'budget' => [], 'attending' => [], 'lateplates' => [], 'weeks' => []];

    public function __construct($services)
    {
        $this->services = $services;
    }

    public function getData($args)
    {
        $post = $this->services->get('Services')->get('Request')->request;// Services is the core services
        $form = $this->services->get('Services')->get('FormHandler')->setFields($fields = \CampusCooks\Models\Budget::getSearchFields());
        $form->validate($this->mapInputFields($post, $fields), false);
        if ($form->hasViolations()) {
            //var_export($form->getViolations());
        }
        $data = $form->toArray();
        if (empty($data['houseIds'] = array_filter($data['houseIds'], function($item){return ctype_digit($item);}))) {
            $this->sendResponse($this->data);
            return true;
        }
        // @note the user array is the content
        if (!$this->services->get('Security')->validateSignature($data['signature'], $content = json_encode(['user' => $data['userId']]))) {
            // var_dump($content);exit;
            $this->errors[] = 'Not Authorized';
            $this->sendErrorResponse();
            return true;
        }
        // can also check here if permissions on houses, for budget info
        // this can be aggregated from the chart data
        //$this->data['avgAttending'] = $this->services->get('Attending')->getAvg($data['houseIds']);
        // this can be aggregated from the chart data
        // $this->data['avgLateplate'] = $this->services->get('Lateplate')->getAvg($data['houseIds']);
        $this->data['studentCt'] = $this->services->get('Students')->getHouseStudents($data['houseIds']);
        // $this->data['weeks'] = \CampusCooks\Utils\DateTime::getCurrentMonday(true, true);
        // get all weeks for which there is budget data
        $this->data['weeks'] = \CampusCooks\Utils\DateTime::formatWeeksOf($this->services->get('Budgets')->getActiveWeeks($data['houseIds'], \CampusCooks\Utils\DateTime::getCurrentMonday()));
        $current = key($this->data['weeks']);
        $weekOf = empty($data['weekOf']) ? (empty($data['allWeeks']) ? $current : false) : $data['weekOf'];
        if (empty($data['weekOf'])) {
            if (empty($data['allWeeks'])) {
                $data['allWeeks'] = '1';
            }
            $weekOf = false;
        }
        else {
            $weekOf = $data['weekOf'];
        }
        // $this->data['weeklyBudget'] = $this->services->get('Budgets')->getByWeek($data['houseIds'], $weekOf = \CampusCooks\Utils\DateTime::getCurrentMonday());
        // send the budget chart data in the same payload
        //if (false !== ($result = $this->services->get('Budgets')->search($weekOf, null, $data['houseIds']))) {
        // if all weeks get weekOf chart
        if (!empty($data['allWeeks'])) {
            if (false !== ($result = $this->services->get('Budgets')->getWeekOfData($data['houseIds']))) {
                $this->data['budget'] = \CampusCooks\Views\Mappers\ChartJs::mapWeekOf($result);
                $this->data['weeklyBudget'] = array_sum(array_map(function($item){return $item['total'];}, $this->data['budget']));
            }
        }
        // instead of daily line get a pie by vendor
        elseif (false !== ($result = $this->services->get('Budgets')->getPieData($data['houseIds'], $weekOf))) {
            $this->data['budget'] = \CampusCooks\Views\Mappers\ChartJs::mapPie($result);
            $this->data['weeklyBudget'] = $this->data['budget']['total'];
            if (false !== ($byHouseData = $this->services->get('Budgets')->getPieData($data['houseIds'], $weekOf, true))) {
                $this->data['budget']['houses'] = $byHouseData;
            }
            else {
                $this->data['budget']['houses'] = [];
            }
            // @depricated
            /*$result = array_values(array_filter($this->services->get('Budgets')->addBudgetItemSummary($result, $weekOf), function($item){return !empty($item['amounts']);}));
            $this->data['budget'] = \CampusCooks\Views\Mappers\ChartJs::map($result);
            $this->data['weeklyBudget'] = array_sum(array_map(function($item){return $item['total'];}, $this->data['budget'])); // this should not vary from total for raw result, but does*/
        }
        // if (false !== ($result = $this->services->get('Attending')->getPlateAvg($data['houseIds'], true)) ||
        $isAllWeeks = !empty($data['allWeeks']);
        // lateplates
        // get app data
        /*if (false !== ($result = $this->services->get('Lateplate')->getAvgByWeekday($data['houseIds'], $weekOf))) {
            
        }*/
        // lateplates
        if (false !== ($result = $this->services->get('Attendance')->getPlateData($data['houseIds'], empty($data['allWeeks']) ? $weekOf : false, true, $data['userId']))) {
            $this->data['lateplates'] = \CampusCooks\Views\Mappers\ChartJs::mapAttending($result);
            if ($isAllWeeks) {
                $this->data['avgLateplate'] = array_sum(array_map(function($item){return $item['avgTotal'];}, $this->data['lateplates']));
            }
            else {
                $this->data['avgLateplate'] = array_sum(array_map(function($item){return $item['total'];}, $this->data['lateplates']));
            }
            /*if ($isAllWeeks && 0 !== ($weekCt = $this->services->get('Attendance')->getWeekCt($data['houseIds'], true))) {// get the avg
                $this->data['avgLateplate'] = round($this->data['avgLateplate'] / $weekCt);
            }*/
        }
        // plate count
        if (false !== ($result = $this->services->get('Attendance')->getPlateData($data['houseIds'], empty($data['allWeeks']) ? $weekOf : false))) {
            $this->data['attending'] = \CampusCooks\Views\Mappers\ChartJs::mapAttending($result);
            if ($isAllWeeks) {
                $this->data['avgAttending'] = array_sum(array_map(function($item){return $item['avgTotal'];}, $this->data['attending']));
            }
            else {
                $this->data['avgAttending'] = array_sum(array_map(function($item){return $item['total'];}, $this->data['attending']));
            }
            /*if ($isAllWeeks && 0 !== ($weekCt = $this->services->get('Attendance')->getWeekCt($data['houseIds']))) {// get the avg
                $this->data['avgAttending'] = round($this->data['avgAttending'] / $weekCt);
            }*/
        }
        // make the weeks top down, js cannot easily do this
        // $this->data['weeks'] = array_reverse($this->data['weeks'], true);
        $this->sendResponse($this->data);
        return true;
    }

//@todo add this to utils or form handler internally
    public function mapInputFields($data, $fields)
    {
        $map = [];
        foreach ($fields as $key => $value) {
            $map[$key] = (isset($data[$value['key']])) ? $data[$value['key']] : '';
        }
        return $map;
    }
}
