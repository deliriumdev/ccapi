<?php
/**
 * Campus Cooks API
 */
namespace CampusCooks;

use Reo\DependencyInjection\Container;

class ApiServices extends Container
{
/**
 * __construct
 * 
 * register services used by this API
 */
    public function __construct($coreServices)
    {
        $this->set('Services', $coreServices);
        $this->registerShared([
            'Students' => function($c)
            {
                return new Models\StudentCollection($c->get('Services')->get('Pdo'), $c->get('Services')->get('Config')->get('database')->get('prefix'));
            },
            'Users' => function($c)
            {
                return new Models\Users\UserCollection($c->get('Services')->get('Pdo'), $c->get('Services')->get('Config')->get('database')->get('prefix'));
            },
            'Roles' => function($c)
            {
                return new Models\Users\RoleCollection($c->get('Services')->get('Pdo'), $c->get('Services')->get('Config')->get('database')->get('prefix'), $c);
            },
            'Lateplate' => function($c)
            {
                return new Models\LateplateCollection($c->get('Services')->get('Pdo'), $c);
            },
            'Notifications' => function($c)
            {
                return new Models\NotificationCollection($c->get('Services')->get('Pdo'), $c);
            },
            'Menus' => function($c)
            {
                return new Models\MenuCollection($c->get('Services')->get('Pdo'), $c->get('Services')->get('Paginator'), $c);
            },
            'Budgets' => function($c)
            {
                return new Models\BudgetCollection($c->get('Services')->get('Pdo'), $c->get('Services')->get('Paginator'), $c);
            },
            'Attending' => function($c)
            {
                return new Models\AttendingCollection($c->get('Services')->get('Pdo'), $c);
            },
            'ReportsEmail' => function($c)
            {
                return new Models\Reports\Email($c->get('Services')->get('Pdo'), $c);
            },
            'Attendance' => function($c)
            {
                return new Models\Attendance($c->get('Services')->get('Pdo'), $c);
            },
            'Feedback' => function($c)
            {
                return new Models\FeedbackCollection($c->get('Services')->get('Pdo'), $c);
            },
            'Forum' => function($c)
            {
                return new Models\ForumCollection($c->get('Services')->get('Pdo'), $c);
            },
            'Ratemeal' => function($c)
            {
                return new Models\RatemealCollection($c->get('Services')->get('Pdo'), $c);
            },
            'Campus' => function($c)
            {
                return new Models\CampusCollection($c->get('Services')->get('Pdo'), $c);
            },
            'Houses' => function($c)
            {
                return new Models\HouseCollection($c->get('Services')->get('Pdo'), $c);
            },
            'Entrys' => function($c)
            {
                return new Models\EntryFactory($c->get('Services')->get('Pdo'), $c);
            },
            'ReportRunner' => function($c)
            {
                return new Reports\Scheduler($c, $c->get('Services')->get('Config')->get('api'));
            },
            'ReportMailer' => function($c)
            {
                return new Reports\Mailer($c->get('Services')->get('Swift'), $c->get('Services')->getNew('Logger', ['logDir' => $c->get('Services')->get('Config')->get('logger')->get('maillog')]), $c->get('Services')->get('Config')->get('mailer'));
            },
            'WpData' => function($c)
            {
                return new Utils\WpData($c->get('Services')->get('Pdo'), $c->get('Services')->get('Config')->get('database')->get('prefix'));
            },
            'WpImport' => function($c)
            {
                return new Utils\WpImport($c->get('Services')->get('Pdo'), $c->get('Services')->get('Config')->get('database')->get('prefix'));
            },
            'Security' => function($c)
            {
                return new Utils\Security($this->get('Services')->get('Config')->api->get('hmac_key'));
            },
        ]);
    }
}
