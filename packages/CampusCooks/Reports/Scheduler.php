<?php
/**
 * Campus Cooks API
 */
namespace CampusCooks\Reports;

class Scheduler
{
    protected $services;
    protected $runHours;
    protected $dailyHour;
    protected $timezones;
    protected $reportRoles;
    protected $templateDir;
    protected static $defaultTz = 'UTC';
    protected static $tzOffsets = [];

    public function __construct($services, $config, $timezones = null)
    {
        $this->services = $services;
        $this->runHours = $config->get('report_hours')->toArray();
        $this->reportRoles = $config->offsetExists('report_roles') ? $config->get('report_roles')->toArray() : [];
        $this->dailyHour = $config->get('daily_report_hour');
        $this->templateDir = $config->get('mail_template_dir');
        self::$defaultTz = $config->get('default_tz');
        $this->timezones = isset($timezones) ? $timezones : ['EST', 'CST', 'MST', 'PST'];
    }

    public function run()
    {
        $tzRun = false;
        if ('UTC' !== ($resetTz = date_default_timezone_get())) {
            date_default_timezone_set('UTC');
        }
        $this->runScheduledByHour();
        // run the attending the old way with set times
        // check hours to run against timezones
        $time = time();
        foreach ($this->timezones as $tz) {
            $hourLocal = date('G', $time + self::getTimeZoneOffset($tz));
            if ($hourLocal == $this->dailyHour) {//daily reports
                $tzRun = $tz;
                $mealtime = 'daily';
                printf("Run daily scheduled in %s zone\n", $tz);
                $this->runDailyReports($tz);
                // break;
            }
            // the following would have to be replace by something that queries meal schedule like this
            foreach ($this->runHours as $mealtime => $hour) {
                printf("%s hour is %s vs %d\n", $tz, $hourLocal, $hour);
                if ($hourLocal == $hour) { //run it!
                    $tzRun = $tz;
                    printf("Run %s in %s zone\n", $mealtime, $tz);
                    $this->runScheduled($tz, $mealtime);
                    //break 2;
                }
            }
        }
        if (false === $tzRun) {
            return false;
        }
        return true;
    }

    public function runScheduledByHour()
    {
        $time = time();
        // get the hour in each time zone
        $hours = [];
        foreach ($this->timezones as $tz) {
            $hours[$tz] = date('G', $time + self::getTimeZoneOffset($tz));
        }
        // var_dump($hours);
        // populates the campuses from wp to api
        $this->services->get('Campus')->getCampusByTz($tz);

        $scheduled = [
            'lateplate' => ['service' => 'Lateplate', 'template' => 'lateplate-template.php', 'subject' => 'Lateplate Report'],
            // 'attending' => ['service' => 'Attending', 'template' => 'attending-template.php', 'subject' => 'Attending Report'],
        ];

        foreach ($scheduled as $report) {
            $items = [];
            $lateplates = $this->services->get($report['service']);
            // get lateplates that send for the current hour in each time zone
            foreach ($hours as $tz => $hour) {
                if (false === ($meals = $lateplates->getTodayByHour($hour, $tz, false))) { // no sorting necessary, items are accumulated
                    // echo "Nada!\n";
                    continue;
                }
                $items = array_merge($items, $meals); 
            }
            if (empty($items)) {
                continue;
            }
            $lateplates->populateItems($items, true);
            // var_dump($lateplates->toArray());exit;//,  $this->templateDir . '/lateplate-template.php');
            // get the recipients
            foreach ($lateplates as $houseId => $item) {
                //@note campus ID isn't really needed but passed so the user can be imported frow WP if neccessary
                //$lateplates->setRecipients($houseId, $this->services->get('Houses')->getRecipients($houseId, $item[0]['campus_id'], $this->reportRoles));
                //@note use the roles object now that syncs better with wp roles and groups
                $lateplates->setRecipients($houseId, $this->services->get('Roles')->getReportUsers($houseId, $item[0]['campus_id']));
            }
            $this->services->get('ReportMailer')->send($lateplates, $this->templateDir . '/' . $report['template'], $report['subject']);
        }
        return true;
    }

    public function runScheduled($tz, $mealtime)
    {
        // populates the campuses from wp to api
        $this->services->get('Campus')->getCampusByTz($tz);
        $scheduled = [
            // 'lateplate' => ['service' => 'Lateplate', 'template' => 'lateplate-template.php', 'subject' => 'Lateplate Report'],
            'attending' => ['service' => 'Attending', 'template' => 'attending-template.php', 'subject' => 'Attending Report'],
        ];
        foreach ($scheduled as $report) {
        // gets the lateplates for tz
            if (false === ($items = $this->services->get($report['service'])->getToday($mealtime, $tz))) {
                continue;
            }
            //var_dump($lateplates->toArray(),  $this->templateDir . '/lateplate-template.php');
            // get the recipients
            foreach ($items as $houseId => $item) {
                //@note campus ID isn't really needed but passed so the user can be imported frow WP if neccessary
                //$items->setRecipients($houseId, $this->services->get('Houses')->getRecipients($houseId, $item[0]['campus_id'], $this->reportRoles));
                //@note use the roles object now that syncs better with wp roles and groups
                $items->setRecipients($houseId, $this->services->get('Roles')->getReportUsers($houseId, $item[0]['campus_id']));
            }
            $this->services->get('ReportMailer')->send($items, $this->templateDir . '/' . $report['template'], $report['subject']);
        }
        return true;
    }

    public function runDailyReports($tz)
    {
        // populates the campuses from wp to api
        $this->services->get('Campus')->getCampusByTz($tz);
        $scheduled = [
            'feedback' => ['service' => 'Feedback', 'template' => 'feedback-template.php', 'subject' => 'Community Feedback Report'],
            'ratemeal' => ['service' => 'Ratemeal', 'template' => 'ratemeal-template.php', 'subject' => 'Meal Rating Report'],
        ];
        foreach ($scheduled as $report) {
            if (false === ($items = $this->services->get($report['service'])->getData($tz))) {
                continue;
            }
            //var_dump($this->templateDir . '/feedback-template.php', file_exists($this->templateDir . '/feedback-template.php'));exit;
            // get the recipients
            foreach ($items as $houseId => $item) {
                //@note campus ID isn't really needed but passed so the user can be imported frow WP if neccessary
                $items->setRecipients($houseId, $this->services->get('Houses')->getRecipients($houseId, $item[0]['campus_id'], $this->reportRoles));
            }
            $this->services->get('ReportMailer')->send($items, $this->templateDir . '/' . $report['template'], $report['subject']);
        }
        return true;
    }

    public static function getTimeZoneOffset($zone = 0)
    {
        if(isset(self::$tzOffsets[$zone])) {
            return self::$tzOffsets[$zone];
        }

        switch ($zone) {
            case 'EST': 
                $tz = 'America/New_York';
            break;
            case 'CST':
                $tz = 'America/Chicago';
            break;
            case 'PST':
                $tz = 'America/Los_Angeles';
            break;
            case 'MST':
                $tz = 'America/Denver';
            break;
            default:
                $tz = self::$defaultTz;
                $zone = 0;
            break;
        }
        // @note will be the offset against the current timezone which in most cases should be UTC
        $offset = self::$tzOffsets[$zone] = timezone_offset_get(timezone_open($tz), new \DateTime());
        return $offset;
    }

    public function getRunHours()
    {
        return $this->runHours;
    }

    public function getReportRoles()
    {
        return $this->reportRoles;
    }

    public function setRunHours(array $hours)
    {
        $this->runHours = $hours;
    }
}
