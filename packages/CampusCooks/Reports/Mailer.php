<?php
/**
 * Campus Cooks API
 */
namespace CampusCooks\Reports;

class Mailer
{
    protected $mailer;
    protected $from;

    public function __construct($mailer, $logger, $config)
    {
        $this->mailer = $mailer;
        $this->logger = $logger;
        $this->from = [$config->get('fromEmail') => $config->get('fromName')];
    }

    public function send($data, $template, $subject)
    {
        foreach ($data as $houseId => $reports) {
            ob_start();
            include $template;
            $content = ob_get_clean();
            $message = \Swift_Message::newInstance()
                ->setSubject($subject)
                ->setFrom($this->from)
                ->setReplyTo($this->from)
                ->setBody('A ' . $subject . ' has been included with this message. Please view this message in an email client that supports HTML rendering.')
                ->addPart($content, 'text/html');
            //$message->getHeaders()->addTextHeader('X-Mailer', sprintf('X-Mailer: PHP/%s', phpversion()));
            if (false === ($recipients = $data->getRecipients($houseId))) {
                $this->logger->error(sprintf('Recipients not found for House [%s] ID [%d]', $reports[0]['house_name'], $houseId));
                continue;
            }
            $this->logger->info(sprintf('Mailing %s reports to %s [%d]', $subject, $reports[0]['house_name'], $houseId)); 
            foreach($recipients as $email) {
                var_dump($email, $reports[0]['house_name']);
                $this->mailer->send($message->setTo($email));
                //$this->mailer->send($message->setTo(['schuyler@gatordev.com' => 'Schuyler']));
                $this->logger->info(sprintf('- Sent to %s %s', current($email), key($email)));
                sleep(2); // 2 seconds throttled 30 per minute
                //break;
            }
            $data->recordSend($houseId, $subject);
            //break;
        }
    }

    public function sendTest($data, $template, $subject, array $email)
    {
        foreach ($data as $houseId => $reports) {
            ob_start();
            include $template;
            $content = ob_get_clean();
            $message = \Swift_Message::newInstance()
                ->setSubject($subject)
                ->setFrom($this->from)
                ->setReplyTo($this->from)
                ->setBody('A ' . $subject . ' has been included with this message. Please view this message in an email client that supports HTML rendering.')
                ->addPart($content, 'text/html');
            //$message->getHeaders()->addTextHeader('X-Mailer', sprintf('X-Mailer: PHP/%s', phpversion()));
            printf('Mailing %s reports to %s [%d]', $subject, $reports[0]['house_name'], $houseId); 
            $this->mailer->send($message->setTo($email));
            printf('- Sent to %s %s', current($email), key($email)); 
            break;
        }
    }
}
