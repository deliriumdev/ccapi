<?php
/**
 * Campus Cooks API
 */
namespace CampusCooks\Models;

use Reo\Collection\TraversableTrait;

class MenuItemCollection implements \ArrayAccess, \Countable, \IteratorAggregate
{
    use TraversableTrait;

    protected $entree;
    protected $dessert;
    protected $soup;
    protected $sides;

    public function __construct(array $items)
    {
        $this->items = $items;
    }

/**
 * override magic get to return empty string for empty values and special names
 */
    public function __get($name)
    {
        if ('entree' === $name) {
            return $this->getEntree();
        }
        if ('dessert' === $name) {
            return $this->getDessert();
        }
        if ('soup' === $name) {
            return $this->getSoup();
        }
        if ('sides' === $name) {
            return $this->getSides();
        }

        if (empty($this->items[$name])) {
            return '';
        }
        return $this->items[$name];
    }

    public function getEntree()
    {
        if (isset($this->entree)) {
            return $this->entree;
        }
        $this->parseMenu();
        return $this->entree;
    }

    public function getDessert()
    {
        if (isset($this->dessert)) {
            return $this->dessert;
        }
        $this->parseMenu();
        return $this->dessert;
    }

    public function getSoup()
    {
        if (isset($this->soup)) {
            return $this->soup;
        }
        $this->parseMenu();
        return $this->soup;
    }

    public function getSides()
    {
        if (isset($this->sides)) {
            return $this->sides;
        }
        $this->parseMenu();
        return $this->sides;
    }

    protected function parseMenu()
    {
        $this->sides = [];
        $this->entree = $this->dessert = '';
        foreach ($this->items as $key => $item) {
            if ('entree' === $item['meal_item_type']) {
                $this->entree = new MenuItem($item);
            }
            elseif ('dessert' === $item['meal_item_type']) {
                $this->dessert = new MenuItem($item);
            }
            elseif ('soup' === $item['meal_item_type']) {
                $this->soup = new MenuItem($item);
            }
            else {
                $this->sides[] = new MenuItem($item);
            }
        }
    }
}
