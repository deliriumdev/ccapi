<?php
/**
 * Campus Cooks API
 */
namespace CampusCooks\Models;

use Reo\Collection\TraversableTrait;

class MenuItem implements \ArrayAccess, \Countable, \IteratorAggregate
{
    use TraversableTrait;

    public function __construct(array $items)
    {
        $this->items = $items;
    }

/**
 * override magic get to return empty string for empty values and special names
 */
    public function __get($name)
    {
        if ('name' === $name) {
            $name = 'meal_item_name';
        }

        if (empty($this->items[$name])) {
            return '';
        }
        return $this->items[$name];
    }
}
