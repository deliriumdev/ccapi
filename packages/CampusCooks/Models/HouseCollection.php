<?php
/**
 * Campus Cooks API
 */
namespace CampusCooks\Models;

use Reo\Collection\TraversableTrait;

class HouseCollection implements \ArrayAccess, \Countable, \IteratorAggregate
{
    use TraversableTrait;

    protected $db;
    protected $services;

    public function __construct($db, $services)
    {
        $this->db = $db;
        $this->services = $services;
    }

/**
 * get
 * 
 * returns house data by ID
 */
    public function get($id)
    {
        $house = $this->db->get_row('select house_id as id, house_name as name, campus_id, has_offcampus as ocRestrict, max_lunch, max_dinner, lateplate_lunch, lateplate_dinner from cc_house where house_id=:id', [':id' => $id]);
        if (empty($house)) {
            return false;
        }
        $house['max_lunch'] = (int) $house['max_lunch'];
        $house['max_dinner'] = (int) $house['max_dinner'];
        $house['lateplate_lunch'] = (int) $house['lateplate_lunch'];
        $house['lateplate_dinner'] = (int) $house['lateplate_dinner'];
        return $house;
    }

/**
 * getByName
 * 
 * returns house data by Name
 */
    public function getByName($name)
    {
        // @note has_offcampus really means restrict off campus
        $house = $this->db->get_row('select house_id as id, house_name as name, campus_id, has_offcampus as ocRestrict, lateplate_lunch, lateplate_dinner from cc_house where house_name=:name', [':name' => $name]);
        if (empty($house)) {
            return false;
        }
        return $house;
    }

/**
 * get
 * 
 * returns house data by ID
 */
    public function getData($ids)
    {
        $sql = <<<'EOD'
select house_id as id, house_name as name, campus_id,
lateplate_lunch as lateplateLunch, lateplate_dinner as lateplateDinner, cutoff_lunch as cutoffLunch, cutoff_dinner as cutoffDinner 
from cc_house
EOD;
        $data = $this->db->get_array($sql .= ' where house_id in (' . implode(',', $ids) . ')');

        if (empty($data)) {
            return false;
        }
        return $data;
    }

/**
 * getByCampusName
 * 
 * returns house data by Campus Name
 * 
 * @note use wp groups by default
 */
    public function getByCampusName($name, $useGroups = true)
    {
        if (!$useGroups) {
            // $result = $this->db->execute('use c3campuscook');//@note remove after test
            $houses = $this->db->get_array($sql = 'select cc_house.house_id as id, cc_house.house_name as name from cc_house inner join cc_campus on cc_campus.campus_id = cc_house.campus_id where cc_campus.campus_name=:name', [':name' => $name]);
            if (empty($houses)) {
                return false;
            }
            return $houses;
        }
        // get wp prefix
        $wpPrefix = $this->services->get('Services')->get('Config')->get('database')->get('prefix');
        // get campus_id
        $campusId = $this->db->get_column('select group_id from ' . $wpPrefix . 'groups_group where name=:name', [':name' => $name]);
        if (empty($campusId)) {
            return false;
        }
        $houses = $this->db->get_array($sql = 'select group_id as id, name from ' . $wpPrefix . 'groups_group where parent_id = :id', [':id' => $campusId]);
        if (empty($houses)) {
            return false;
        }
        return $houses;
    }

/**
 * getByNames
 * 
 * returns house data by Multiple Campus Names
 * 
 * @note use wp groups by default
 */
    public function getByNames($names, $useGroups = true, $byId = false)
    {
        // each param in the in has to be tokenized
        $params = [];
        for ($ct = count($names), $xx = 0; $xx < $ct; $xx++) {
            $params[':name' . $xx] = $names[$xx];
        }
        if (!$useGroups) {
            // $result = $this->db->execute('use c3campuscook');//@note remove after test
            $sql = 'select cc_house.house_id as id, cc_house.house_name as name from cc_house inner join cc_campus on cc_campus.campus_id = cc_house.campus_id ';
            $sql .= ($byId ? 'where cc_campus.campus_id in (' . implode(',', array_keys($params)) . ')' : 'where cc_campus.campus_name in (' . implode(',', array_keys($params)) . ')');
            $houses = $this->db->get_array($sql, $params); 
            if (empty($houses)) {
                return false;
            }
            return $houses;
        }
        // get wp prefix
        $wpPrefix = $this->services->get('Services')->get('Config')->get('database')->get('prefix');
        if (!$byId) {
        // get campus_id
            $ids = $this->db->get_stack($sql = 'select group_id from ' . $wpPrefix . 'groups_group where name in ('. implode(',', array_keys($params)) .')', $params);
        }
        else {  // ids are passed in campus names
            $ids = $names;
        }
        if (empty($ids)) {
            // var_dump($sql, $this->db->err, $params);
            return false;
        }
        $houses = $this->db->get_array($sql = 'select group_id as id, name from ' . $wpPrefix . 'groups_group where parent_id in (' . implode(',', $ids) . ')');
        if (empty($houses)) {
            return false;
        }
        return $houses;
    }

/**
 * getCutoffHour
 * 
 * get the Lateplate cutoff hour
 * 
 * @param $houseId int the house ID
 */ 
    public function getCutoffHour($houseId, $mealtime)
    {
        if (false === ($cutoff = $this->db->get_column($sql = sprintf('select cutoff_%s from cc_house where house_id = %d', $mealtime, $houseId))) || empty($cutoff)) {
            return false;
        }
        return $cutoff;
    }

/**
 * getRecipients
 * 
 * get recipients by house
 * 
 * @param $houseId int the house ID
 */ 
    public function getRecipients($houseId, $campusId, $roles = null)
    {
        //this imports users if necessary and pulls back the wp users
        $users = $this->services->get('WpImport')->getHouseUsers($houseId, $campusId, $roles);
        if (empty($users)) {
            return false;
        }
        // get an array of IDs and use this for our pull rather than the house id
        $in = implode(',', array_map(function($item){
            return $item['ID'];
        }, $users));

        // only select those active in WP @note both queries could be run to sync, but probably not necessary
        $sql = 'select * from cc_user where house_id = :house_id and user_id in (' . $in .')';
        $rows = $this->db->get_array($sql, [':house_id' => $houseId]);
        // get local data not in users to possibly sync
        $sync = $this->db->get_array('select * from cc_user where house_id = :house_id and user_id not in (' . $in .') and roles & :roles', [':house_id' => $houseId, ':roles' => array_sum(array_keys($roles))]);
        if (!empty($sync)) {
            $this->services->get('WpImport')->syncUsers($sync, $this->services->get('Services')->get('Config')->get('api')->get('report_roles'));
        }
        if (empty($rows)) {
            return false;
        }
        return array_map(function($item) {
            // swift wants an array
            return [$item['email'] => sprintf('%s %s', $item['first_name'], $item['last_name'])];
            return sprintf('%s %s <%s>', $item['first_name'], $item['last_name'], $item['email']);
        }, $rows);
    }

    public function updateSettings($id, $data)
    {
        $sql = 'update cc_house set lateplate_lunch=:lateplateLunch, lateplate_dinner=:lateplateDinner, cutoff_lunch=:cutoffLunch, cutoff_dinner=:cutoffDinner where house_id = :id';
        $this->db->execute($sql, [':lateplateDinner' => $data['lateplateDinner'], ':lateplateLunch' => $data['lateplateLunch'], ':cutoffDinner' => $data['cutoffDinner'], ':cutoffLunch' => $data['cutoffLunch'], ':id' => $id]);
        return true;
    }
}
