<?php
/**
 * Campus Cooks API
 */
namespace CampusCooks\Models;

class AttendingCollection extends AbstractEntryCollection
{
    protected $start;

    protected static $mealtimes = [
        1 => 'Lunch',
        2 => 'Dinner',
        3 => 'Breakfast',
    ];

    public function __construct($db, $services)
    {
        parent::__construct($db, $services);
        if (null === ($this->start = $services->get('Services')->get('Config')->get('api')->get('start')) || empty($this->start)) {
            $this->start = false;
        }
    }
/**
 * getToday
 * 
 * get todays attendings by mealtime
 * 
 * @param $mealtime string the meal time, dinner|lunch
 */ 
    public function getToday($mealtime, $tz, $sort = true)
    {
        $sql = <<<'EOD'
select entry.student_id, entry.house_id, entry.campus_id, attending.*, DATE_FORMAT(attending.request_date, '%m/%d/%Y') as formatted_date, 
DATE_FORMAT(CURDATE(), '%m/%d/%Y') as recur_date, campus.tz, campus.campus_name, house.house_name, user.first_name, user.last_name, user.in_house 
from (((request_entry entry inner join attending_entry attending on attending.entry_id = entry.entry_id) 
inner join cc_campus campus on campus.campus_id = entry.campus_id and campus.tz=:tz) left join cc_house house on house.house_id = entry.house_id)
left join cc_user user on user.user_id = entry.student_id 
where (attending.request_date = CURDATE() or 
(DAYOFWEEK(CURDATE()) = DAYOFWEEK(attending.request_date) and recurring_stop_date > CURDATE() and is_recurring = 1)) and attending.meal_time = :mealtime 
EOD;
        $rows = $this->db->get_array($sql, $params = [':tz' => $tz, ':mealtime' => 'lunch' === $mealtime ? 1 : ('breakfast' === $mealtime ? 3 : 2)]);
        if (empty($rows)) {
            //var_dump($sql, $this->db->err);
            return false;
        }

        // populate the tables that may not have data from legacy wp groups
        $attendings = [];
        $students = $this->services->get('Students'); // to get inhouse from wp meta
        foreach ($rows as $key => $data) {
            if ('1' === $data['is_recurring']) {
                $rows[$key]['formatted_date'] = $data['recur_date'];
            }
            $rows[$key]['mealtime'] = isset(self::$mealtimes[$data['meal_time']]) ? self::$mealtimes[$data['meal_time']] : 'Dinner';
        }
        if (is_null($data['in_house'])) {
            $rows[$key]['in_house'] = $students->isInHouse($data['student_id']);
        }
        $this->populateItems($rows, $sort);
        return $this;
    }

    public function get($houses, $all = false)
    {
        $sql = <<<'EOD'
select entry.student_id, entry.house_id, entry.campus_id, attending.*, DATE_FORMAT(attending.request_date, '%a %m/%d/%Y') as formatted_date, 
DATE_FORMAT(CURDATE(), '%m/%d/%Y') as recur_date, campus.tz, campus.campus_name, house.house_name, user.first_name, user.last_name, user.in_house, DATE_FORMAT(entry.entry_date, '%m/%d/%Y') as entry_date  
from (((request_entry entry inner join attending_entry attending on attending.entry_id = entry.entry_id) 
inner join cc_campus campus on campus.campus_id = entry.campus_id) inner join cc_house house on house.house_id = entry.house_id)
left join cc_user user on user.user_id = entry.student_id
EOD;
        $conds = (false !== $this->start) ? ['attending.request_date > \'' . $this->start . '\''] : [];
        if (!$all) {
            $conds[] = '(attending.request_date = CURDATE() or (DAYOFWEEK(CURDATE()) = DAYOFWEEK(attending.request_date) and recurring_stop_date > CURDATE() and is_recurring = 1))';
        }
        if (false !== $houses) {
            $conds[] = 'entry.house_id in (' . implode(',', $houses) . ')';
        }
        if (!empty($conds)) {
            $sql .= ' where ' . implode(' and ', $conds);
        }
        $rows = $this->db->get_array($sql);
        if (empty($rows)) {
            //var_dump($sql, $this->db->err);
            return false;
        }
        $students = $this->services->get('Students');// to get inhouse from wp meta
        // populate the tables that may not have data from legacy wp groups
        foreach ($rows as $key => $data) {
            if ('1' !== $data['is_recurring']) {
                unset($rows[$key]['stop_date']);
            }
            $rows[$key]['is_recurring'] = (int) $data['is_recurring'];
            if ($data['is_recurring']) {
                $rows[$key]['formatted_date'] = $today;
            }
            $rows[$key]['mealtime'] = isset(self::$mealtimes[$data['meal_time']]) ? self::$mealtimes[$data['meal_time']] : 'Dinner';
            if (empty($data['last_name']) && false !== ($user = $this->services->get('WpImport')->importUser($data['student_id'], $data['campus_id'], $data['house_id']))) {
                $rows[$key]['first_name'] = $user['first_name'];
                $rows[$key]['last_name'] = $user['last_name'];
            }
            $rows[$key]['student'] =  $rows[$key]['first_name'] . ' ' . $rows[$key]['last_name'];
            $rows[$key]['sortname'] = $rows[$key]['last_name'] . ' ' .  $rows[$key]['first_name'];
            if (is_null($data['in_house'])) {
                $rows[$key]['in_house'] = $students->isInHouse($data['student_id']);
            }
            //$rows[$key]['house_name'] .= ' ' . $data['house_id'];
            unset($rows[$key]['first_name'], $rows[$key]['last_name']);
        }
        return $rows;
    }

    public function getCount($studentId, $weekOf, $mealTime)
    {
        $meal = 'Lunch' === $mealTime ? 1 : 2; // lunch of dinner
        $sql = 'select attending.attending_id, attending.request_date from request_entry entry inner join attending_entry attending on attending.entry_id = entry.entry_id';
        $sql .= ' where attending.meal_time = ' . $meal . ' and attending.request_date >= '. ($date = $this->db->quote($weekOf)) . ' and attending.request_date < DATE_ADD(' . $date . ', INTERVAL 7 DAY) and entry.student_id=:student_id';
        if (false === ($data = $this->db->get_array($sql, [':student_id' => $studentId]))) {
            // var_dump($sql, $this->db->err);
            return 0;
        }
        // var_dump($data);
        return count($data);
    }

    public function getAvg($houseIds, $total = true)
    {
        // @note where request_date > certian date does not work with aggregate group by week attending.request_date as reqDate
        $sql = 'select count(attending.attending_id) as ct, WEEK(attending.request_date) as weekNo from attending_entry attending inner join request_entry entry on entry.entry_id = attending.entry_id';
        $sql .= ' where year(attending.request_date) = 2017 and entry.house_id in (' . implode(',', $houseIds) . ') group by WEEK(attending.request_date)';
        // var_dump($sql);
        if (false === ($data = $this->db->get_array($sql)) || empty($data)) {
            // var_dump($sql, $this->db->err);
            return $total ? 0 : false;
        }
        if (!$total) {
            return $data;
        }
        // var_dump($data);
        $flat = array_map(function($item){ return $item['ct'];}, $data);
        return round(array_sum($flat) / count($flat), 0);
    }

    public function getAvgByWeekday($houseIds, $weekOf)
    {
        $data = [];
        // @note where request_date > certian date does not work with aggregate group by week
        $sql = <<<'EOD'
select count(att.attending_id) as ct, WEEKDAY(att.request_date) as weekDay, house.house_name from 
attending_entry att inner join request_entry entry on entry.entry_id = att.entry_id 
inner join cc_house house on house.house_id = entry.house_id
EOD;
        foreach ($houseIds as $id) { // get series data for each house
            $where = ' where year(att.request_date) = 2017 and entry.house_id = ' . $id . ' group by WEEKDAY(att.request_date)';
            if (false === ($rows = $this->db->get_array($query = $sql . $where, null, 'weekDay')) || empty($rows)) {
                // var_dump($query, $this->db->err);exit;
                continue;
            }
            // fill in any missing weekdays
            $sample = current($rows);
            $house = $sample['house_name'];
            for ($xx = 0; $xx < 7; $xx++) {
                if (empty($rows[$xx])) {
                    $rows[$xx] = ['ct' => 0];
                }
            }
            ksort($rows); // the pushed numeric keys will not be in order so it has to be sorted
            $data[] = ['house' => $house, 'data' => array_map(function($item){return (int) $item['ct'];}, $rows)];
        }
        return $data;
    }

    public function getPlateAvg($houseIds, $isLateplate = false)
    {
        $sql = <<<'EOD'
select round(avg(plates)) as ct, dow, house.house_name from cc_meals_served served inner join cc_meals_served_day servedDay on servedDay.serveID = served.serveID 
inner join cc_house house on house.house_id = served.houseID 
where served.houseID = :houseID group by served.houseID, servedDay.dow order by servedDay.dow
EOD;
        if ($isLateplate) {
            $sql = str_replace('avg(plates)', 'avg(latePlates)', $sql);
        }
        $data = [];
        foreach ($houseIds as $id) {
            $rows = $this->db->get_array($sql, [':houseID' => $id]);
            if (empty($rows)) {
                continue;
            }
            $sample = current($rows);
            $house = $sample['house_name'];
            $data[] = ['house' => $house, 'data' => array_map(function($item){return (int) $item['ct'];}, $rows)];
            // var_dump($sql, $this->db->err);
        }
        if (empty($data)) {
            return false;
        }
        return $data;
    }

    public function getPlates($houseId, $weekOf, $isLateplate = false)
    {
        $sql = 'select servedDay.' . ($isLateplate ? 'latePlates' : 'plates') . ' from cc_meals_served served inner join cc_meals_served_day servedDay on servedDay.serveID = served.serveID where weekOf=:weekOf and houseID=:houseID order by servedDay.dow asc';
        $data = $this->db->get_stack($sql, $params = [':weekOf' => $weekOf, ':houseID' => $houseId]);
        if (empty($data)) {
            // var_dump($sql, $params, $this->db->err);
            return false;
        }
        return array_map(function($item){return (int) $item;}, $data);
    }

    public function updatePlateCounts($houseId, $weekOf, array $values, $isLateplate = false)
    {
        $sql = 'select served.serveID as id from cc_meals_served served inner join cc_meals_served_day servedDay on servedDay.serveID = served.serveID where weekOf=:weekOf and houseID=:houseID order by servedDay.dow asc limit 0,1';
        $data = $this->db->get_row($sql, [':weekOf' => $weekOf, ':houseID' => $houseId]);
        if (!empty($data)) {
            $serveId = $data['id'];
            foreach ($values as $key => $ct) {
                $this->db->execute($sql = 'update cc_meals_served_day set ' . ($isLateplate ? 'latePlates' : 'plates') . ' = :value where dow = :dow and serveID=:serveID', $params = [':serveID' => $serveId, ':dow' => $key + 1, ':value' => $ct]);
                // var_dump($sql, $params, $this->db->err);
            }
            return true;
        }
        
        $serveId = $this->db->execute('insert into cc_meals_served (houseID, weekOf) values(:houseID, :weekOf)', [':weekOf' => $weekOf, ':houseID' => $houseId]);
        foreach ($values as $key => $ct) {
            $this->db->execute('insert into cc_meals_served_day (serveID, dow, ' . ($isLateplate ? 'latePlates' : 'plates') . ') values(:serveID, :dow, :value)', [':serveID' => $serveId, ':dow' => $key + 1, ':value' => $ct]);
        }
        return true;
    }
}
