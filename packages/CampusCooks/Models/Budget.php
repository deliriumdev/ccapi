<?php
/**
 * Campus Cooks API
 */
namespace CampusCooks\Models;

use Reo\Collection\TraversableTrait;

class Budget
{
    public function __construct($id)
    {
        
        if (isset($fields)) {
            $this->items = $values;
        }
    }

    public static function getSearchFields()
    {
        return [
            'userId' => ['constraints' => ['Digit'], 'key' => 'user_id', 'default' => 0],
            'weekOf' => ['constraints' => ['Date'], 'presence' => false, 'optional' => true, 'key' => 'weekOf', 'default' => 0],
            'allWeeks' => ['constraints' => ['Digit'], 'presence' => false, 'optional' => true, 'key' => 'allWeeks', 'default' => 0],
            'currentWeek' => ['constraints' => ['Digit'], 'presence' => false, 'optional' => true, 'key' => 'currentWeek', 'default' => 0],
            'budgetId' => ['constraints' => ['Digit'], 'presence' => false, 'optional' => true, 'key' => 'invID', 'default' => 0],
            'dataType' => ['constraints' => ['Text'], 'presence' => false, 'optional' => true, 'key' => 'dataType', 'default' => ''],
            'budgetType' => ['constraints' => ['Text'], 'presence' => false, 'optional' => true, 'key' => 'type', 'default' => ''],
            'signature' => ['constraints' => ['Text'], 'presence' => false, 'optional' => true, 'key' => 'token', 'default' => ''],
            'houseIds' => ['presence' => false, 'optional' => true, 'key' => 'ids', 'default' => []],
            'houseId' => ['constraints' => ['Digit'], 'presence' => false, 'optional' => true, 'key' => 'house_id', 'default' => 0],
            'gfFormId' => ['constraints' => ['Digit'], 'presence' => false, 'optional' => true, 'key' => 'form_id', 'default' => 0],
            'start' => ['constraints' => ['Digit'], 'presence' => false, 'optional' => true, 'key' => 'start', 'default' => 0],
            'length' => ['constraints' => ['Digit'], 'presence' => false, 'optional' => true, 'key' => 'length', 'default' => 50],
        ];
    }

    public static function getUpdateFields()
    {
        return [
            'userId' => ['constraints' => ['Digit'], 'key' => 'user_id', 'default' => 0],
            'signature' => ['constraints' => ['Text'], 'presence' => false, 'optional' => true, 'key' => 'token', 'default' => ''],
            'itemId' => ['constraints' => ['Digit'], 'presence' => true, 'optional' => false, 'key' => 'pk', 'default' => 0],
            'fieldName' => ['constraints' => ['Text'], 'presence' => true, 'optional' => false, 'key' => 'name', 'default' => ''],
            'value' => ['presence' => true, 'optional' => false, 'key' => 'value', 'default' => ''],
        ];
    }
}
