<?php
/**
 * Campus Cooks API
 */
namespace CampusCooks\Models;

use Reo\Collection\TraversableTrait;

abstract class AbstractEntryCollection implements \ArrayAccess, \Countable, \IteratorAggregate
{
    use TraversableTrait;

    protected $db;
    protected $services;
    protected $recipients;
    protected $populated = false;

    public function __construct($db, $services)
    {
        $this->db = $db;
        $this->services = $services;
    }

    public function rewind()
    {
        reset($this->items);
    }

    public function populateItems($rows, $sort = true)
    {
        $this->populated = false;
        $items = [];
        foreach ($rows as $key => $data) {
            if (empty($data['tz'])) {
                $campus = $this->services->get('WpImport')->importCampus($data['campus_id']);
                $rows[$key]['tz'] = $campus['tz'];
                $rows[$key]['campus_name'] = $campus['name'];
                $this->populated = true;
            }
            if (empty($data['house_name'])) {
                $rows[$key]['house_name'] = $this->services->get('WpImport')->importHouse($data['house_id']);
                $this->populated = true;
            }
            if (empty($data['last_name'])) {
                $user = $this->services->get('WpImport')->importUser($data['student_id'], $data['campus_id'], $data['house_id']);
                $rows[$key]['first_name'] = $user['first_name'];
                $rows[$key]['last_name'] = $user['last_name'];
                $this->populated = true;
            }
            $items[$data['house_id']][] = $rows[$key];
        }
        if ($sort) {
            foreach ($items as &$item) {
                usort($item, function($a, $b){
                    if ($a['house_name'] == $b['house_name']) {
                        if ($a['last_name'] == $b['last_name']) {
                            return strcasecmp ($a['first_name'], $b['first_name']);
                        }
                        return strcasecmp ($a['last_name'], $b['last_name']);
                    }
                    return strcasecmp ($a['house_name'],$b['house_name']);
                });
            }
        }
        $this->items = $items;
    }

    public function setRecipients($houseId, $recipients)
    {
        if (!isset($this->items[$houseId])) {
            $this->services->get('Services')->get('Logger')->error(sprintf('Item [%s] does not exist', $houseId));
            return;
            throw new InvalidArgumentException(sprintf('Item [%s] does not exist', $houseId));
        }
        $this->recipients[$houseId] = $recipients;
    }

    public function getRecipients($houseId)
    {
        return empty($this->recipients[$houseId]) ? false : $this->recipients[$houseId];
    }

    public function recordSend($houseId, $subject = 'n/a')
    {
        if (!isset($this->items[$houseId])) {
            echo "$houseId not found\n";
            return false;
        }
        $sql = 'INSERT INTO cc_report_sent (sentID, house_id, subject, recipient, sent_date) VALUES (NULL, :house_id, :subject, :recipient, NOW())';
        if (false === ($sentId = $this->db->execute($sql, [':house_id' => $houseId, ':subject' => $subject, ':recipient' => empty($this->recipients[$houseId]) ? '0': json_encode($this->recipients[$houseId])])) || empty($sentId)) {
            $sentId = 0;
        }
        foreach ($this->items[$houseId] as $item) {
            $this->db->execute('update request_entry set sent_date = NOW(), is_sent = 1, sentID = :sentID  where entry_id = :id', [':sentID' => $sentId, ':id' => $item['entry_id']]);
        }
        return true;
    }

    public function hasPopulated()
    {
        return $this->populated;
    }
}
