<?php
/**
 * Campus Cooks API
 */
namespace CampusCooks\Models\Reports;
use Reo\Collection\TraversableTrait;

class Email
{

    protected $db;
    protected $services;
    
    public function __construct($db, $services)
    {
        $this->db = $db;
        $this->services = $services;
    }

    public function getData(array $houses)
    {
        
        $sql = <<<'EOD'
select rep.*, DATE_FORMAT(sent_date, '%a %b %e, %y %h:%i %p') as date_sent, house.house_name from cc_report_sent rep 
inner join cc_house house on house.house_id = rep.house_id 
where DATE(rep.sent_date) > (NOW() - INTERVAL 7 DAY) and rep.house_id in (:ids) order by rep.house_id, rep.sent_date desc
EOD;
        $rows = $this->db->get_array(str_replace(':ids', implode(',', $houses), $sql));
        if (empty($rows)) {
            // var_dump($sql, $this->db->err);
            return [];
        }
        // @note recepients is json
        return array_map(function($item){$item['recipient'] = json_decode($item['recipient'], true);return $item;}, $rows);
    }
}
