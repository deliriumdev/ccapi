<?php
/**
 * Campus Cooks API
 */
namespace CampusCooks\Models;

use Reo\Collection\TraversableTrait;

class Menu
{
    public function __construct($id)
    {
        
        if (isset($fields)) {
            $this->items = $values;
        }
    }

    public static function getFields()
    {
        return [
            'campusId'  => ['constraints' => ['Digit'], 'optional' => true, 'presence' => false, 'key' => 'campusFilter', 'default' => false],
            'houseId'   => ['constraints' => ['Digit'], 'optional' => true, 'presence' => false, 'key' => 'house_id', 'default' => false],
            'houses'    => ['optional' => true, 'presence' => false, 'key' => 'houses', 'default' => false],
            'startDate' => ['constraints' => ['Date'], 'optional' => true, 'key' => 'menuDate1'],
            'endDate'   => ['constraints' => ['Date'], 'optional' => true, 'key' => 'menuDate2'],
            'keyword'   => ['filter' => 'CampusCooks\Models\EntryFactory::keywordFilter', 'optional' => true, 'presence' => false, 'key' => 'menuKeyword'],
            'page'      => ['constraints' => ['Digit'], 'optional' => true, 'presence' => false, 'default' => 1, 'key' => 'p'], //@note not hitting default when not present
            'fullWeek'  => ['constraints' => ['Digit'], 'optional' => true, 'presence' => false, 'default' => 0, 'key' => 'full'],
        ];
    }
}
