<?php
/**
 * Campus Cooks API
 */
namespace CampusCooks\Models;

use Reo\Collection\TraversableTrait;

class Lateplate
{
    public function __construct($id)
    {
        
        if (isset($fields)) {
            $this->items = $values;
        }
    }

    public static function getFields()
    {
        return [
            'studentId' => ['constraints' => ['Digit'], 'key' => 'student_id'],
            'studentName' => ['constraints' => ['Text', ['MinLength', 2]], 'key' => 'input_1'],
            'mealTime'    => ['constraints' => ['Text'], 'key' => 'input_2'],
            'requestDate' => ['filter' => 'CampusCooks\Models\EntryFactory::formatDate', 'key' => 'input_3'],
            'campusName'  => ['constraints' => ['Text'], 'optional' => true, 'key' => 'input_4'],
            'houseName'   => ['constraints' => ['Text'], 'optional' => true, 'key' => 'input_5'],
            'chefMessage' => ['constraints' => ['Text'], 'optional' => true, 'key' => 'input_6'],
            'isRecurring' => ['filter' => 'CampusCooks\Models\EntryFactory::formatBool', 'presence' => false, 'optional' => true, 'key' => 'input_10', 'default' => 0],
            'recurringStopDate' => ['filter' => 'CampusCooks\Models\EntryFactory::formatDate', 'optional' => true, 'key' => 'input_11'],
        ];
    }

    public static function getSearchFields()
    {
        return [
            'houseId' => ['constraints' => ['Digit'], 'optional' => true, 'presence' => false, 'key' => 'house_id', 'default' => false],
            'userId' => ['constraints' => ['Digit'], 'key' => 'user_id', 'default' => 0],
            'days' => ['constraints' => ['Digit'], 'optional' => true, 'presence' => false, 'key' => 'days', 'default' => 0],
        ];
    }
}
