<?php
/**
 * Campus Cooks API
 */
namespace CampusCooks\Models;

use Reo\Collection\TraversableTrait;

class House
{
    public function __construct($id)
    {
        if (isset($fields)) {
            $this->items = $values;
        }
    }

    public static function getSearchFields()
    {
        return [
            'userId' => ['constraints' => ['Digit'], 'presence' => false, 'optional' => true, 'key' => 'user_id', 'default' => 0],
            'signature' => ['constraints' => ['Text'], 'presence' => false, 'optional' => true, 'key' => 'token', 'default' => ''],
            'houseId' => ['constraints' => ['Digit'], 'optional' => true, 'presence' => false, 'key' => 'house_id', 'default' => false],
            'house' => ['constraints' => ['Text'], 'optional' => true, 'presence' => false, 'key' => 'house', 'default' => false],
            'campusName' => ['constraints' => ['Text'], 'optional' => true, 'presence' => false, 'key' => 'campus_name', 'default' => false],
            'campuses' => ['optional' => true, 'presence' => false, 'key' => 'campus_names', 'default' => false],
            'campusIds' => ['optional' => true, 'presence' => false, 'key' => 'campus_ids', 'default' => false],
        ];
    }

    public static function getDataFields()
    {
        return [
            'houseIds' => ['key' => 'ids', 'default' => false],
        ];
    }

    public static function getLateplateFields()
    {
        return [
            'lateplateLunch' => ['constraints' => ['Digit'], 'key' => 'lateplateLunch', 'default' => 0],
            'cutoffLunch' => ['constraints' => ['Digit', ['Betwixt', [8, 16]]], 'key' => 'cutoffLunch', 'default' => false],
            'lateplateDinner' => ['constraints' => ['Digit'], 'key' => 'lateplateLunch', 'default' => 0],
            'cutoffDinner' => ['constraints' => ['Digit'], 'key' => 'cutoffLunch', 'default' => false],
        ];
    }
}
