<?php
/**
 * Campus Cooks API
 */
namespace CampusCooks\Models;

use Reo\Collection\TraversableTrait;

class Student
{
    use TraversableTrait;

    protected $id;

    public function __construct($id, array $data)
    {
        $this->id = $id;
        $this->items = $data;
    }
}
