<?php
/**
 * Campus Cooks API
 */
namespace CampusCooks\Models;

class RatemealCollection extends AbstractEntryCollection
{
    protected static $mealtimes = [
        1 => 'Lunch',
        2 => 'Dinner',
        3 => 'Breakfast',
    ];

    protected $start;

    public function __construct($db, $services)
    {
        parent::__construct($db, $services);
        if (null === ($this->start = $services->get('Services')->get('Config')->get('api')->get('start')) || empty($this->start)) {
            $this->start = false;
        }
    }
/**
 * getData
 * 
 * get todays ratings
 * 
 * @param $tz the local time zone
 */ 
    public function getData($tz, $sort = true)
    {
        $sql = <<<'EOD'
select entry.student_id, entry.house_id, entry.campus_id, ratemeal.*, 
campus.tz, campus.campus_name, house.house_name, user.first_name, user.last_name  
from (((request_entry entry inner join ratemeal_entry ratemeal on ratemeal.entry_id = entry.entry_id) 
inner join cc_campus campus on campus.campus_id = entry.campus_id and campus.tz=:tz) left join cc_house house on house.house_id = entry.house_id)
left join cc_user user on user.user_id = entry.student_id where ratemeal.item_name != '' and entry.is_sent = 0 and entry.inactive = 0
EOD;
        $rows = $this->db->get_array($sql, $params = [':tz' => $tz]);
        if (empty($rows)) {
            // var_dump($sql, $this->db->err);
            return false;
        }

        $this->populateItems($rows, $sort);
        // populate the tables that may not have data from legacy wp groups
        return $this;
    }

/**
 * get
 * 
 * Gets data based on house ID
 * @note the menu ID in ratemeal corresponds to the lead id in menu schedule and not the actual meal id
 */ 
    public function get($houses)
    {
        $sql = <<<'EOD'
select entry.student_id, entry.house_id, entry.campus_id, DATE_FORMAT(entry.entry_date, '%m/%d/%Y') as formatted_date, ratemeal.*, 
campus.tz, campus.campus_name, house.house_name, user.first_name, user.last_name, DATE_FORMAT(sched.schedule_date, '%m/%d/%Y') as meal_date, sched.meal_time 
from ((((request_entry entry inner join ratemeal_entry ratemeal on ratemeal.entry_id = entry.entry_id) 
inner join cc_campus campus on campus.campus_id = entry.campus_id) inner join cc_house house on house.house_id = entry.house_id)
inner join cc_meal_schedule sched on sched.lead_id = ratemeal.menu_id)
left join cc_user user on user.user_id = entry.student_id where ratemeal.item_name != ''
EOD;
        $conds = (false !== $this->start) ? ['sched.schedule_date > \'' . $this->start . '\''] : [];

        if (false !== $houses) {
            $conds[] = 'entry.house_id in (' . implode(',', $houses) . ')';
        }
        if (!empty($conds)) {
            $sql .= ' and ' . implode(' and ', $conds);
        }
        $sql .= ' and entry.inactive = 0';
        $rows = $this->db->get_array($sql);
        if (empty($rows)) {
            // var_dump($sql, $this->db->err);
            return false;
        }

         foreach ($rows as $key => $data) {
            if (empty($data['last_name']) && false !== ($user = $this->services->get('WpImport')->importUser($data['student_id'], $data['campus_id'], $data['house_id']))) {
                $rows[$key]['first_name'] = $user['first_name'];
                $rows[$key]['last_name'] = $user['last_name'];
            }
            $rows[$key]['student'] = $data['first_name'] . ' ' . $data['last_name'];
            $rows[$key]['sortname'] = $data['last_name'] . ' ' . $data['first_name'];
            $rows[$key]['meal_time'] = isset(self::$mealtimes[$data['meal_time']]) ? self::$mealtimes[$data['meal_time']] : 'Dinner';
            //$rows[$key]['house_name'] .= ' ' . $data['house_id'];
            unset($rows[$key]['first_name'], $rows[$key]['last_name']);
        }
        return $rows;
    }

    public function getRating($studentId, $menuId, $mealName)
    {
        $sql = <<<'EOD'
select ratemeal.rating from request_entry entry inner join ratemeal_entry ratemeal on ratemeal.entry_id = entry.entry_id 
where ratemeal.item_name = :item_name and ratemeal.menu_id = :menu_id and entry.student_id = :student_id
EOD;
        $rating = $this->db->get_column($sql, $params = ['item_name' => $mealName, 'menu_id' => $menuId, 'student_id' => $studentId]);
        if (empty($rating)) {
            // var_dump($sql, $params, $this->db->err);
            return false;
        }
        return (int) $rating;
    }

    public function getGfData($data)
    {
        $menuId = $data['field_filters'][0]['value'];
        $studentName = $data['field_filters'][1]['value'];
        $houseName = $data['field_filters'][3]['value'];
        $mealName = $data['field_filters'][4]['value'];
        // try to determine the student from the above data and wonder why a developer would use names rather the ids for anything
        if (false === ($house = $this->services->get('Houses')->getByName($houseName))) {
            return false;
        }
        if (false === ($student = $this->services->get('Students')->getStudentGf($studentName, $house['id']))) {
            return false;
        }
        return ['student' => $student, 'house' => $house, 'menuId' => $menuId, 'meal' => $mealName];
        //var_dump($menuId, $studentName, $houseName, $mealName, $house);exit;
    }
}
