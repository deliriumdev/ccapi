<?php
/**
 * Campus Cooks API
 */
namespace CampusCooks\Models;

use Reo\Collection\TraversableTrait;

class StudentCollection implements \ArrayAccess, \Countable, \IteratorAggregate
{
    use TraversableTrait;

    protected $db;
    protected $wpDbPrefix;

    public function __construct($db, $wpDbPrefix = 'wp_')
    {
        $this->db = $db;
        $this->wpDbPrefix = $wpDbPrefix; 
    }

    public function get($id)
    {
        //@note the id passed by the app is the meta entry id, get the real user ID
        if (false === ($userId = $this->db->get_column('select user_id from ' . $this->wpDbPrefix . 'usermeta where meta_value=:meta_value and meta_key=:meta_key', [':meta_value' => $id, ':meta_key' => 'entry_id']))) {
            return false;
        }
        if (isset($this->items[$userId])) {
            return $this->items[$userId];
        }
        $group = $this->db->get_row($sql = 'SELECT gg.* FROM ' . $this->wpDbPrefix . 'groups_user_group ug inner join ' . $this->wpDbPrefix . 'groups_group gg on gg.group_id = ug.group_id and ug.user_id = :id and gg.parent_id is not null and gg.group_id!=1 ', [':id' => $userId]);
        if (empty($group)) {
            return false;
        }
        return $this->items[$userId] = new Student((int) $userId, ['id' => $userId, 'houseId' => $group['group_id'], 'campusId' => $group['parent_id']]);
    }

    public function getStudentGf($name, $houseId)
    {
        $sql = <<<'EOD'
select * from cc_user where CONCAT_WS(' ', first_name, last_name) = :name and house_id = :house_id
EOD;
        $data = $this->db->get_row($sql, $params = [':name' => $name, 'house_id' => $houseId]);
        if (empty($data)) {
            // var_dump($sql, $params, $this->db->err);
            return false;
        }
        return new Student((int) $data['user_id'], ['id' => $data['user_id'], 'houseId' => $data['house_id'], 'campusId' => $data['campus_id']]);
    }
}
