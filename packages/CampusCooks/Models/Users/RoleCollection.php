<?php
/**
 * Campus Cooks API
 */
namespace CampusCooks\Models\Users;

use Reo\Collection\TraversableTrait;

class RoleCollection implements \ArrayAccess, \Countable, \IteratorAggregate
{
    use TraversableTrait;

    protected $db;
    protected $services;
    protected $roles;
    protected $reportRoles;
    protected $wpDbPrefix;

    public function __construct($db, $wpDbPrefix = 'wp_', $services)
    {
        $this->reportRoles = array_values($services->get('Services')->get('Config')->get('api')->get('report_roles')->toArray());
        $this->roles = $services->get('Services')->get('Config')->get('api')->get('roles');
        $this->db = $db;
        $this->wpDbPrefix = $wpDbPrefix;
        $this->services = $services;
    }

/**
 * getRole
 */
    public function getCampusAssigned($userId)
    {
        // meta key for campus ids
        $sql = 'select meta_value from ' . $this->wpDbPrefix . 'usermeta um where meta_key = \'cc_campus\' and user_id = ' . $userId;
        if (false === ($campusIds = $this->db->get_column($sql)) || empty($campusIds) || false === ($campusIds = unserialize($campusIds)) || empty($campusIds)) {
            return false;
        }
        return $campusIds;
    }

/**
 * getHouseAccess
 * 
 * @return $houses array of allow houses ([0] no perms or access), false for administrator (access to all)
 */ 
    public function getHouseAccess(User $user)
    {
        if (!empty($this->items[$user->id])) {
            return $this->items[$user->id];
        }
        if (isset($user->roles['cc_admin']) || isset($user->roles['administrator'])) {
            return $this->items[$user->id] = false; // access to all houses
        }
        // access to their house
        if (isset($user->roles['chefs']) || isset($user->roles['trainer'])) {
            return $this->items[$user->id] = empty($user->houses) ? 0 : $user->houses;
        }
        // access to all houses at their campus
        if (isset($user->roles['cod']) || isset($user->roles['campuscookssupervisors'])) {// || isset($user->roles['administrator']
            if (false === ($houses = $this->getHouses($user->campus))) {
                return $this->items[$user->id] = [0];
            }
            return $this->items[$user->id] = $houses;
        }
        return [0];
    }

    /**
 * getHouseAccess
 * 
 * @return $houses array of allow houses ([0] no perms or access), false for administrator (access to all)
 */ 
    public function getCampusAccess(User $user)
    {
        if (!empty($this->items[$user->id])) {
            return $this->items[$user->id];
        }
        // super admins
        if (isset($user->roles['cc_admin']) || isset($user->roles['administrator'])) {
            return $this->services->get('Campus')->getAll(); // all campuses
        }
        // access to all houses at their campus
        if (isset($user->roles['cod']) || isset($user->roles['campuscookssupervisors'])) {// || isset($user->roles['administrator']
            /* @depricated instead this needs all campus for the houses that are assigned which is handled in the user object creation
            if (false !== ($assigned = $this->getCampusAssigned($user->id))) {
                $user->campus = $assigned;
            }*/
            $db = $this->db;
            $data =  array_map(function($id) use ($db) {
                $name = $db->get_column($sql = 'select campus_name from cc_campus where campus_id = ' . $id);
                return ['id' => (int) $id, 'text' => $name];
            }, $user->campus);
            usort($data, function($a, $b) { // disregard the univ prefix
                return strnatcasecmp(str_replace('University of ', '', $a['text']), str_replace('University of ', '', $b['text']));
            });
            return $data;
        }
        /*// access to their house, so only one campus skip
        if (isset($user->roles['chefs']) || isset($user->roles['trainer'])) {
            return false; // by business rules only one campus
        }*/
        return false;
    }

    public function getReportUsers($houseId, $campusId)
    {
        $ids = $this->db->get_stack($sql = 'SELECT ug.user_id FROM ' . $this->wpDbPrefix . 'groups_user_group ug where ug.group_id = :id', [':id' => $houseId]);
        if (empty($ids)) {
            return false;
        }
        $importer = $this->services->get('WpImport');
        $users = [];
        foreach ($ids as $userId) {// check perms
            $roles = $this->db->get_column('select meta_value from ' . $this->wpDbPrefix . 'usermeta um where meta_key = \''. $this->wpDbPrefix . 'capabilities\' and user_id = ' . $userId);
            if (empty($roles)) {
                continue;
            }
            foreach ($this->reportRoles as $match) {
                if (strstr($roles, $match)) {
                    if (false === ($roles = unserialize($roles))) {
                        continue;
                    }
                    $bitwiseRoles = 0;
                    foreach ($this->roles as $key => $nbr) {
                        if (isset($roles[$key])) {
                            $bitwiseRoles += $nbr;
                        }
                    }
                    // this will check if user exists and return their data
                    $users[] = $importer->importUser($userId, $campusId, $houseId, $bitwiseRoles);
                    break;
                }
            }
            /*if (empty($roles) || false === ($roles = unserialize($roles))) {
                continue;
            }*/
        }
        return array_map(function($item) {
            // swift wants an array
            return [$item['email'] => sprintf('%s %s', $item['first_name'], $item['last_name'])];
            return sprintf('%s %s <%s>', $item['first_name'], $item['last_name'], $item['email']);
        }, $users);
    }

    public function getHouses(array $campus)
    {
        $data = $this->db->get_stack('select house_id as id from cc_house where campus_id in (' . implode(',', $campus) . ')');
        if (empty($data)) {
            return false;
        }
        return $data;
    }
}
