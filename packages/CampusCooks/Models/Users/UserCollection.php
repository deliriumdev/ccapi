<?php
/**
 * Campus Cooks API
 */
namespace CampusCooks\Models\Users;

use Reo\Collection\TraversableTrait;

class UserCollection implements \ArrayAccess, \Countable, \IteratorAggregate
{
    use TraversableTrait;

    protected $db;
    protected $wpDbPrefix;

    public function __construct($db, $wpDbPrefix = 'wp_')
    {
        $this->db = $db;
        $this->wpDbPrefix = $wpDbPrefix; 
    }

    public function get($id)
    {
        if (isset($this->items[$id])) {
            return $this->items[$id];
        }
        $groups = $this->db->get_array($sql = 'SELECT gg.* FROM ' . $this->wpDbPrefix . 'groups_user_group ug inner join ' . $this->wpDbPrefix . 'groups_group gg on gg.group_id = ug.group_id and ug.user_id = :id and gg.group_id!=1 ', [':id' => $id]); //and gg.parent_id is not null
        if (empty($groups)) {
            $groups = $houses = $campus = [];
        }
        else {
            $houses = array_values(array_filter($groups, function($item){
                return !empty($item['parent_id']);
            }));
            $campus = array_map(function($item) {
                return $item['group_id'];
            }, array_filter($groups, function($item) {
                return empty($item['parent_id']);
            }));
        }
        // typically the campus group is not applied
        // @note the meta key cc_campus holds the campuses for supervisor and regional chef
        $houseCampus = array_unique(array_map(function($item) {
            return $item['parent_id'];
        }, $houses));
        $houses = array_map(function($item) {
            return $item['group_id'];
        }, $houses);
        $campus = empty($campus) ? $houseCampus : array_merge($houseCampus, array_values($campus));
        $roles = $this->db->get_column('select meta_value from ' . $this->wpDbPrefix . 'usermeta um where meta_key = \''. $this->wpDbPrefix . 'capabilities\' and user_id = ' . $id);
        if (empty($roles) || false === ($roles = unserialize($roles))) {
            $roles = [];
        }
        return $this->items[$id] = new User((int) $id, ['id' => $id, 'groups' => $groups, 'houses' => $houses, 'campus' => $campus, 'roles' => $roles]);
    }

    public function getEmptyUser()
    {
        return new User(0, ['id' => 0, 'groups' => [], 'houses' => [], 'campus' => [], 'roles' => []]);
    }
}
