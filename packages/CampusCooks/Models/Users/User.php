<?php
/**
 * Campus Cooks API
 */
namespace CampusCooks\Models\Users;

use Reo\Collection\TraversableTrait;

class User
{
    use TraversableTrait;

    protected $id;

    public function __construct($id, array $data)
    {
        $this->id = $id;
        $this->items = $data;
    }
}
