<?php
/**
 * Campus Cooks API
 */
namespace CampusCooks\Models;
use Reo\Collection\TraversableTrait;

class BudgetCollection implements \ArrayAccess, \Countable, \IteratorAggregate
{
    use TraversableTrait;

    protected $db;
    protected $paginator;
    protected $services;
    
    public function __construct($db, $paginator, $services)
    {
        $this->db = $db;
        $this->paginator = $paginator;
        $this->services = $services;
    }

    public function getWeekOfData($houses)
    {
        
        $sql = <<<'EOD'
select sum(bud_item.amount) as total, bud.weekOf, house.house_name as house, house.house_id as id from cc_budget bud 
inner join cc_budget_item bud_item on bud_item.budgetID = bud.budgetID
left join cc_campus campus on campus.campus_id = bud.campus_id 
left join cc_house house on house.house_id = bud.house_id
left join cc_user user on user.user_id = bud.user_id
where bud.house_id = :house_id and billBack != 1 and donation != 1 and bud.archived != 1 group by bud.weekOf order by bud.weekOf
EOD;
        $data = [];
        foreach ($houses as $houseId) {
            $rows = $this->db->get_array($sql, $params = [':house_id' => $houseId]);
            if (empty($rows)) {
                // var_dump($sql, $this->db->err, $params);exit;
                continue;
            }
            $data[] = $rows;
        }
        return $data;
    }

    public function getPieData($houses, $weekOf = null, $byHouse = false)
    {
        $sql = <<<'EOD'
select sum(bud_item.amount) as total, bud_item.vendor_name as vendor from cc_budget bud 
inner join cc_budget_item bud_item on bud_item.budgetID = bud.budgetID
left join cc_campus campus on campus.campus_id = bud.campus_id 
left join cc_house house on house.house_id = bud.house_id
left join cc_user user on user.user_id = bud.user_id
EOD;
 
        $conds = ['billBack != 1', 'donation != 1'];
        $params = [];
        if (!empty($weekOf)) {
            $conds[] = 'bud.weekOf = :weekOf';
            $params[':weekOf'] = $weekOf;
        }
        if (!empty($houses)) {
            $conds[] = 'bud.house_id in (' . implode(',', $houses) . ')';
        }
        if (!empty($conds)) {
            $sql .= ' where ' . implode(' and ', $conds);
        }
        if ($byHouse) {
            $sql = str_replace('bud_item.vendor_name as vendor', 'house.house_name as house, house.house_id as id', $sql);
            $sql .= ' group by house.house_name order by house.house_name';
        }
        else {
            $sql .= ' group by bud_item.vendor_name order by bud_item.vendor_name';
        }
        $rows = $this->db->get_array($sql, empty($params) ? null : $params);

        if (empty($rows)) {
            return false;
        }
        return $rows;
    }
/**
 * search
 * 
 * Search budgets
 */ 
    public function search($dateStart = null, $dateEnd = null, $house = false, $campus = false, $page = 1, $isFullWeek = false, $type = '', $details = null, $conds)
    {
        $all = false;
        $sql = <<<'EOD'
select bud.* from cc_budget bud 
left join cc_campus campus on campus.campus_id = bud.campus_id 
left join cc_house house on house.house_id = bud.house_id
left join cc_user user on user.user_id = bud.user_id
EOD;
        if ($details = (isset($details) && true === $details)) {
            $sql .= ' inner join cc_budget_item bud_item on bud_item.budgetID = bud.budgetID';
        }
        $params = [];
        if (!isset($conds)) {
            $conds = [];
        }
        else {
            $all = true; // request from FormArchive in WP, get all results and it will handle search and CSV downloads
        }
        switch($type) {
            case 'billback':
                $conds[] = 'bud.billBack = 1';
            break;
            case 'donation':
                $conds[] = 'donation = 1';
            break;
            default:
                $conds[] = 'donation = 0 and billBack = 0';
            break;
        }
        if (($isWeekOf = !empty($dateStart)) && !empty($dateEnd)) {
            $conds[] = 'bud.weekOf >= :startDate and sched.weekOf <= :endDate';
            $params[':startDate'] = EntryFactory::formatDate($dateStart);
            $params[':endDate'] = EntryFactory::formatDate($dateEnd);
        }
        elseif ($isWeekOf)
        {
            $conds[] = 'bud.weekOf = \'' . $dateStart . '\'';
        }
        else{
            $conds[] = 'bud.weekOf != \'0000-00-00\' and bud.archived != 1';
        }

        if (!empty($campus)) {
            $conds[] = 'bud.campus_id = :campus_id';
            $params[':campus_id'] = $campus;
        }
        $multiHouse = false;
        if (!empty($house) || '0' === $house) {
            if ($multiHouse = is_array($house)) {
                $conds[] = 'bud.house_id in (' . implode(',', $house) . ')';
            }
            else {
                $conds[] = 'bud.house_id = :house_id';
                $params[':house_id'] = $house;
            }
        }
        else {
            $house = false;
        }

        if (!empty($conds)) {
            $sql .= ' where ' . implode(' and ', $conds);
        }
        /*if ($isFullWeek) { // for weekly menu archive
            $sql .= ' group by sched.weekOf' . (false === $house || $multiHouse ? ', sched.house_id' : '');
            $count = $this->db->get_column('select count(*) from (' . $sql . ') cq', empty($conds) ? null : $params);
        }
        else {*/
            $count = $this->db->get_column(str_replace('select bud.*', 'select count(*)', $sql), empty($conds) ? null : $params);
        //}
        //var_export($sql);var_export($params);echo($this->db->err . "\n");exit;
        // var_dump($count, $sql);
        if (empty($count)) {
            // var_export($sql);var_export($params);echo($this->db->err . "\n");exit;
            // $result = $this->db->execute('use c3campuscook');
            return [];
        }
        //$orderBy = $isFullWeek ? ' order by weekDate desc, sortCampus asc, sortHouse asc' : ' order by sched.schedule_date desc, campus.campus_name asc, house.house_name asc';
        $orderBy =  ' order by bud.weekOf desc, campus.campus_name asc, house.house_name asc';
        if ($details) {
            $sql = str_replace('select bud.*', 'select bud.*, bud_item.*, DATE_FORMAT(bud_item.invoice_date, \'%M %d, %Y\') as invDate, campus.campus_name, house.house_name', $sql);
        }
        $rows = $this->db->get_array($sql = $sql . $orderBy . ($all ? '' : $this->paginator->setCount((int) $count)->paginate($page)), empty($conds) ? null : $params);
        // var_export($sql);echo($this->db->err . "\n");exit;
        if (empty($rows)) {
            // var_export($sql);echo($this->db->err . "\n");exit;
            // $result = $this->db->execute('use c3campuscook');
            return false;
        }
        // var_export($sql);
        $houses = $this->services->get('Houses');
        $campus = $this->services->get('Campus');
        foreach ($rows as $key => $item) {
            /*if (!$isFullWeek) { // get the item detail
                // $rows[$key]['invoiceItems'] = $this->getBudgetItems($item['budgetID']);
            }
            else { // the full week data needs campus info */
            if (empty($item['campus_name'])) {
                $rows[$key]['campus'] = $campus->get($item['campus_id']);
                if (empty($rows[$key]['campus'])) {
                    $this->services->get('WpImport')->importCampus($item['campus_id']);
                    $rows[$key]['campus'] = $campus->get($item['campus_id']);
                }
                if ($details) { // should not happen unless campus is imported above
                    $item['campus_name'] =  $rows[$key]['campus']['name'];
                }
            }
            //}
            if (empty($item['campus_name'])) {
                $rows[$key]['house'] = $houses->get($item['house_id']);
                if (empty($rows[$key]['house'])) {
                    $this->services->get('WpImport')->importHouse($item['house_id']);
                    $rows[$key]['house'] = $houses->get($item['house_id']);
                }
                if ($details) { // should not happen unless campus is imported above
                    $item['house_name'] =  $rows[$key]['house']['name'];
                }
            }
        }
        // $result = $this->db->execute('use c3campuscook');
        return $rows;
    }

    public function getBudgetItems($id)
    {
        // $result = $this->db->execute('use c3ccdev');
        $items = $this->db->get_array($sql = 'select * from cc_budget_item where budgetID = :id order by invoice_date desc', [':id' => $id]);
        if (empty($items)) {
            $items = [];
        }
        return $items;
    }

    public function addBudgetItems(array $budget)
    {
        foreach ($budget as $key => $item) {
            $budget[$key]['items'] = $this->getBudgetItems($item['budgetID']);
        }
        return $budget;
    }

    public function updateBudgetItem($itemId, $fieldName, $value)
    {
        $result = $this->db->execute($sql = 'update cc_budget_item set ' . $fieldName . '= :value where itemID = :id', $params = [':value' => $value, ':id' => $itemId]);
        return $result;
    }

    public function removeBudgetItem($itemId)
    {
        $result = $this->db->execute($sql = 'delete from cc_budget_item where itemID = :id', [':id' => $itemId]);
        // var_dump($result, $sql, $itemId);exit;
        return $result;
    }

    public function addBudgetItemSummary(array $budget, $weekOf)
    {
        foreach ($budget as $key => $bud) {
            $results = [];
            $date = $weekOf;
            for ($xx = 0; $xx < 6; $xx++) {
                if (0 < $xx) { // increment days
                    $date = date('Y-m-d', strtotime($weekOf) + (86400 * $xx));
                }
                $amount = $this->db->get_column($sql = 'select sum(amount) from cc_budget_item where budgetID = :id and invoice_date = :invDate', $params = [':id' => $bud['budgetID'], ':invDate' => $date]);
                if (!empty($amount)) {
                    $results[] = $amount;
                }
                else {
                    // var_dump($sql, $this->db->err, $params);exit;
                    $budget[$key]['amounts'] = $results;
                    break;
                }
            }
            $budget[$key]['amounts'] = $results;
        }
        return $budget;
    }

/**
 * getByWeek
 * 
 * get budget summary for the week
 * 
 * @param $weekOf date the week of
 * @param $houseId int|array the house ID or IDs
 */ 
    public function getByWeek($houseId, $weekOf, $all = false, $type = null)
    {
        if (!$all) {
            $sql = 'select bud.total from cc_budget bud where bud.weekOf = :weekOf and bud.house_id in (' . implode (',', (array) $houseId) . ')';
            $total = $this->db->get_column($sql, [':weekOf' => $weekOf]);
            if (empty($total)) {
                // var_export($sql);echo($this->db->err . "\n");exit;
                return 0;
            }
            return $total;
        }
        $sql = 'select * from cc_budget bud where bud.weekOf = :weekOf';
        if (!empty($type)) {
            switch($type) {
                case 'billback':
                    $sql .= ' and billBack = 1';
                break;
                case 'donation':
                    $sql .= ' and donation = 1';
                break;
                default:
                    $sql .= ' and donation = 0 and billBack = 0';
                break;
            }
        }
        $sql .=  ' and bud.house_id = :house_id';
        $data = $this->db->get_row($sql, $params = [':weekOf' => $weekOf, ':house_id' => $houseId]);
        // var_dump($sql, $data, $params, $this->db->err);
        if (empty($data)) {
            return false;
        }
        return $data;
    }

    public function getArchiveData($houseIds, $length, $page, $type)
    {
        // pagination
        $this->paginator->setItemsPerPage($length);
        // var_dump($page, $length);
        // column search
        $columns = [
            'campus.campus_name',
            'house.house_name',
        ];
        $conds = [];
        if (isset($_POST['columns']) && is_array($_POST['columns'])) {
            foreach ($_POST['columns'] as $key => $value) {
                if (!empty($value['search']['value']) && isset($columns[$key])) {
                    $conds[] = $columns[$key] . ' like ' . $this->db->quote('%' . str_replace('%', '', $value['search']['value']) . '%');
                }
            }
        }
        return $this->search(null, null, empty($houseIds) ? false : $houseIds, false, $page, false, $type, true, []); // @note the WP frontend will handle the conds for search
    }

    public function getActiveWeeks($houseIds, $weekOf)
    {
        $weeks = $this->db->get_stack($sql = 'select distinct weekOf from cc_budget bud where weekOf < ' . $this->db->quote($weekOf) . ' and archived = 0 and house_id in (' . implode(',', $houseIds) . ') order by weekOf desc');
        if (empty($weeks)) {
            // var_dump($sql, $this->db->err);
            return [$weekOf];
        }
        array_unshift($weeks, $weekOf);
        return $weeks;
    }

    public function getPaginator()
    {
        return $this->paginator;
    }

    public function getResultCount()
    {
        return $this->paginator->count;
    }
}
