<?php
/**
 * Campus Cooks API
 */
namespace CampusCooks\Models;

use Reo\Collection\TraversableTrait;

class CampusCollection implements \ArrayAccess, \Countable, \IteratorAggregate
{
    use TraversableTrait;

    protected $db;
    protected $services;

    public function __construct($db, $services)
    {
        $this->db = $db;
        $this->services = $services;
    }

/**
 * get
 * 
 * returns campus data by ID
 */
    public function get($id)
    {
        // $result = $this->db->execute('use c3campuscook');//@note remove after test
        $campus = $this->db->get_row('select campus_id as id, campus_name as name, tz from cc_campus where campus_id=:id', [':id' => $id]);
        if (empty($campus)) {
            return false;
        }
        return $campus;
    }

/**
 * getToday
 * 
 * get todays lateplates by mealtime
 * 
 * @param $mealtime string the meal time, dinner|lunch
 */ 
    public function getCampusByTz($tz)
    {
        $campus = $this->db->get_array('select * from cc_campus where tz=:tz', [':tz' => $tz], 'campus_id');
        // will import additional campuses found in wp groups if not populated here
        if ($this->services->get('WpImport')->getGroupsByTz($tz, $campus)) {
            // reload
            $campus = $this->db->get_array('select * from cc_campus where tz=:tz', [':tz' => $tz], 'campus_id');
        }
        return $campus;
    }

/**
 * getAll
 * 
 * get all houses, defaults to groups for sychronization purposes
 */ 
    public function getAll($sort = true, $kvp = false, array $ids = null)
    {
        $wpPrefix = $this->services->get('Services')->get('Config')->get('database')->get('prefix');
        $sql = 'select grouper.group_id as id, campus.campus_id, grouper.name from ' . $wpPrefix . 'groups_group grouper left join cc_campus campus on campus.campus_id = grouper.group_id where '; // @note 1 is default registered group
        $sql .= (empty($ids) ? 'grouper.parent_id is null and grouper.group_id != 1' : 'grouper.group_id in (' . implode($ids) . ')');
        $data = $this->db->get_array($sql, null, $kvp ? 'id' : false);
        if (empty($data)) {
            // var_dump($data, $sql, $this->db->err);
            return [];
        }
        $missing = array_filter($data, function($item) {return empty($item['campus_id']);});
        if (!empty($missing)) {
            // var_dump($missing);exit;
            foreach ($missing as $key => $item) {
                $this->services->get('WpImport')->importCampus($data['id']);
            }
        }
        // leave out campus id field
        $data = array_map(function($item) use(&$kvp) {return $kvp ? $item['name'] : ['text' => $item['name'], 'id' => $item['id']];}, $data);
        if ($sort) {
            $fN = $kvp ? 'uasort' : 'usort';
            $fN($data, function($a, $b) { // disregard the univ prefix
                return strnatcasecmp(str_replace('University of ', '', $a['text']), str_replace('University of ', '', $b['text']));
            });
        }
        return $data;
    }

/**
 * getCampusTz
 * 
 * get campus timezone
 * 
 * @param $campusId int the campus id
 */ 
    public function getCampusTz($campusId)
    {
        $tz = $this->db->get_column('select tz from cc_campus where campus_id = :id', [':id' => $campusId]);
        if (empty($tz)) {
            return false;
        }
        return $tz;
    }
}
