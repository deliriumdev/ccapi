<?php
/**
 * Campus Cooks API
 */
namespace CampusCooks\Models;

use Reo\Collection\TraversableTrait;

class Attendance
{
    protected $db;
    protected $services;

    public function __construct($db, $services)
    {
        
        $this->db = $db;
        $this->services = $services;
    }

    public function getPlateCt($houseId, $weekOf)
    {
        $sql = <<<'EOD'
select sched.meal_schedule_id as id, sched.meal_id, sched.dayOfWeek, sched.meal_time, DAYOFWEEK(sched.schedule_date) as dow, meal.meal_name, att.attendCt as ct, att.lateplateCt as lct 
from cc_meal_schedule sched inner join cc_meal meal on meal.meal_id = sched.meal_id 
left join cc_meal_attend att on att.meal_schedule_id = sched.meal_schedule_id 
where sched.weekOf = :weekOf and sched.house_id = :house_id order by sched.meal_time, dow 
EOD;
        $data = $this->db->get_array($sql, $params = [':house_id' => $houseId, ':weekOf' => $weekOf]);
        if (empty($data)) {
            return false;
        }
        // var_dump($params, $this->db->err, $data, $sql);
        $menus = $this->services->get('Menus');
        // populate the menus
        $default = [1 => false, 2 => false, 3 => false];//lunch, dinner, breakfast
        $days = [$default, $default, $default, $default, $default, $default, $default];
        foreach ($data as $key => $item) {
            $index = (int) $item['dow'];
            $index--;
            $days[$index][$item['meal_time']] = ['id' => $item['id'], 'name' => $item['meal_name'], 'ct' => empty($item['ct']) ? 0 : (int) $item['ct'], 'lct' => empty($item['lct']) ? 0 : (int) $item['lct']]; 
            // $data[$key]['menu'] = $menus->getMealItems($item['meal_id']);
        }
        return $days;
    }

/*
 * getPlateCtB
 * 
 * gets the plate count for the editing ui, will seperate lateplate chef entered and student entered through mobile app
 */ 
    public function getPlateCtB($houseId, $weekOf)
    {
        $sql = <<<'EOD'
select sched.meal_schedule_id as id, sched.meal_id, sched.dayOfWeek, sched.meal_time, sched.schedule_date, 
DAYOFWEEK(sched.schedule_date) as dow, meal.meal_name, att.attendCt as ct, att.lateplateCt as lct 
from cc_meal_schedule sched inner join cc_meal meal on meal.meal_id = sched.meal_id 
left join cc_meal_attend att on att.meal_schedule_id = sched.meal_schedule_id 
where sched.weekOf = :weekOf and sched.house_id = :house_id order by sched.meal_time, dow 
EOD;
        $data = $this->db->get_array($sql, $params = [':house_id' => $houseId, ':weekOf' => $weekOf]);
        if (empty($data)) {
            return false;
        }
        // var_dump($params, $this->db->err, $data, $sql);
        $menus = $this->services->get('Menus');
        // populate the menus
        $default = [1 => false, 2 => false, 3 => false];//lunch, dinner, breakfast
        $days = [$default, $default, $default, $default, $default, $default, $default];
        // these are the actual plates
        // $lateplates = $this->services->get('Lateplate')->getAvgByWeekday([$houseId], $weekOf);
        foreach ($data as $key => $item) {
            $lateplateCt = $this->getStudentLateplates($houseId, $item['schedule_date'], $item['meal_time']);
            $index = (int) $item['dow'];
            $index--;
            $days[$index][$item['meal_time']] = ['id' => $item['id'], 'name' => $item['meal_name'], 'ct' => empty($item['ct']) ? 0 : (int) $item['ct'], 'lct' => empty($item['lct']) ? 0 : (int) $item['lct'], 'lp' => $lateplateCt]; 
            // $data[$key]['menu'] = $menus->getMealItems($item['meal_id']);
        }
        return $days;
    }

    public function updatePlateCt($menuId, $ct, $isLateplate = false)
    {
        $data = $this->db->get_row('select attendID from cc_meal_attend where meal_schedule_id = :menuID', [':menuID' => $menuId]);
        if (empty($data)) { //insert
            $sql = 'insert into cc_meal_attend (meal_schedule_id, ' . ($isLateplate ? 'lateplateCt' : 'attendCt') . ') values (:menuID, :ct)'; 
        }
        else { // update
            $sql = 'update cc_meal_attend set ' . ($isLateplate ? 'lateplateCt' : 'attendCt') . ' = :ct where meal_schedule_id = :menuID';
        }
        $this->db->execute($sql, [':menuID' => $menuId, ':ct' => $ct]);
        return true;
    }

/*
 * getPlateData
 * 
 * gets plate data for charts, will combine chef entered and student request through app counts for lateplates
 */ 
    public function getPlateData($houseIds, $weekOf = false, $isLateplate = false, $userId = 0)
    {
        if (false === $weekOf) {
            return $this->getPlateDataSummary($houseIds, $isLateplate, $userId);
        }
        $sql = <<<'EOD'
select sched.meal_schedule_id as id, sched.meal_id, sched.dayOfWeek, sched.meal_time, DAYOFWEEK(sched.schedule_date) as dow, 
sched.schedule_date as plate_date, meal.meal_name, att.attendCt as ct, att.lateplateCt as lct 
from cc_meal_schedule sched inner join cc_meal meal on meal.meal_id = sched.meal_id 
left join cc_meal_attend att on att.meal_schedule_id = sched.meal_schedule_id 
where sched.weekOf = :weekOf and sched.meal_time = :mealTime and sched.house_id = :house_id order by dow 
EOD;
        if (false === $weekOf) {
            $sql = str_replace('sched.weekOf = :weekOf', 'sched.weekOf > :weekOf', $sql);
            $weekOf = '2017-07-31';
        }
        $meals = [1, 2, 3]; // lunch, dinner, breakfast
        $houses = $this->services->get('Houses');
        $data = [];
        $default = [0, 0, 0, 0, 0, 0, 0];
        $defaultNames = ['', '', '', '', '', '', ''];
        $plateType = $isLateplate ? 'lct' : 'ct';
        foreach ($houseIds as $id) {
            $house = $houses->get($id);
            foreach ($meals as $mealTime) {
                $rows = $this->db->get_array($sql, [':weekOf' => $weekOf, ':house_id' => $id, ':mealTime' => $mealTime]);
                if (empty($rows)) {
                    // var_dump($sql, $this->db->err);exit;
                    // continue;
                }
                $meals = $default;
                $names = $defaultNames;
                foreach ($rows as $item) {
                    $dow = -2 + $item['dow'];
                    if (0 > $dow) { // Sunday
                        $dow = 6;
                    }
                    $meals[$dow] = empty($item[$plateType]) ? 0 : (int) $item[$plateType];
                    $names[$dow] = $item['meal_name'];
                    // get the student data if lateplates
                    if ($isLateplate && 0 !== ($lpCt = $this->getStudentLateplates($id, $item['plate_date'], $mealTime))) {
                        $meals[$dow] += $lpCt;
                    }
                }
                $data[] = ['house' => $house['name'], 'mealTime' => $mealTime, 'data' => $meals, 'meals' => $names];
            }
        }
        if (empty($data)) {
            return false;
        }
        return $data;
    }

    public function getPlateDataSummary($houseIds, $isLateplate = false, $userId)
    {
        $sql = <<<'EOD'
sched.dayOfWeek, sched.meal_time, DAYOFWEEK(sched.schedule_date) as dow   
from cc_meal_schedule sched left join cc_meal_attend att on att.meal_schedule_id = sched.meal_schedule_id 
where sched.weekOf > '2017-07-31' and sched.meal_time = :mealTime and sched.house_id = :house_id group by dow order by dow 
EOD;
        //$sql = 'select ' . ($isLateplate ?  'sum(att.lateplateCt) as ct, count(att.lateplateCt) as tot, ' : 'sum(att.attendCt) as ct, count(att.attendCt) as tot, ') . $sql;
        // attending does not check mobile data
        if (!$isLateplate) {
            $sql = 'select sum(att.attendCt) as ct, count(att.attendCt) as tot, ' . $sql;
            // drop the zero rows for attending
            $sql = str_replace('sched.meal_time = :mealTime', 'att.attendCt != 0 and sched.meal_time = :mealTime', $sql);
        }
        else {
            // get count from distinct weekOf not plate ct
            $sql = 'select sum(att.lateplateCt) as ct, count(distinct sched.weekOf) as tot, ' . $sql;
        }
        // drop the zero rows, keep house_id index at the end of the sql
        // $sql = str_replace('sched.meal_time = :mealTime', ($isLateplate ? ' att.lateplateCt != 0' : 'att.attendCt != 0') . ' and sched.meal_time = :mealTime', $sql);
        $meals = [1, 2]; // lunch and dinner
        $houses = $this->services->get('Houses');
        $data = [];
        $default = [0, 0, 0, 0, 0, 0, 0];
        $defaultNames = ['', '', '', '', '', '', ''];
        foreach ($houseIds as $id) {
            $house = $houses->get($id);
            foreach ($meals as $mealTime) {
                $rows = $this->db->get_array($sql, $params = [':house_id' => $id, ':mealTime' => $mealTime]);
                if (empty($rows)) {
                    // var_dump($sql, $this->db->err);exit;
                    continue;
                }
                $meals = $avg = $default;
                $names = $defaultNames;
                foreach ($rows as $item) {
                    $dow = -2 + $item['dow'];
                    if (0 > $dow) { // Sunday
                        $dow = 6;
                    }
                    $meals[$dow] = empty($item['ct']) ? 0 : (int) $item['ct'];
                    $names[$dow] = 'Semester Avg';
                    if ($isLateplate) {
                        if (0 !== ($lpCt = $this->getStudentLateplates($id, false, $mealTime, $item['dow']))) {
                            $meals[$dow] += $lpCt;
                        }
                        // recurring is a more complex query for the semester summary, check here
                        /*if (174 == $id && 6 == $item['dow']) {
                            var_dump($this->getRecurringStudentLateplates($id, $mealTime, $item['dow']));exit;
                        }*/
                        if (0 !== ($lpCt = $this->getRecurringStudentLateplates($id, $mealTime, $item['dow']))) {
                            $meals[$dow] += $lpCt;
                        }
                    }
                    $avg[$dow] = (0 == $meals[$dow] || empty($item['tot'])) ? 0 : round($meals[$dow] / $item['tot']);
                }
                $data[] = ['house' => $house['name'], 'mealTime' => $mealTime, 'data' => $meals, 'meals' => $names, 'avg' => $avg];
            }
        }
        if (empty($data)) {
            return false;
        }
        return $data;
    }

    public function getWeekCt($houseIds, $isLateplate)
    {
        // get the week count for the average calc, count the weekOfs that contain data
        $sql = <<<'EOD'
select sched.weekOf from cc_meal_schedule sched inner join cc_meal_attend att on att.meal_schedule_id = sched.meal_schedule_id 
where sched.weekOf > '2017-07-31' 
EOD;
        $sql .= ($isLateplate ? ' and att.lateplateCt != 0 and sched.is_lateplate = 1' :  ' and att.attendCt != 0');
        $sql .= ' and sched.house_id in (' . implode(',', $houseIds) . ') group by sched.weekOf';
        if (false === ($rows = $this->db->get_array($sql)) || empty($rows)) {
            return 0;
        }
        // var_dump($rows, $sql, $isLateplate, $this->db->err);
        return count($rows);
    }

/**
 * @note this should probably go in lateplates
 * gets lateplates requested by mobile app
 */
    public function getStudentLateplates($houseId, $date, $mealtime, $dow = false)
    {
        $params = [':house_id' => $houseId, ':mealtime' => $mealtime];
        if (false === $date) {// entire semester
            $sql = <<<'EOD'
select count(entry.entry_id) as ct from request_entry entry inner join lateplate_entry lateplate on lateplate.entry_id = entry.entry_id 
where DAYOFWEEK(lateplate.request_date) = :dow and lateplate.is_recurring = 0
and lateplate.archived = 0 and lateplate.meal_time = :mealtime and entry.house_id = :house_id
EOD;
        $params[':dow'] = $dow; 
        // where CASE WHEN recurring_stop_date < CURDATE() THEN startDate >= stop ELSE startTime >= start
        }
        else { // by date
            $sql = <<<'EOD'
select count(entry.entry_id) as ct from request_entry entry inner join lateplate_entry lateplate on lateplate.entry_id = entry.entry_id 
where (lateplate.request_date = :date or 
(DAYOFWEEK(:date) = DAYOFWEEK(lateplate.request_date) and recurring_stop_date > :date and is_recurring = 1)) 
and lateplate.archived = 0 and lateplate.meal_time = :mealtime and entry.house_id = :house_id
EOD;
        $params[':date'] = $date; 
        }
        $sql = str_replace('DAYOFWEEK(:date)', 'DAYOFWEEK(' . $this->db->quote($date) . ')', $sql);
        if (false === ($row = $this->db->get_row($sql, $params)) || empty($row)) {
            // var_dump($ct, $sql, $this->db->err, $params);exit;
            return 0;
        }
        return (int) $row['ct'];
    }
    
    public function getRecurringStudentLateplates($houseId, $mealtime, $dow)
    {
    // get recurring for semester day of week
        $sql = <<<'EOD'
select FLOOR(DATEDIFF(IF(lateplate.recurring_stop_date < CURDATE(), lateplate.recurring_stop_date, CURDATE()), lateplate.request_date)/7) + 1 as ct, lateplate.request_date, lateplate.recurring_stop_date 
from lateplate_entry lateplate inner join request_entry entry on entry.entry_id = lateplate.entry_id 
where DAYOFWEEK(lateplate.request_date) = :dow and request_date <= CURDATE() and lateplate.is_recurring = 1 
and lateplate.archived = 0 and lateplate.meal_time = :mealtime and entry.house_id = :house_id
EOD;
        if (false === ($rows = $this->db->get_array($sql, ['house_id' => $houseId, ':mealtime' => $mealtime, ':dow' => $dow])) || empty($rows)) {
            // var_dump($ct, $sql, $this->db->err, $params);exit;
            return 0;
        }
        return array_sum(array_map(function($item){ return (int) $item['ct'];}, $rows));
    }
}
