<?php
/**
 * Campus Cooks API
 */
namespace CampusCooks\Models;

use Reo\Collection\TraversableTrait;

class NotificationCollection  implements \ArrayAccess, \Countable, \IteratorAggregate
{
    use TraversableTrait;
    
    protected $db;
    protected $services;

    public function __construct($db, $services)
    {
        $this->db = $db;
        $this->services = $services;
    }

/**
 * get
 * 
 * gets notifications for mobile app
 * 
 * @param $studentId int the students user id
 * @note will implement student ID in the future to pull certian house or campus wide messages
 */ 
    public function get(Student $student)
    {
        $sql = <<<'EOD'
select noticeID, content from cc_notification where start_date <= CURDATE() and stop_date >= CURDATE() 
and (campus_id = 0 or campus_id = :campus_id) and (house_id = 0 or house_id = :house_id)
EOD;
        $rows = $this->db->get_array($sql . ' order by noticeID desc', ['campus_id' => $student->campusId, 'house_id' => $student->houseId]);
        if (empty($rows)) {
            // var_dump($sql, $this->db->err);
            return false;
        }
        return $rows;
    }
}
