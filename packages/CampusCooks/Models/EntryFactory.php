<?php
/**
 * Campus Cooks API
 */
namespace CampusCooks\Models;

use Reo\Collection\TraversableTrait;

class EntryFactory
{
    protected $db;
    protected $services;
    protected $errors = [];
    protected static $days = [
        'Sunday' => 7,
        'Monday' => 1,
        'Tuesday' => 2,
        'Wednesday' => 3,
        'Thursday' => 4,
        'Friday' => 5,
        'Saturday' => 6,
    ];

    public function __construct($db, $services)
    {
        $this->db = $db;
        $this->services = $services;
    }

    public function getCutoffTime($mealtime, $houseId = null, $isLateplate = false)
    {
        $mealtime = strtolower($mealtime);
        if ($isLateplate && isset($houseId) && false !== ($hour = $this->services->get('Houses')->getCutoffHour($houseId, $mealtime))) {
            return $hour;
        }
        $hours = $this->services->get('Services')->get('Config')->get('api')->get('report_hours')->toArray();
        return isset($hours[$mealtime]) ? $hours[$mealtime] : $hours['dinner'];
    }

    public function addLateplate(Student $student, $values)
    {
        // sometimes the mobile app passes an empty request data
        if ('0000-00-00' === $values['requestDate']) {
            $values['requestDate'] = date('Y-m-d');
        }
        // check if lateplate already exists
        $sql = <<<'EOD'
select entry.entry_id from lateplate_entry lateplate inner join request_entry entry on entry.entry_id = lateplate.entry_id 
where lateplate.request_date = :requestDate and lateplate.meal_time = :mealTime and entry.student_id = :studentId
EOD;
        $id = $this->db->get_column($sql, $params = [':mealTime' => 'Dinner' == $values['mealTime'] ? 2 : ('Breakfast' == $values['mealTime'] ? 3 : 1), ':requestDate' => $values['requestDate'], ':studentId' => $student->id]);
        /*if (1713 == $student->id) {
            //var_dump($values);exit;
            var_dump($id, $sql, $params, $this->db->err);exit;
        }*/
        if (!empty($id)) {
            return $id;
        }
        // if out of house and house restricts check limit
        if (!$this->checkMax($student, $values, true)) {
            return false;
        }
        // check cutoff
        if (false === $this->checkCutoff($student, $values, 'a late plate') || false === ($entryId = $this->addEntry($student))) {
            return false;
        }
        $sql = <<<'EOD'
INSERT INTO lateplate_entry (lateplate_id, entry_id, meal_time, request_date, is_recurring, recurring_stop_date, chef_message) 
VALUES (NULL, :entryId, :mealTime, :requestDate, :isRecurring, :recurringStopDate, :chefMessage)
EOD;
        $params = [
            ':entryId'           => $entryId,
            ':mealTime'          => 'Dinner' == $values['mealTime'] ? 2 : ('Breakfast' == $values['mealTime'] ? 3 : 1),
            ':requestDate'       => $values['requestDate'],
            ':isRecurring'       => $values['isRecurring'],
            ':recurringStopDate' => $values['recurringStopDate'],
            ':chefMessage'       => $values['chefMessage'],
        ];
        $id = $this->db->execute($sql, $params);
        if (empty($id)) {
            // var_dump($this->db->err, $sql, $params);
            $this->errors[] = $sql . ' ' . $this->db->err;
            return false;
        }
        return $entryId;
    }

    public function addAttending(Student $student, $values)
    {
        // sometimes the mobile app passes an empty request data
        if ('0000-00-00' === $values['requestDate']) {
            $values['requestDate'] = date('Y-m-d');
        }
        // check if attending already exists
        $sql = <<<'EOD'
select entry.entry_id from attending_entry attending inner join request_entry entry on entry.entry_id = attending.entry_id 
where attending.request_date = :requestDate and attending.meal_time = :mealTime and entry.student_id = :studentId
EOD;
        $id = $this->db->get_column($sql, $params = [':mealTime' => 'Dinner' == $values['mealTime'] ? 2 : ('Breakfast' == $values['mealTime'] ? 3 : 1), ':requestDate' => $values['requestDate'], ':studentId' => $student->id]);
        /*if (1713 == $student->id) {
            //var_dump($values);exit;
            var_dump($id, $sql, $params, $this->db->err);exit;
        }*/
        if (!empty($id)) {
            return $id;
        }
        // if out of house and house restricts check limit
        if (!$this->checkMax($student, $values)) {
            return false;
        }
        // check cutoff
        if (false === $this->checkCutoff($student, $values, 'an attending request') || false === ($entryId = $this->addEntry($student))) {
            return false;
        }
        $sql = <<<'EOD'
INSERT INTO attending_entry (attending_id, entry_id, meal_id, week_of, day_of_week, meal_time, is_recurring, recurring_stop_date, chef_email, request_date) 
VALUES (NULL, :entryId, :mealId, :weekOf, :dayOfWeek, :mealTime, :isRecurring, :recurringStopDate, :chefEmail, :requestDate)
EOD;
        $params = [
            ':entryId'           => $entryId,
            ':mealId'            => $values['mealId'],
            ':weekOf'            => $values['weekOf'],
            ':dayOfWeek'         => isset(self::$days[$values['dayOfWeek']]) ? self::$days[$values['dayOfWeek']] : 0,
            ':mealTime'          => 'Dinner' == $values['mealTime'] ? 2 : ('Breakfast' == $values['mealTime'] ? 3 : 1),
            ':isRecurring'       => $values['isRecurring'], 
            ':recurringStopDate' => $values['recurringStopDate'],
            ':chefEmail'         => $values['chefEmail'],
            ':requestDate'       => $values['requestDate'],
        ];
        $id = $this->db->execute($sql, $params);
        if (empty($id)) {
            var_dump($this->db->err, $sql, $params);
            return false;
        }
        return $entryId;
    }

    public function addRatemeal(Student $student, $values)
    {
        // have to get mealtime to check if meal has happened @note the menu id passed is actually the lead id
        $mealtime = $this->db->get_row('select meal_id, meal_time, schedule_date from cc_meal_schedule where lead_id = :id', [':id' => $values['menuId']]);
        if (empty($mealtime)) {
            return false;
        }
        $values['mealTime'] = '1' === $mealtime['meal_time'] ? 'lunch' : ('3' ===  $mealtime['meal_time'] ? 'breakfast' : 'dinner');
        $values['requestDate'] = $mealtime['schedule_date'];
        if (false === $this->checkCutoff($student, $values, 'ratemeal')) {
            return false;
        }
        // check if meal is already rated
        $sql = <<<'EOD'
select entry.entry_id from ratemeal_entry ratemeal inner join request_entry entry on entry.entry_id = ratemeal.entry_id 
where ratemeal.menu_id = :menuId and ratemeal.item_name = :itemName and entry.student_id = :studentId
EOD;
        $id = $this->db->get_column($sql, $params = [':menuId' => $values['menuId'], ':itemName' => $values['itemName'], ':studentId' => $student->id]);
        if (!empty($id)) {
            return $id;
        }
        if (false === ($entryId = $this->addEntry($student))) {
            return false;
        }
        $sql = <<<'EOD'
INSERT INTO ratemeal_entry (ratemeal_id, entry_id, menu_id, item_name, rating, comments) 
VALUES (NULL , :entryId, :menuId, :itemName, :rating, :comments);
EOD;
        $params = [
            ':entryId'  => $entryId,
            ':menuId'   => $values['menuId'],
            ':itemName' => $values['itemName'],
            ':rating'   => $values['rating'],
            ':comments' => $values['comments'],
        ];
        $id = $this->db->execute($sql, $params);
        if (empty($id)) {
            // var_dump($this->db->err, $sql, $params);
            return false;
        }
        return $entryId;
    }

    public function addForum(Student $student, $values)
    {
        if (false === ($entryId = $this->addEntry($student))) {
            return false;
        }
        $sql = <<<'EOD'
INSERT INTO forum_entry (forum_id, entry_id, parent_id, thread_id, subject, comments) 
VALUES(NULL, :entryId, :parentId, :threadId, :subject, :comments)
EOD;
        $params = [
            ':entryId'  => $entryId,
            ':parentId' => $values['parentId'],
            ':threadId' => $values['threadId'],
            ':subject'  => $values['subject'],
            ':comments' => $values['comments'],
        ];
        $id = $this->db->execute($sql, $params);
        if (empty($id)) {
            // var_dump($this->db->err, $sql, $params);
            return false;
        }
        return $entryId;
    }

    public function addFeedback(Student $student, $values)
    {
        if (false === ($entryId = $this->addEntry($student))) {
            return false;
        }
        $sql = <<<'EOD'
INSERT INTO feedback_entry (feedback_id, entry_id, subject, comments) VALUES(NULL, :entryId, :subject, :comments)
EOD;
        $params = [
            ':entryId'  => $entryId,
            ':subject'  => $values['subject'],
            ':comments' => $values['comments'],
        ];
        $id = $this->db->execute($sql, $params);
        if (empty($id)) {
            // var_dump($this->db->err, $sql, $params);
            return false;
        }
        return $entryId;
    }

    public function checkMax($student, $values, $isLateplate = false)
    {
        if (false === ($houseData = $this->services->get('Houses')->get($student->houseId))) {
            return true;
        }
        if ($isLateplate) { // check if lateplates are enabled for the mealtime
            if (('Lunch' === $values['mealTime'] && 0 === $houseData['lateplate_lunch']) || ('Dinner' === $values['mealTime'] && 0 === $houseData['lateplate_dinner'])) {
                $this->errors[] = sprintf('Your house does not accept late plates for %s', $values['mealTime']);
                return false;
            }
        }
        if (0 == $student->inHouse && 'Breakfast' !== $values['mealTime'] && 1 == $houseData['ocRestrict']) {
            // get the monday for that week
            $dow = date('w', $time = strtotime($values['requestDate']));
            $dow = (0 == $dow) ? 7 : (int) $dow; // sun
            $offset = -86400 * ($dow - 1);
            $monday = date('Y-m-d', $time + $offset);
            // var_dump($monday, $values);
            $lateplates = $this->services->get('Lateplate')->getLateplates($student->id, $monday, $values['mealTime']);
            $attendingCount = $this->services->get('Attending')->getCount($student->id, $monday, $values['mealTime']);
            $total = $attendingCount + count($lateplates);
            $maxed = 'Lunch' === $values['mealTime'] ? $houseData['max_lunch'] <= $total : $houseData['max_dinner'] <= $total; 
            if ($maxed) {
                $this->errors[] = sprintf('You have exceeded the maximum number of %s', 'Lunch' === $values['mealTime'] ? 'Lunches' : 'Dinners');
                return false;
            }
            return true;
        }
        return true;
    }

    public function checkCutoff(Student $student, $values, $context)
    {
        /*if (174 == $student->houseId) {
            var_dump($student->id, $context, $values);exit;
        }*/
        // check the cuttoff time
        $hour = $this->getCutoffTime($values['mealTime'], $student->houseId, $isLateplate = 'a late plate' === $context);
        /*if (174 == $student->houseId) {
            var_dump($hour, $values['mealTime'], $student->houseId, 'a late plate' === $context);exit;
        }*/
        // ratemeal checks that the time has passed, so hours aftermeal
        // @note same now for attending
        if (!$isLateplate && 'ratemeal' !== $context) { // this will apply to attending, can be put in right up to meal time
            //var_dump($values);
            $hour += 2;
        }
        $isRatemeal = 'ratemeal' === $context;
        //@note maybe move this to utils so it does not have to be called statically and add the tz settings instead of here
        if ('UTC' !== ($resetTz = date_default_timezone_get())) {
            date_default_timezone_set('UTC');
        }
        $nowDate = date('Y-m-d', $time = time() + \CampusCooks\Reports\Scheduler::getTimeZoneOffset($this->services->get('Campus')->getCampusTz($student->campusId)));
        //@note todays meal does not send a date, so it will skip this and just check time
        if ('0000-00-00' != $values['requestDate'] && $nowDate > $values['requestDate']) {
            // $this->services->get('Services')->get('Logger')->info(sprintf('[%s] is greater than [%s]', $nowDate, $values['requestDate']));
            if ($isRatemeal) { // ratemeal returns the opposite of attending, it happens after the meal
                return true;
            }
            $this->errors[] = $this->getCutoffMessage($context);
            return false;
        }
        $nowHour = date('G', $time);
        if ($nowDate == $values['requestDate'] && $nowHour >= $hour) {//at or past the time
            // $this->services->get('Services')->get('Logger')->info(sprintf('[%s] is greater than [%s]', $nowHour, $hour));
            if ($isRatemeal) {
                if ($nowHour != $hour) {
                    return true;
                }
                //same hour, check the minutes
                $minutes = intval(date('i', $time));
                if (30 <= $minutes) {
                    return true;
                }
            }
            else {
                $this->errors[] = $this->getCutoffMessage($context);
                return false;
            }
        }
        // $this->services->get('Services')->get('Logger')->info(sprintf('TS %s %s %s %s %s', $values['mealTime'], $nowDate, $values['requestDate'], $nowHour, $hour));
        if ($isRatemeal) {
            $this->errors[] = 'This meal has not been served yet.';// . $nowHour . ' ' . $hour . ' ' . $minutes;
            return false;
        }
        return true;
    }

    public static function formatDate($date)
    {
        if (empty($date)) {
            return '0000-00-00';
        }
        $tmp = explode('/', $date);
        if (3 !== count($tmp) || !checkdate($tmp[0], $tmp[1], $tmp[2])) {
            return '0000-00-00';
        }
        return $tmp[2] . '-' . $tmp[0] . '-' . $tmp[1];
    }

    public static function formatBool($bool)
    {
        return true === $bool || '1' == $bool ? 1 : 0;
    }

    public static function getRequestDate($date, $day)
    {
        // check valid date
        if (empty($date) || empty($day)) {
            return false;
        }
        $dates = explode('/', $date);
        if (3 !== count($dates) || !checkdate($dates[0], $dates[1], $dates[2]) || !isset(self::$days[$day])) {
            return false;
        }
        // get the weekday for the start date and store the time
        $weekday = date('w', $time = mktime(12, 0, 0, $dates[0], $dates[1], $dates[2]));
        if (0 == $weekday) {
            $weekday = 7;
        }
        // the same day as week start (monday is day)
        if ($weekday == ($dow = self::$days[$day])) {
            return $date;
        }
        // calculate the number of days and get the date, weekday should always be monday
        return date('m/d/Y', $time + (($dow - $weekday) * 86400));
    }

    public static function keywordFilter($keyword)
    {
        return trim(str_replace('%', '', filter_var($keyword, FILTER_SANITIZE_STRING)), " ;\t\n\r\0\x0B");
    }

    public function getLastError()
    {
        if (empty($this->errors)) {
            return '';
        }
        return end($this->errors);
    }

    protected function getCutoffMessage($context)
    {
        if (null === ($message = $this->services->get('Services')->get('Config')->get('api')->get('deadline_msg')) || !strstr($message, '%s')) {
            return 'The cutoff time has passed for this meal';
        }
        return sprintf($message, $context);
    }

    protected function addEntry(Student $student)
    {
        $sql = 'INSERT INTO request_entry (entry_id, student_id, house_id, campus_id, entry_date) VALUES (NULL, :studentId, :houseId, :campusId, NOW())';
        $entryId = $this->db->execute($sql, $params = [':studentId' => $student->id, ':houseId' => $student->houseId, ':campusId' => $student->campusId]);
        if (empty($entryId)) {
            //var_dump($this->db->err, $sql, $params);
            $this->services->get('Services')->get('Logger')->error(sprintf('Entry error [%s] on [%s] with %s', $nowDate, $this->db->err, $sql, print_r($params, true)));
            return false;
        }
        return $entryId;
    }
}
