<?php
/**
 * Campus Cooks API
 */
namespace CampusCooks\Models;

class FeedbackCollection extends AbstractEntryCollection
{
    protected $paginator;

    public function __construct($db, $services)
    {
        parent::__construct($db, $services);
    }

/**
 * getOpenFeedback
 * 
 * get todays feedback
 * 
 * @param $mealtime string the meal time, dinner|lunch
 */ 
    public function getData($tz, $sort = true)
    {
        $sql = <<<'EOD'
select entry.student_id, entry.house_id, entry.campus_id, feedback.*, 
campus.tz, campus.campus_name, house.house_name, user.first_name, user.last_name  
from (((request_entry entry inner join feedback_entry feedback on feedback.entry_id = entry.entry_id) 
inner join cc_campus campus on campus.campus_id = entry.campus_id and campus.tz=:tz) left join cc_house house on house.house_id = entry.house_id)
left join cc_user user on user.user_id = entry.student_id where entry.is_sent = 0 and entry.inactive = 0
EOD;
        $rows = $this->db->get_array($sql, $params = [':tz' => $tz]);
        if (empty($rows)) {
            //var_dump($sql, $this->db->err);exit;
            return false;
        }
        foreach ($rows as $key => $data) {
            if ('' === $data['subject']) {
                $rows[$key]['subject'] = 'n/a';
            }
        }
        $this->populateItems($rows, $sort);
        // populate the tables that may not have data from legacy wp groups
        return $this;
    }

/**
 * get
 * 
 * Get the feedback, optionally by a specific house
 * 
 * @param $houses array of houses
 */
    public function get($houses, $dateStart = null, $dateEnd = null, $page = 1)
    {
        $sql = <<<'EOD'
select count(*) from request_entry entry inner join feedback_entry feedback on feedback.entry_id = entry.entry_id
EOD;
        $params = $conds = [];
        if (false !== $houses) {
            $conds[] = 'entry.house_id in (' . implode(',', $houses) . ')';
        }
        if (!empty($dateStart)) {
            $conds[] = 'entry.entry_date >= :startDate';
            $params[':startDate'] = EntryFactory::formatDate($dateStart) . ' 00:00:00';
        }
        if (!empty($dateEnd)) {
            $conds[] = 'entry.entry_date <= :endDate';
            $params[':endDate'] = EntryFactory::formatDate($dateEnd) . ' 23:59:59';
        }
        $conds[] = 'entry.inactive = 0';
        if (!empty($conds)) {
            $sql .= ' where ' . implode(' and ', $conds);
        }
        $count = $this->db->get_column($sql, empty($params) ? null : $params);
        if (empty($count)) {
            // var_dump($sql, $this->db->err);exit;
            return $this;
        }

        $sql = <<<'EOD'
select entry.student_id, entry.house_id, entry.campus_id, DATE_FORMAT(entry_date, '%m/%d/%Y') as feedback_date, feedback.*, 
campus.tz, campus.campus_name, house.house_name, user.first_name, user.last_name  
from (((request_entry entry inner join feedback_entry feedback on feedback.entry_id = entry.entry_id) 
inner join cc_campus campus on campus.campus_id = entry.campus_id) left join cc_house house on house.house_id = entry.house_id)
left join cc_user user on user.user_id = entry.student_id
EOD;
        if (!empty($conds)) {
            $sql .= ' where ' . implode(' and ', $conds);
        }
        $sql .= ' order by entry_date desc';
        if (0 !== $page) {
            $sql .= $this->getPaginator()->setCount((int) $count)->paginate($page);
        }
        // @note should probably key by feedback_id
        $rows = $this->db->get_array($sql, empty($params) ? null : $params);
        if (empty($rows)) {
            return $this;
        }
        // to make sure students are populated
        $this->populateItems($rows, false);
        if ($this->hasPopulated()) { //pull the data again
            $rows = $this->db->get_array($sql . $this->getPaginator()->setCount((int) $count)->paginate($page), empty($params) ? null : $params);
        }
        // reset data back to items;
        $this->items = $rows;
        return $this;
    }

/**
 * getPaginator
 * 
 * public access to paginator object
 * @todo move this to the abstract, would be used by all child classes
 */ 
    public function getPaginator()
    {
        if (!isset($this->paginator)) {
            $this->paginator = $this->services->get('Services')->get('Paginator');
        }
        return $this->paginator;
    }

    public function getResultCount()
    {
        return $this->getPaginator()->count;
    }
}
