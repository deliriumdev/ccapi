<?php
/**
 * Campus Cooks API
 */
namespace CampusCooks\Models;
use Reo\Collection\TraversableTrait;

class MenuCollection implements \ArrayAccess, \Countable, \IteratorAggregate
{
    use TraversableTrait;

    protected $db;
    protected $paginator;
    protected $services;
    
    public function __construct($db, $paginator, $services)
    {
        $this->db = $db;
        $this->paginator = $paginator;
        $this->services = $services;
    }

/**
 * search
 * 
 * Search menus
 * 
 * @param $keyword string a search keyword
 */ 
    public function search($keyword = '', $campus = false,  $dateStart = null, $dateEnd = null, $page = 1, $isFullWeek = false, $house = false)
    {
        $sql = <<<'EOD'
select sched.* from cc_meal_schedule sched 
inner join cc_meal meal on meal.meal_id = sched.meal_id 
left join cc_campus campus on campus.campus_id = sched.campus_id 
left join cc_house house on house.house_id = sched.house_id
EOD;
        if ($isFullWeek) {
            $sql = <<<'EOD'
select sched.*, DATE_FORMAT(sched.weekOf, '%m/%d/%Y') as weekDate,
max(campus.campus_name) as sortCampus, max(house.house_name) as SortHouse  
from cc_meal_schedule sched 
left join cc_campus campus on campus.campus_id = sched.campus_id 
left join cc_house house on house.house_id = sched.house_id
EOD;
        }
//inner join cc_meal_menu_item lu on lu.meal_id meal.meal_id inner join cc_menu_item on cc_menu_item.menu_item_id = lu.menu_item_id
        $params = $conds = [];
        if (!empty($dateStart) && !empty($dateEnd)) {
            $conds[] = 'sched.schedule_date >= :startDate and sched.schedule_date <= :endDate';
            $params[':startDate'] = EntryFactory::formatDate($dateStart);
            $params[':endDate'] = EntryFactory::formatDate($dateEnd);
        }
        elseif (!empty($dateStart)) {
            $conds[] = 'sched.schedule_date >= :startDate';
            $params[':startDate'] = EntryFactory::formatDate($dateStart);
        }
        else{
            // @note semester end should be setting, hardcoded for now
            // $conds[] = 'sched.schedule_date != \'0000-00-00\'';
            $conds[] = 'sched.schedule_date > \'2017-07-16\'';
        }

        if (!empty($campus)) {
            $conds[] = 'sched.campus_id = :campus_id';
            $params[':campus_id'] = $campus;
        }
        $multiHouse = false;
        if (!empty($house) || '0' === $house) {
            if ($multiHouse = is_array($house)) {
                $conds[] = 'sched.house_id in (' . implode(',', $house) . ')';
            }
            else {
                $conds[] = 'sched.house_id = :house_id';
                $params[':house_id'] = $house;
            }
        }
        else {
            $house = false;
        }
        if ('' !== $keyword) {
            $conds[] = 'meal.meal_name like :keyword';
            //$params[':keyword'] = $keyword;
            $params[':keyword'] = '%' . $keyword . '%';
        }
        //@note just for test, remove uses
        // $result = $this->db->execute('use c3ccdev');
        // var_export($result);
        if (!empty($conds)) {
            $sql .= ' where ' . implode(' and ', $conds);
        }
        if ($isFullWeek) { // for weekly menu archive
            $sql .= ' group by sched.weekOf' . (false === $house || $multiHouse ? ', sched.house_id' : '');
            $count = $this->db->get_column('select count(*) from (' . $sql . ') cq', empty($conds) ? null : $params);
        }
        else {
            $count = $this->db->get_column(str_replace('select *', 'select count(*)', $sql), empty($conds) ? null : $params);
        }
        //var_export($sql);var_export($params);echo($this->db->err . "\n");exit;
        if (empty($count)) {
            // var_export($sql);var_export($params);echo($this->db->err . "\n");exit;
            // $result = $this->db->execute('use c3campuscook');
            return [];
        }
        $orderBy = $isFullWeek ? ' order by weekDate desc, sortCampus asc, sortHouse asc' : ' order by sched.schedule_date desc, campus.campus_name asc, house.house_name asc';
        $rows = $this->db->get_array($sql = $sql . $orderBy . $this->paginator->setCount((int) $count)->paginate($page), empty($conds) ? null : $params);
        // var_export($sql);echo($this->db->err . "\n");exit;
        if (empty($rows)) {
            // var_export($sql);echo($this->db->err . "\n");exit;
            // $result = $this->db->execute('use c3campuscook');
            return false;
        }
        $houses = $this->services->get('Houses');
        $campus = $this->services->get('Campus');
        foreach ($rows as $key => $item) {
            if (!$isFullWeek) { // get the item detail
                $rows[$key]['menuItems'] = $this->getMealItems($item['meal_id']);
            }
            else { // the full week data needs campus info
                $rows[$key]['campus'] = $campus->get($item['campus_id']);
                if (empty($rows[$key]['campus'])) {
                    $this->services->get('WpImport')->importCampus($item['campus_id']);
                    $rows[$key]['campus'] = $campus->get($item['campus_id']);
                }
            }
            $rows[$key]['house'] = $houses->get($item['house_id']);
            if (empty($rows[$key]['house'])) {
                $this->services->get('WpImport')->importHouse($item['house_id']);
                $rows[$key]['house'] = $houses->get($item['house_id']);
            }
        }
        // $result = $this->db->execute('use c3campuscook');
        return $rows;
    }

    public function getMealItems($id)
    {
        // $result = $this->db->execute('use c3ccdev');
        $items = $this->db->get_array($sql = 'select menu_item.* from cc_menu_item menu_item inner join cc_meal_menu_item lu on lu.meal_item_id = menu_item.meal_item_id where lu.meal_id = :id', [':id' => $id]);
        if (empty($items)) {
            $items = [];
        }
        return new MenuItemCollection($items);
    }

/**
 * getByWeek
 * 
 * get all menus for the week
 * 
 * @param $weekOf date the week of
 * @param $houseId int the house ID
 */ 
    public function getByWeek($weekOf, $houseId)
    {
        $sql = <<<'EOD'
select sched.* from cc_meal_schedule sched 
inner join cc_campus campus on campus.campus_id = sched.campus_id 
inner join cc_house house on house.house_id = sched.house_id 
where sched.weekOf = :weekOf and sched.house_id = :houseId order by sched.schedule_date desc 
EOD;
        $rows = $this->db->get_array($sql, [':weekOf' => $weekOf, ':houseId' => $houseId]);
        if (empty($rows)) {
            var_export($sql);echo($this->db->err . "\n");exit;
            return false;
        }
        $houses = $this->services->get('Houses');
        $campus = $this->services->get('Campus');
        // populate detail data
        foreach ($rows as $key => $item) {
            $menuItems = $this->getMealItems($item['meal_id']);
            $rows[$key]['menuItems'] = $menuItems->toArray();
            $rows[$key]['campus'] = $campus->get($item['campus_id']);
            if (empty($rows[$key]['campus'])) {
                $this->services->get('WpImport')->importCampus($item['campus_id']);
                $rows[$key]['campus'] = $campus->get($item['campus_id']);
            }
            $rows[$key]['house'] = $houses->get($item['house_id']);
            if (empty($rows[$key]['house'])) {
                $this->services->get('WpImport')->importHouse($item['house_id']);
                $rows[$key]['house'] = $houses->get($item['house_id']);
            }
        }
        return $rows;
    }

    public function getPaginator()
    {
        return $this->paginator;
    }

    public function getResultCount()
    {
        return $this->paginator->count;
    }
}
