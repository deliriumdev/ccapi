<?php
/**
 * Campus Cooks API
 */
namespace CampusCooks\Models;

use Reo\Collection\TraversableTrait;

class Forum
{
    public function __construct($id)
    {
        
        if (isset($fields)) {
            $this->items = $values;
        }
    }

    public static function getFields()
    {
        return [
            'studentId'   => ['constraints' => ['Digit'], 'key' => 'student_id'],
            'studentName' => ['constraints' => ['Text', ['MinLength', 2]], 'key' => 'input_3'],
            'comments'    => ['constraints' => ['Text'], 'optional' => true, 'key' => 'input_5'],
            'subject'     => ['constraints' => ['Text'], 'optional' => true, 'key' => 'input_4'],
            'campusName'  => ['constraints' => ['Text'], 'optional' => true, 'key' => 'input_1'],
            'houseName'   => ['constraints' => ['Text'], 'optional' => true, 'key' => 'input_2'],
            'parentId'    => ['constraints' => ['Digit'], 'optional' => true, 'presence' => false, 'default' => 0, 'key' => 'input_6'],
            'threadId'    => ['constraints' => ['Digit'], 'optional' => true, 'presence' => false, 'default' => 0, 'key' => 'input_7'],
        ];
    }

    public static function getSearchFields()
    {
        return [
            'houseId' => ['constraints' => ['Digit'], 'optional' => true, 'presence' => false, 'key' => 'house_id', 'default' => false],
            'parentId' => ['constraints' => ['Digit'], 'optional' => true, 'presence' => false, 'key' => 'parent_id', 'default' => '0'],
            'page'    => ['constraints' => ['Digit'], 'optional' => true, 'presence' => false, 'default' => 1, 'key' => 'p'],
            'startDate' => ['constraints' => ['Date'], 'optional' => true, 'key' => 'menuDate1'],
            'endDate'   => ['constraints' => ['Date'], 'optional' => true, 'key' => 'menuDate2'],
        ];
    }
}
