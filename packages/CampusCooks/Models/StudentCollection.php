<?php
/**
 * Campus Cooks API
 */
namespace CampusCooks\Models;

use Reo\Collection\TraversableTrait;

class StudentCollection implements \ArrayAccess, \Countable, \IteratorAggregate
{
    use TraversableTrait;

    protected $db;
    protected $wpDbPrefix;

    public function __construct($db, $wpDbPrefix = 'wp_')
    {
        $this->db = $db;
        $this->wpDbPrefix = $wpDbPrefix; 
    }

    public function get($id)
    {
        //@note the id passed by the app is the meta entry id, get the real user ID
        if (false === ($userId = $this->db->get_column('select user_id from ' . $this->wpDbPrefix . 'usermeta where meta_value=:meta_value and meta_key=:meta_key', [':meta_value' => $id, ':meta_key' => 'entry_id']))) {
            return false;
        }
        if (isset($this->items[$userId])) {
            return $this->items[$userId];
        }
        $group = $this->db->get_row($sql = 'SELECT gg.* FROM ' . $this->wpDbPrefix . 'groups_user_group ug inner join ' . $this->wpDbPrefix . 'groups_group gg on gg.group_id = ug.group_id and ug.user_id = :id and gg.parent_id is not null and gg.group_id!=1 ', [':id' => $userId]);
        if (empty($group)) {
            return false;
        }
        return $this->items[$userId] = new Student((int) $userId, ['id' => $userId, 'houseId' => $group['group_id'], 'campusId' => $group['parent_id'], 'inHouse' => $this->isInHouse($userId, true)]);
    }

    public function isInHouse($userId, $save = false)
    {
        /*this is now updatable, just pull from user meta
        $col = $this->db->get_column('select in_house from cc_user where user_id = ' . $userId);
        if (false !== $col && !is_null($col)) {
            return (int) $col;
        }*/
        $col = $this->db->get_column('select meta_value from ' . $this->wpDbPrefix . 'usermeta where meta_key = \'live_in_house\' and user_id=' . $userId);
        $inHouse = !empty($col) && 0 == strcasecmp('Yes', $col) ? 1 : 0;
        if (12231 == $userId) {
            var_dump($col, $inHouse);exit;
        }
        if ($save) {
            $this->db->execute('update cc_user set in_house = ' . $inHouse . ' where user_id = ' . $userId);
        }
        return $inHouse;
    }

    public function getStudentGf($name, $houseId)
    {
        $sql = <<<'EOD'
select * from cc_user where CONCAT_WS(' ', first_name, last_name) = :name and house_id = :house_id
EOD;
        $data = $this->db->get_row($sql, $params = [':name' => $name, 'house_id' => $houseId]);
        if (empty($data)) {
            // var_dump($sql, $params, $this->db->err);
            return false;
        }
        return new Student((int) $data['user_id'], ['id' => $data['user_id'], 'houseId' => $data['house_id'], 'campusId' => $data['campus_id']]);
    }

    public function getStudentEmail($student)
    {
        $email = $this->db->get_column('select user_email from ' . $this->wpDbPrefix . 'users where ID = :id', [':id' => $student->id]);
        if (empty($email)) {
            return false;
        }
        return $email;
    }

/**
 * updateStudentData
 * 
 * @param Student $student the student object
 * @param $data array of new data to update
 * @param $email string the users current email address
 */ 
    public function updateStudentData($student, $data, $email)
    {
        if ($email !== $data['email']) { // changing email address, check db for registered user
            $row = $this->db->get_row('select * from ' . $this->wpDbPrefix . 'users where user_email = :email', [':email' => $data['email']]);
            if (!empty($row)) {
                return false;
            }
            // update email
            $this->db->query('update ' . $this->wpDbPrefix . 'users set user_email = :email where ID = :id', [':email' => $data['email'], ':id' => $student->id]);
        }
        // update other data w/o checking if change
        // meta data
        $this->db->query($sql = 'update ' . $this->wpDbPrefix . 'usermeta set meta_value = :value where meta_key = :key and user_id = :id', [':key' => 'last_name', ':value' => $data['lastName'], ':id' => $student->id]);
        $this->db->query($sql, [':key' => 'first_name', ':value' => $data['firstName'], ':id' => $student->id]);
        $this->db->query($sql, [':key' => 'live_in_house', ':value' => $data['inHouse'], ':id' => $student->id]);
        // custom db table
        $sql = 'update cc_user set first_name = :firstName, last_name = :lastName, email = :email, in_house = :inHouse where user_id = :id';
        $this->db->query($sql, [':firstName' => $data['firstName'], ':lastName' => $data['lastName'], ':email' => $data['email'], ':inHouse' => 'Yes' == $data['inHouse'] ? 0 : 1]);
        return true;
    }

/**
 * getHouseStudents
 * 
 * gets students by house or mulitple houses
 * 
 * @param houseIds array|int house ID or array of ids
 * 
 * @note uses wp groups tables for accuracy, the api student tables are filled on demand
 */ 
    public function getHouseStudents($houseIds, $count = true)
    {
        $ct = $this->db->get_column($sql = 'SELECT count(ug.user_id) FROM ' . $this->wpDbPrefix . 'groups_user_group ug inner join ' . $this->wpDbPrefix . 'groups_group gg on gg.group_id = ug.group_id and gg.group_id in (' . implode(',', (array) $houseIds) .')');
        if (empty($ct)) {
            return 0;
        }
        return $ct;
    }

    public static function getDataFields()
    {
        return [
            'lastName' => ['key' => 'last_name', 'constraints' => ['Text'], 'optional' => false, 'default' => ''],
            'firstName' => ['key' => 'first_name', 'constraints' => ['Text'], 'optional' => false, 'default' => ''],
            'email' => ['key' => 'email', 'constraints' => ['Email'], 'optional' => false, 'default' => ''],
            'inHouse' => ['key' => 'live_in_house', 'constraints' => ['Text'], 'optional' => false, 'default' => ''],
        ];
    }
}
