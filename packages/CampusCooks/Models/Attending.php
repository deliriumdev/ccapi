<?php
/**
 * Campus Cooks API
 */
namespace CampusCooks\Models;

use Reo\Collection\TraversableTrait;

class Attending
{
    public function __construct($id)
    {
        
        if (isset($fields)) {
            $this->items = $values;
        }
    }

    public static function getFields()
    {
        return [
            'studentId'   => ['constraints' => ['Digit'], 'key' => 'student_id'],
            'mealId'      => ['constraints' => ['Digit'], 'key' => 'input_1'],
            'studentName' => ['constraints' => ['Text', ['MinLength', 2]], 'key' => 'input_6'],
            'weekOf'      => ['constraints' => ['Date'], 'key' => 'input_2'],
            'mealTime'    => ['constraints' => ['Text'], 'key' => 'input_4'],
            'dayOfWeek'   => ['constraints' => ['Text'], 'key' => 'input_3'],
            'campusName'  => ['constraints' => ['Text'], 'optional' => true, 'key' => 'input_7'],
            'houseName'   => ['constraints' => ['Text'], 'optional' => true, 'key' => 'input_8'],
            'chefEmail'   => ['constraints' => ['Email'], 'key' => 'input_5'],
            //'studentId'   => ['constraints' => ['Digit'], 'optional' => true, 'key' => 'input_9'],
            'isRecurring' => ['filter' => 'CampusCooks\Models\EntryFactory::formatDate', 'presence' => false, 'optional' => true, 'key' => 'input_10', 'default' => 0],
            'recurringStopDate' => ['filter' => 'CampusCooks\Models\EntryFactory::formatDate', 'presence' => false, 'optional' => true, 'key' => 'input_11'],
            'requestDate' => ['filter' => 'CampusCooks\Models\EntryFactory::formatDate', 'key' => 'input_12'],
        ];
    }

    public static function mapData($data)
    {
        $data['input_values']['input_12'] = EntryFactory::getRequestDate($data['input_values']['input_2'], $data['input_values']['input_3']);
        return $data;
        //@depricated, sent as entry
        $mapped['input_values'] = [];
        foreach ($data['requestData'] as $key => $val) {
            if ('student_id' === $key) {
                continue;
            }
            $mapped['input_values']['input_' . $key] = $val;
        }
        $mapped['input_values']['student_id'] = $data['requestData']['student_id'];
        $mapped['input_values']['input_12'] = EntryFactory::getRequestDate($mapped['input_values']['input_2'], $mapped['input_values']['input_3']);
        return $mapped;
    }

    public static function getSearchFields()
    {
        return [
            'houseId' => ['constraints' => ['Digit'], 'optional' => true, 'presence' => false, 'key' => 'house_id', 'default' => false],
        ];
    }

    public static function getUpdateFields()
    {
        return [
            'houseId' => ['constraints' => ['Digit'], 'optional' => true, 'presence' => false, 'key' => 'house_id', 'default' => false],
            'values' => ['optional' => true, 'presence' => false, 'key' => 'values', 'default' => 0],
            'value' => ['optional' => true, 'presence' => false, 'key' => 'value', 'default' => 0],
            'weekOf' => ['constraints' => ['Date'], 'optional' => true, 'presence' => false, 'key' => 'weekOf', 'default' => ''],
            'isLateplate' => ['constraints' => ['Digit'], 'optional' => true, 'presence' => false, 'key' => 'dataType', 'default' => 0],
            'userId' => ['constraints' => ['Digit'], 'key' => 'user_id', 'default' => 0],
            'signature' => ['constraints' => ['Text'], 'optional' => true, 'presence' => false, 'key' => 'token', 'default' => ''],
            'menuId' => ['constraints' => ['Digit'], 'optional' => true, 'presence' => false, 'key' => 'pk', 'default' => 0],
        ];
    }
}
