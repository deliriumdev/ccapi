<?php
/**
 * Campus Cooks API
 */
namespace CampusCooks\Models;

class LateplateCollection extends AbstractEntryCollection
{
    protected $start;

    protected static $mealtimes = [
        1 => 'Lunch',
        2 => 'Dinner',
        3 => 'Breakfast',
    ];

    public function __construct($db, $services)
    {
        parent::__construct($db, $services);
        if (null === ($this->start = $services->get('Services')->get('Config')->get('api')->get('start')) || empty($this->start)) {
            $this->start = false;
        }
    }

/**
 * getToday
 * 
 * get todays lateplates by mealtime
 * 
 * @param $mealtime string the meal time, dinner|lunch
 */ 
    public function getToday($mealtime, $tz, $sort = true)
    {
        $sql = <<<'EOD'
select entry.student_id, entry.house_id, entry.campus_id, lateplate.*, DATE_FORMAT(lateplate.request_date, '%m/%d/%Y') as formatted_date, 
DATE_FORMAT(CURDATE(), '%m/%d/%Y') as recur_date, campus.tz, campus.campus_name, house.house_name, user.first_name, user.last_name, user.in_house   
from (((request_entry entry inner join lateplate_entry lateplate on lateplate.entry_id = entry.entry_id) 
inner join cc_campus campus on campus.campus_id = entry.campus_id and campus.tz=:tz) left join cc_house house on house.house_id = entry.house_id)
left join cc_user user on user.user_id = entry.student_id 
where (lateplate.request_date = CURDATE() or 
(DAYOFWEEK(CURDATE()) = DAYOFWEEK(lateplate.request_date) and recurring_stop_date > CURDATE() and is_recurring = 1)) and lateplate.meal_time = :mealtime 
EOD;
        $rows = $this->db->get_array($sql, $params = [':tz' => $tz, ':mealtime' => 'lunch' === $mealtime ? 1 : ('breakfast' === $mealtime ? 3 : 2)]);
        if (empty($rows)) {
            //var_dump($sql, $this->db->err);
            return false;
        }

        // populate the tables that may not have data from legacy wp groups
        $lateplates = [];
        $students = $this->services->get('Students'); // to get inhouse from wp meta
        foreach ($rows as $key => $data) {
            if ('1' === $data['is_recurring']) {
                $rows[$key]['formatted_date'] = $data['recur_date'];
            }
            if (is_null($data['in_house'])) {
                $rows[$key]['in_house'] = $students->isInHouse($data['student_id']);
            }
            $rows[$key]['mealtime'] = isset(self::$mealtimes[$data['meal_time']]) ? self::$mealtimes[$data['meal_time']] : 'Dinner';
        }
        $this->populateItems($rows, $sort);
        return $this;
    }

/**
 * getTodayByHour
 * 
 * get todays lateplates by hour of day from house settings
 * 
 * @param $mealtime string the meal time, dinner|lunch
 */ 
    public function getTodayByHour($hour, $tz, $sort = true)
    {
        $sql = <<<'EOD'
select entry.student_id, entry.house_id, entry.campus_id, lateplate.*, DATE_FORMAT(lateplate.request_date, '%m/%d/%Y') as formatted_date, 
DATE_FORMAT(CURDATE(), '%m/%d/%Y') as recur_date, campus.tz, campus.campus_name, house.house_name, user.first_name, user.last_name, user.in_house   
from (((request_entry entry inner join lateplate_entry lateplate on lateplate.entry_id = entry.entry_id) 
inner join cc_campus campus on campus.campus_id = entry.campus_id and campus.tz=:tz) inner join cc_house house on house.house_id = entry.house_id)
left join cc_user user on user.user_id = entry.student_id 
where (lateplate.request_date = CURDATE() or 
(DAYOFWEEK(CURDATE()) = DAYOFWEEK(lateplate.request_date) and recurring_stop_date > CURDATE() and is_recurring = 1 and archived = 0)) 
and ((house.cutoff_lunch = :hour and house.lateplate_lunch = 1 and lateplate.meal_time =1) or (house.cutoff_dinner = :hour and house.lateplate_dinner = 1 and lateplate.meal_time = 2))
EOD;
        $rows = $this->db->get_array($sql, $params = [':tz' => $tz, ':hour' => $hour]);
        if (empty($rows)) {
            //var_dump($sql, $this->db->err);
            return false;
        }

        // populate the tables that may not have data from legacy wp groups
        $lateplates = [];
        $students = $this->services->get('Students'); // to get inhouse from wp meta
        foreach ($rows as $key => $data) {
            if ('1' === $data['is_recurring']) {
                $rows[$key]['formatted_date'] = $data['recur_date'];
            }
            if (is_null($data['in_house'])) {
                $rows[$key]['in_house'] = $students->isInHouse($data['student_id']);
            }
            $rows[$key]['mealtime'] = isset(self::$mealtimes[$data['meal_time']]) ? self::$mealtimes[$data['meal_time']] : 'Dinner';
        }
        return $rows;
        $this->populateItems($rows, $sort);
        return $this;
    }

/**
 * getTodayMealByHour
 * 
 * get todays lateplates by mealtime
 * @note this checks the individual meals for lateplate settings which is not implemented, ie not saved with meal entry form
 * 
 * @param $hour current hour in timezone, $tz the timezone
 */ 
    public function getTodayMealByHour($hour, $tz, $sort = true)
    {
        $sql = <<<'EOD'
select sched.*, campus.tz from cc_meal_schedule sched inner join cc_campus campus on campus.campus_id = sched.campus_id  
where schedule_date = CURDATE() and sched.is_lateplate = 1 and sched.lateplate_hour = :hour and campus.tz = :tz
EOD;
        $meals = $this->db->get_array($sql, $params = [':hour' => $hour, ':tz' => $tz]);
        if (empty($meals)) {
            //var_dump($sql, $params, $this->db->err);
            return false;
        }
//@note the lateplates are not specically assigned a meal, it simply goes by the house, current date and mealtime
        $sql = <<<'EOD'
select entry.student_id, entry.house_id, entry.campus_id, lateplate.*, DATE_FORMAT(lateplate.request_date, '%m/%d/%Y') as formatted_date, 
DATE_FORMAT(CURDATE(), '%m/%d/%Y') as recur_date, campus.tz, campus.campus_name, house.house_name, user.first_name, user.last_name, user.in_house   
from (((request_entry entry inner join lateplate_entry lateplate on lateplate.entry_id = entry.entry_id) 
inner join cc_campus campus on campus.campus_id = entry.campus_id) 
inner join cc_house house on house.house_id = entry.house_id)
left join cc_user user on user.user_id = entry.student_id
where (lateplate.request_date = CURDATE() or 
(DAYOFWEEK(CURDATE()) = DAYOFWEEK(lateplate.request_date) and recurring_stop_date > CURDATE() and is_recurring = 1)) 
and lateplate.meal_time = :mealtime and entry.house_id = :houseID
EOD;
        $rows = [];
        foreach ($meals as $item) {
            $lateplates = $this->db->get_array($sql, $params = [':mealtime' => $item['meal_time'], ':houseID' => $item['house_id']]);
            if (!empty($lateplates)) {
                $rows = array_merge($rows, $lateplates);
            }
            else {
                var_dump($sql, $this->db->err, $params);
            }
        }
        if (empty($rows)) {
            //var_dump($sql, $this->db->err);
            return false;
        }

        // populate the tables that may not have data from legacy wp groups
        $lateplates = [];
        $students = $this->services->get('Students'); // to get inhouse from wp meta
        foreach ($rows as $key => $data) {
            if ('1' === $data['is_recurring']) {
                $rows[$key]['formatted_date'] = $data['recur_date'];
            }
            if (is_null($data['in_house'])) {
                $rows[$key]['in_house'] = $students->isInHouse($data['student_id']);
            }
            $rows[$key]['mealtime'] = isset(self::$mealtimes[$data['meal_time']]) ? self::$mealtimes[$data['meal_time']] : 'Dinner';
        }
        return $rows;
    }

    public function getLateplates($studentId, $weekOf = null, $mealTime = 'Dinner')
    {
        $sql = <<<'EOD'
select lateplate.lateplate_id as id, lateplate.meal_time as mealtime, lateplate.chef_message, DATE_FORMAT(lateplate.request_date, '%m/%d/%Y') as formatted_date, 
lateplate.is_recurring, DATE_FORMAT(recurring_stop_date, '%m/%d/%Y') as stop_date 
from (((request_entry entry inner join lateplate_entry lateplate on lateplate.entry_id = entry.entry_id) 
inner join cc_campus campus on campus.campus_id = entry.campus_id) left join cc_house house on house.house_id = entry.house_id) 
left join cc_user user on user.user_id = entry.student_id 
EOD;
        if (!empty($weekOf)) {
            $meal = 'Lunch' === $mealTime ? 1 : 2; // lunch of dinner
            $sql .= 'where ((lateplate.request_date >= ' . ($date = $this->db->quote($weekOf)) . ' and lateplate.request_date < DATE_ADD(' . $date . ', INTERVAL 7 DAY)) or (recurring_stop_date >= ' . $date . ' and is_recurring = 1 and lateplate.archived != 1)) and meal_time = ' . $meal . ' and entry.student_id=:student_id';
        }
        else {// current lateplates
            $sql .= 'where (lateplate.request_date >= CURDATE() or (recurring_stop_date >= CURDATE() and is_recurring = 1 and lateplate.archived != 1)) and entry.student_id=:student_id';
        }
//entry.student_id, entry.house_id, entry.campus_id,and lateplate.archived != 1 
//campus.tz, campus.campus_name, house.house_name, user.first_name, user.last_name
        $rows = $this->db->get_array($sql, $params = [':student_id' => $studentId], 'id');
        if (empty($rows)) {
            // var_export($sql);echo $this->db->err, $studentId, "\n";
            return [];
        }

        foreach ($rows as $key => $data) {
            if ('1' !== $data['is_recurring']) {
                unset($rows[$key]['stop_date']);
            }
            $rows[$key]['is_recurring'] = (int) $data['is_recurring'];
            if (1 === $rows[$key]['is_recurring']) {
                $rows[$key]['dow'] = date('D', strtotime($data['formatted_date']));
            }
            $rows[$key]['mealtime'] = isset(self::$mealtimes[$data['mealtime']]) ? self::$mealtimes[$data['mealtime']] : 'Dinner';
        }
        return $rows;
    }

/**
 * get
 * 
 * @param $houses an array of house IDs
 */ 
    public function get($houses, $all = false)
    {
        $sql = <<<'EOD'
select lateplate.lateplate_id as id, lateplate.meal_time as mealtime, lateplate.chef_message, DATE_FORMAT(lateplate.request_date, '%a %m/%d/%Y') as formatted_date, 
UNIX_TIMESTAMP(lateplate.request_date) as ts, lateplate.is_recurring, DATE_FORMAT(recurring_stop_date, '%m/%d/%Y') as stop_date, user.first_name, user.last_name, user.in_house, 
DATE_FORMAT(entry.entry_date, '%m/%d/%Y') as entry_date, UNIX_TIMESTAMP(entry.entry_date) as entry_ts, entry.student_id, entry.campus_id, entry.house_id, campus.tz, house.house_name 
from (((request_entry entry inner join lateplate_entry lateplate on lateplate.entry_id = entry.entry_id and lateplate.archived = 0) 
inner join cc_campus campus on campus.campus_id = entry.campus_id) inner join cc_house house on house.house_id = entry.house_id) 
left join cc_user user on user.user_id = entry.student_id
EOD;

        $conds = (false !== $this->start) ? ['lateplate.request_date > \'' . $this->start . '\''] : [];
        if (!$all) {
            $conds[] = '(lateplate.request_date = CURDATE() or (DAYOFWEEK(CURDATE()) = DAYOFWEEK(lateplate.request_date) and recurring_stop_date > CURDATE() and is_recurring = 1))';
        }
        if ($isLive = (false !== $houses)) {
            $conds[] = 'entry.house_id in (' . implode(',', $houses) . ')';
        }
        if (!empty($conds)) {
            $sql .= ' where ' . implode(' and ', $conds);
        }
//entry.student_id, entry.house_id, entry.campus_id,
//campus.tz, campus.campus_name, house.house_name, user.first_name, user.last_name
        $rows = $this->db->get_array($sql);//, $params = $isLive ? [':house_id' => $houseId] : null, 'id'
        if (empty($rows)) {
            //var_export($sql);echo $this->db->err, "\n";
            return [];
        }
        $today = date('D m/d/Y');
        $day = date('l');
        $students = $this->services->get('Students');// to get inhouse from wp meta
        foreach ($rows as $key => $data) {
            if ('1' !== $data['is_recurring']) {
                unset($rows[$key]['stop_date']);
            }
            $rows[$key]['is_recurring'] = (int) $data['is_recurring'];
            if ($data['is_recurring']) {
                if ($all) {
                    $rows[$key]['formatted_date'] = ($day == ($nextDay = date('l', $data['ts']))) ?  $today : date('D m/d/Y', strtotime('next ' . $nextDay));
                }
                else {
                    $rows[$key]['formatted_date'] = $today;
                }
            }
            $rows[$key]['mealtime'] = isset(self::$mealtimes[$data['mealtime']]) ? self::$mealtimes[$data['mealtime']] : 'Dinner';
            if (empty($data['last_name']) && false !== ($user = $this->services->get('WpImport')->importUser($data['student_id'], $data['campus_id'], $data['house_id']))) {
                $rows[$key]['first_name'] = $user['first_name'];
                $rows[$key]['last_name'] = $user['last_name'];
            }
            $rows[$key]['student'] =  $rows[$key]['first_name'] . ' ' . $rows[$key]['last_name'];
            $rows[$key]['sortname'] = $rows[$key]['last_name'] . ' ' .  $rows[$key]['first_name'];
            if (is_null($data['in_house'])) {
                $rows[$key]['in_house'] = $students->isInHouse($data['student_id']);
            }
            //$rows[$key]['house_name'] .= ' ' . $data['house_id'];
            unset($rows[$key]['first_name'], $rows[$key]['last_name']);
            // this is relative to CST ???
            if ('EST' === $data['tz']) {
                $data['entry_ts'] += 3600;
            }
            if ('MST' === $data['tz']) {
                $data['entry_ts'] -= 3600;
            }
            if ('PST' === $data['tz']) {
                $data['entry_ts'] -= 7200;
            }
            $rows[$key]['time'] = date('h:i A', $data['entry_ts']);
        }
        return $rows;
    }

    public function getAvg($houseIds, $total = true)
    {
        // @note where request_date > certian date does not work with aggregate group by week
        $sql = 'select count(lp.lateplate_id) as ct, WEEK(lp.request_date) as weekNo from lateplate_entry lp inner join request_entry entry on entry.entry_id = lp.entry_id';
        $sql .= ' where year(lp.request_date) = 2017 and entry.house_id in (' . implode(',', $houseIds) . ') group by WEEK(lp.request_date)';
        // var_dump($sql);
        if (false === ($data = $this->db->get_array($sql)) || empty($data)) {
            // var_dump($sql, $this->db->err);
            return $total ? 0 : false;
        }
        if (!$total) {
            return $data;
        }
        // var_dump($data);
        $flat = array_map(function($item){ return $item['ct'];}, $data);
        return round(array_sum($flat) / count($flat), 0);
    }

    public function getAvgByWeekday($houseIds, $weekOf)
    {
        $data = [];
        // @note where request_date > certian date does not work with aggregate group by week
         $sql = <<<'EOD'
select count(lp.lateplate_id) as ct, WEEKDAY(lp.request_date) as weekDay, house.house_name from 
lateplate_entry lp inner join request_entry entry on entry.entry_id = lp.entry_id 
inner join cc_house house on house.house_id = entry.house_id
EOD;
        foreach ($houseIds as $id) { // get series data for each house
            $where = ' where year(lp.request_date) = 2017 and entry.house_id = ' . $id . ' group by WEEKDAY(lp.request_date)';
            if (false === ($rows = $this->db->get_array($query = $sql . $where, null, 'weekDay')) || empty($rows)) {
                // var_dump($query, $this->db->err);exit;
                continue;
            }
            // fill in any missing weekdays
            $sample = current($rows);
            $house = $sample['house_name'];
            for ($xx = 0; $xx < 7; $xx++) {
                if (empty($rows[$xx])) {
                    $rows[$xx] = ['ct' => 0];
                }
            }
            ksort($rows); // the pushed numeric keys will not be in order so it has to be sorted
            $data[] = ['house' => $house, 'data' => array_map(function($item){return (int) $item['ct'];}, $rows)];
        }
        return $data;
    }

    public function remove($id)
    {
        $sql = 'delete entry.*, lateplate.* from request_entry entry inner join lateplate_entry lateplate on lateplate.entry_id = entry.entry_id where lateplate.lateplate_id = :id';
        $this->db->execute($sql, [':id' => $id]);
        return true;
    }
}
