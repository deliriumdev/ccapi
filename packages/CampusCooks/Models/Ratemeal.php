<?php
/**
 * Campus Cooks API
 */
namespace CampusCooks\Models;

use Reo\Collection\TraversableTrait;

class Ratemeal
{
    public function __construct($id)
    {
        
        if (isset($fields)) {
            $this->items = $values;
        }
    }

    public static function getFields()
    {
        return [
            'studentId'   => ['constraints' => ['Digit'], 'key' => 'student_id'],
            'menuId'      => ['constraints' => ['Digit'], 'key' => 'input_1'],
            'studentName' => ['constraints' => ['Text'], 'key' => 'input_4'],
            'itemName'    => ['constraints' => ['Text'], 'key' => 'input_7'],
            'requestDate' => ['filter' => 'CampusCooks\Models\EntryFactory::formatDate', 'key' => 'input_3'],
            'campusName'  => ['constraints' => ['Text'], 'optional' => true, 'key' => 'input_5'],
            'houseName'   => ['constraints' => ['Text'], 'optional' => true, 'key' => 'input_6'],
            'rating'      => ['constraints' => ['Digit'], 'key' => 'input_2'],
            'comments'    => ['constraints' => ['Text'], 'optional' => true, 'key' => 'input_3'],
        ];
    }

    public static function getGetFields()
    {
        return [
            // 'studentId' => ['constraints' => ['Digit'], 'key' => 'student_id'], // student name is 4
            'menuId'    => ['constraints' => ['Digit'], 'optional' => true, 'presence' => false, 'key' => 'menu_id'],
            'house'     => ['constraints' => ['Text'], 'optional' => true, 'presence' => false, 'key' => 'house'],
            'meal'      => ['constraints' => ['Text'], 'optional' => true, 'presence' => false, 'key' => 'meal'],
        ];
    }
}
