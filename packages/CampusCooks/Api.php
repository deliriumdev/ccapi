<?php
/**
 * Campus Cooks API
 */
namespace CampusCooks;

use CampusCooks\Models\Lateplate;
use CampusCooks\Models\Attending;
use CampusCooks\Models\Ratemeal;
use CampusCooks\Models\Feedback;
use CampusCooks\Models\Forum;
use CampusCooks\Models\House;
use CampusCooks\Models\Menu;
use Reo\Form\FormHandler;
use Reo\Routing\Route;

class Api
{
    protected $services;
    protected $formHandler;
    protected $apiKey;
    protected $privateKey;
    protected $hmacKey;
    protected $errors = [];
    protected $config;
    protected $apiServices;

    public function __construct($services)
    {
        $this->services = $services;
        $this->config = $this->services->get('Config')->api;
        $this->apiKey = $this->config->key;
        $this->privateKey = $this->config->private_key;
        $this->hmacKey = $this->config->hmac_key;
    }

/**
 * registerRoutes
 * 
 * register routes used by this API
 */
    public function registerRoutes()
    {
        $routeCollection = $this->services->get('RouteCollection');
        $routeCollection->add('submissions', new Route('/forms/[form=*s]', [$this, 'handleFormSubmission'], ['method' => 'POST', 'protocol' => 'https'])); 
        $routeCollection->add('entries', new Route('/forms/[id=%d]/entries', [$this, 'handleEntryPost'], ['method' => 'POST', 'protocol' => 'https']));
        $routeCollection->add('data', new Route('/data/[form=*s]/[student_id=%d]', [$this, 'handleGetData'], ['method' => 'GET', 'protocol' => 'https']));
        $routeCollection->add('search', new Route('/[type=*s]/search', [$this, 'handleSearch'], ['method' => 'POST', 'protocol' => 'https']));
        $routeCollection->add('update', new Route('/[type=*s]/update/[id=%d]', [$this, 'handleUpdate'], ['method' => 'POST', 'protocol' => 'https']));
        $routeCollection->add('remove', new Route('/[type=*s]/remove/[id=%d]', [$this, 'handleRemove'], ['method' => 'POST', 'protocol' => 'https']));
        $routeCollection->add('fetchData', new Route('/[type=*s]/data', [$this, 'handleFetch'], ['method' => 'POST', 'protocol' => 'https']));
        $routeCollection->add('notices', new Route('/notices/[student_id=%d]/[version=*s]', [$this, 'handleNotifications'], ['method' => 'GET', 'protocol' => 'https']));
        //$routeCollection->add('testmail', new Route('/testmail', [$this, 'sendTestMail'], ['method' => 'GET', 'protocol' => 'https']));
    }

/**
 * handleFormSubmission
 * 
 * @note date and bool filters set a default so validation is not necessary
 */ 
    public function handleFormSubmission($args, $data = null)
    {
        $this->services->get('Logger')->info(sprintf('Data: %s', var_export($content = $this->services->get('Request')->getContent(), true)));
        if ((!isset($data) && (empty($content) || null == ($data = json_decode($content, true)))) || empty($data['input_values'])) {
            $this->errors[] = 'Invalid request';
            $this->sendErrorResponse();
            return true;
        }
        if (!$this->checkApiCreds()) {
            $this->sendErrorResponse();
            return true;
        }

        //$data = json_decode('{"input_values":{"input_10":false,"input_2":"Lunch","input_6":"Hi Chef!","student_id":"43530","input_1":"Frank Tank","input_4":"Test","input_5":"Test - Delirium","input_3":"02/26/2016"}}', true);
        switch ($args['form']) {
            case 'lateplate':
                $form = $this->services->get('FormHandler')->setFields($fields = LatePlate::getFields());
                $method = 'Lateplate';
            break;
            case 'attending':
                $form = $this->services->get('FormHandler')->setFields($fields = Attending::getFields());
                $method = 'Attending';
                $data = Attending::mapData($data);
            break;
            case 'ratemeal':
                $form = $this->services->get('FormHandler')->setFields($fields = Ratemeal::getFields());
                $method = 'Ratemeal';
            break;
            case 'feedback':
                $form = $this->services->get('FormHandler')->setFields($fields = Feedback::getFields());
                $method = 'Feedback';
            break;
            case 'forum':
                $form = $this->services->get('FormHandler')->setFields($fields = Forum::getFields());
                $method = 'Forum';
            break;
            default:
                $form = false;
            break;
        }
        if (false === $form) {
            $this->errors[] = 'Form not found';
            $this->services->get('Logger')->error('Invalid Form');
            $this->sendErrorResponse();
            return true;
        }

        $form->validate($this->mapInputFields($data['input_values'], $fields));
        if ($form->hasViolations()) {
            //@note this should be logging and/or returning an error
            $this->services->get('Logger')->error('Form Violation');
            return true;
            //var_dump($form->getViolations());return true;
        }
        $data = $form->toArray();
        if (false == ($student = $this->getService('Students')->get($data['studentId']))) {
            $this->errors[] = 'Student Not Found';
            $this->services->get('Logger')->error(sprintf('Student not found [%s]', $data['studentId']));
            $this->sendErrorResponse();
            return true;
        }
        if (false == ($entryId = $this->getService('Entrys')->{'add' . $method}($student, $data))) {
            $error = $this->getService('Entrys')->getLastError();
            $this->services->get('Logger')->error(sprintf('Entry error [%s]', $error));
            $this->errors[] = false === $error ? 'Could not process request' : $error;
            $this->sendErrorResponse();
            return true;
        }
        $this->sendResponse(['response' => ['is_valid' => true, 'entry_id' => (int) $entryId]]);
        return true;
    }

    public function handleEntryRequest()
    {
        $this->errors[] = 'Access Denied';
        $this->sendErrorResponse();
        return true;
    }

    public function handleEntryPost($args)
    {
        return $this->handleEntryRequest();
        //@depricated, uses /form route now
        if (21 != $args['id']) {
            return $this->handleEntryRequest();
        }
        $this->services->get('Logger')->info(sprintf('Entry Data: %s', var_export($content = $this->services->get('Request')->getContent(), true)));
        //"requestData":{"1":"46967","2":"02/29/2016","3":"Monday","4":"Dinner","5":"schuyler@gatordev.com","6":"Frank Tank","7":"Test","8":"Test - Delirium"
        if (empty($content) || null == ($data = json_decode($content, true)) || empty($data['requestData'])) {
            $this->errors[] = 'Invalid request';
            $this->sendErrorResponse();
            return true;
        }
        // map and pass to form handler
        return $this->handleFormSubmission(['form' => 'attending'], Attending::mapData($data));
    }

    public function handleNotifications($args)
    {
        if (!isset($args['student_id']) || !ctype_digit($args['student_id']) || false == ($student = $this->getService('Students')->get($args['student_id']))) {
            $this->errors[] = 'Student Not Found';
            $this->sendErrorResponse();
            return true;
        }
        if (false === ($notifications = $this->getService('Notifications')->get($student))) {
            $notifications = [];
        }
        if (!empty($args['version']) && $this->config->get('mobile_version') != ($args['version'])) {
            array_unshift($notifications, ['noticeID' => 0, 'content' => $this->config->get('upgrade_message')]);
        }
        $this->sendResponseObjects($notifications);
        return true;
    }

    public function handleGetData($args)
    {
        /*if (!$this->checkApiCreds()) {
            $this->sendErrorResponse();
            return true;
        }*/
        if (!isset($args['student_id']) || !ctype_digit($args['student_id'])) {
            $this->errors[] = 'Student Not Found';
            $this->sendErrorResponse();
            return true;
        }
        // @note gf api is forwarded here in apache RedirectMatch ^/?gravityformsapi/forms/10/entries/(.*)$ https://mycampuscooks.com/ccapi/data/ratemeal/1/$1
        $isGf = false;
        if ($isGf = ('ratemeal' === $args['form'] && '1' === $args['student_id'])) {
            if (false === ($search = json_decode($_GET['search'], true)) || false === ($data = $this->getService('Ratemeal')->getGfData($search))) {
                $this->errors[] = 'Student Not Found';
                $this->sendErrorResponse();
                return true;
            }
            $student = $data['student'];
        }
        elseif (false == ($student = $this->getService('Students')->get($args['student_id']))) {
            $this->errors[] = 'Student Not Found';
            $this->sendErrorResponse();
            return true;
        }

        if ('lateplate' === $args['form']) {
            $lateplates = $this->getService('Lateplate')->getLateplates($student->id);
            if (empty($lateplates)) { //65122 == $args['student_id']
                // get date in users timezone
                if (false === ($campus = $this->getService('Campus')->get($student->campusId))) {
                    $timezone = 'CST';
                }
                else {
                    $timezone = $campus['tz'];
                }
                if ('UTC' !== ($resetTz = date_default_timezone_get())) {
                    date_default_timezone_set('UTC');
                }
                $this->getService('ReportRunner'); // loads the class
                $date = date('m/d/Y', time() + Reports\Scheduler::getTimeZoneOffset($timezone));
                $lateplates = [['id' => 0, 'mealtime' => 'You have no requested Late Plates', 'chef_message' => '', 'formatted_date' => $date, 'is_recurring' => 0]];
            }
            //var_dump($student->id, $lateplates);exit;
            $this->sendResponseObjects($lateplates);
            return true;
        }
        if ('ratemeal' === $args['form']) {
            if (!$isGf) {
                $form = $this->services->get('FormHandler')->setFields($fields = Ratemeal::getGetFields());
                if (isset($_GET['search']) && false !== ($search = json_decode($_GET['search'], true))) {
                    $_GET = array_replace($_GET, $search);
                }
                $form->validate($this->mapInputFields($_GET, $fields), false);
                if ($form->hasViolations()) {
                    //return empty
                }
                $data = $form->toArray();
            }
            // var_dump($data);return true;
            if (false === ($rating = $this->getService('Ratemeal')->getRating($student->id, $data['menuId'], $data['meal']))) {
                $content = ['response' => ['total_count' => 0]];
            }
            else {
                $content = ['response' => ['total_count' => 1,"entries" => [0 => [2 => $rating]]]];
            }
            $this->sendResponse($content);
            return true;
        }
        $this->errors[] = 'Invalid Form Requested';
        $this->sendErrorResponse();
        return true;
    }

    public function handleSearch($args)
    {
        if (empty($args['type'])) {
            $this->errors[] = 'Invalid search type';
            $this->sendErrorResponse();
            return true;
        }
        switch ($args['type']) {
            case 'menu':
                return $this->getController('Menus')->search($args);
            break;
            case 'budget':
                return $this->getController('Budget')->search($args);
            break;
            case 'feedback':
                return $this->handleFeedback($args);
            break;
            case 'forum':
                return $this->handleForum($args);
            break;
            case 'lateplate':
                return $this->handleAttending($args, true);
            break;
            case 'attending':
                return $this->handleAttending($args);
            break;
            case 'ratemeal':
                return $this->handleRatemeal($args);
            break;
            case 'house':
                return $this->getController('Houses')->search($args);
            break;
            case 'email_sent':
                return $this->getController('Reports')->search($args);
            break;
            
            default:
                $this->errors[] = 'Invalid search type';
                $this->sendErrorResponse();
                return true;
            break;
        }
    }

    public function handleUpdate($args)
    {
        if (empty($args['type'])) {
            $this->errors[] = 'Invalid request';
            $this->sendErrorResponse();
            return true;
        }
        switch ($args['type']) {
            case 'lateplate':
                return $this->getController('Lateplates')->update($args);
            break;
            case 'student':
                return $this->getController('Users')->update($args);
            break;
            case 'budget':
                return $this->getController('Budget')->update($args);
            break;
            case 'plates':
                return $this->getController('Plates')->update($args);
            break;
            default:
                $this->errors[] = 'Invalid request';
                $this->sendErrorResponse();
                return true;
            break;
        }
    }

    public function handleFetch($args)
    {
        if (empty($args['type'])) {
            $this->errors[] = 'Invalid request';
            $this->sendErrorResponse();
            return true;
        }
        switch ($args['type']) {
            case 'house':
                return $this->getController('Houses')->getData($args);
            break;
            case 'menu':
                return $this->getController('Menus')->getData($args);
            break;
            case 'permissions':
                return $this->getController('Roles')->getData($args);
            break;
            case 'stats':
                return $this->getController('Stats')->getData($args);
            break;
            case 'plates':
                return $this->getController('Plates')->getData($args);
            break;
            case 'app':
                return $this->getController('App')->getData($args);
            break;
            default:
                $this->errors[] = 'Invalid request';
                $this->sendErrorResponse();
                return true;
            break;
        }
    }

    public function handleRemove($args)
    {
        if (empty($args['type'])) {
            $this->errors[] = 'Invalid request context not specified';
            $this->sendErrorResponse();
            return true;
        }
        switch ($args['type']) {
            case 'lateplate':
                return $this->getController('Lateplates')->remove($args, true);
            break;
            case 'budget':
                return $this->getController('Budget')->remove($args, true);
            break;

            default:
                $this->errors[] = 'Invalid request context not implemented';
                $this->sendErrorResponse();
                return true;
            break;
        }
    }

    public function removeLateplate($args)
    {
        $post = $this->services->get('Request')->request;
        if (empty($args['id']) || empty($post['token']) || empty($post['id']) || !ctype_digit($post['id'])) {
            $this->errors[] = 'Invalid request';
            $this->sendErrorResponse();
            return true;
        }
        // @note the user array is the content
        if (!$this->validateSignature($post['token'], json_encode(['user' => (int)$post['id']]))) {
            $this->errors[] = 'Invalid request';
            $this->sendErrorResponse();
            return true;
        }
        $this->getService('Lateplate')->remove($args['id']);
        $this->sendResponse(['success' => 1]);
        $this->services->getNew('Logger', ['logDir' => $this->services->get('Config')->logger->logdir . '/remove.log'])->info(sprintf('Lateplate [%d] removed by [%d]', $args['id'], $post['id']));
        return true;
    }

    public function handleFeedback($args)
    {
        $feedback = $this->getService('Feedback');
        // process the post
        $form = $this->services->get('FormHandler')->setFields($fields = Feedback::getSearchFields());
        $form->validate($this->mapInputFields($_POST, $fields), false); // all search criteria is optional, do not care if there are violations, it will just skip
        if ($form->hasViolations()) {
            //var_export($form->getViolations());
        }
        $data = $form->toArray();
        if (empty($data['userId'])) {
            $houses = [0];
        }
        else {
            $houses = $this->getService('Roles')->getHouseAccess($user = $this->getService('Users')->get($data['userId']));
        }
        $results = $feedback->get($houses, $data['startDate'], $data['endDate'], ($isCsv = !empty($data['format']) && 'csv' === $data['format']) ? 0 : (empty($data['page']) ? 1 : $data['page']), empty($data['parentId']) ? '0' : $data['parentId']);
        // at this point can either send data in json for the frontend to render or deliver the template from here
        if ($isCsv) {
            $content = $results->toArray();
        }
        else {
            $view = new Views\Feedback($results); //@note $results is the feedback collection object
            $content = $view->getContent();
        }
        $this->sendResponse(['count' => $feedback->getResultCount(), 'pageCount' => $feedback->getPaginator()->last, 'page' => $data['page'], 'html' => $content]);
        return true;
    }

    public function handleAttending($args, $late = false)
    {
        $lateplates = $this->getService($late ? 'Lateplate' : 'Attending');
        // process the post
        $form = $this->services->get('FormHandler')->setFields($fields = Lateplate::getSearchFields());// both attending and lateplate have same fields
        $form->validate($this->mapInputFields($_POST, $fields), false); // all search criteria is optional, do not care if there are violations, it will just skip
        if ($form->hasViolations()) {
            //var_export($form->getViolations());
        }
        $data = $form->toArray();
        if (empty($data['userId'])) {
            $houses = [0];
        }
        else {
            $houses = $this->getService('Roles')->getHouseAccess($user = $this->getService('Users')->get($data['userId']));
        }
        $results = $lateplates->get($houses, !empty($data['days']) && 1 == $data['days']);
        if (empty($results)) {
            $results = [];
        }
        // at this point can either send data in json for the frontend to render or deliver the template from here
        $data = array(
            'sEcho' => 1,
            'iTotalRecords' => $count = count($results),
            'iTotalDisplayRecords' => $count,
            'aaData' => $results,
        );
        $this->sendResponse($data);
        return true;
    }

    public function handleRatemeal($args)
    {
        $ratings = $this->getService('Ratemeal');
        // process the post
        $form = $this->services->get('FormHandler')->setFields($fields = Lateplate::getSearchFields());// both attending and lateplate have same fields
        $form->validate($this->mapInputFields($_POST, $fields), false); // all search criteria is optional, do not care if there are violations, it will just skip
        if ($form->hasViolations()) {
            //var_export($form->getViolations());
        }
        $data = $form->toArray();
        if (empty($data['userId'])) {
            $houses = [0];
        }
        else {
            $houses = $this->getService('Roles')->getHouseAccess($user = $this->getService('Users')->get($data['userId']));
        }
        $results = $ratings->get($houses);
        if (empty($results)) {
            $results = [];
        }
        // at this point can either send data in json for the frontend to render or deliver the template from here
        $data = array(
            'sEcho' => 1,
            'iTotalRecords' => $count = count($results),
            'iTotalDisplayRecords' => $count,
            'aaData' => $results,
        );
        $this->sendResponse($data);
        return true;
    }

    public function handleForum($args)
    {
        $forum = $this->getService('Forum');
        // process the post
        $form = $this->services->get('FormHandler')->setFields($fields = Feedback::getSearchFields());
        $form->validate($this->mapInputFields($_POST, $fields), false); // all search criteria is optional, do not care if there are violations, it will just skip
        if ($form->hasViolations()) {
            //var_export($form->getViolations());
        }
        $data = $form->toArray();
        if (empty($data['userId'])) {
            $houses = [0];
        }
        else {
            $houses = $this->getService('Roles')->getHouseAccess($user = $this->getService('Users')->get($data['userId']));
        }
        $results = $forum->get($houses, $data['startDate'], $data['endDate'], empty($data['page']) ? 1 : $data['page']);
        if (empty($data['parent_id'])) {
            // populate the threads
            foreach ($results as $key => $item) {
                $results->setItemKey($key, 'threads', $this->getService()->getNew('Forum')->get([$item['house_id']], null, null, 1, $item['lead_id']));
            }
        }
        // at this point can either send data in json for the frontend to render or deliver the template from here
        $view = new Views\Forum($results); //@note $results is the feedback collection object
        $content = $view->getContent();
        $this->sendResponse(['count' => $forum->getResultCount(), 'pageCount' => $forum->getPaginator()->last, 'page' => $data['page'], 'html' => $content]);
        return true;
    }

    public function checkApiCreds()
    {
        if (!isset($_GET['api_key']) || $_GET['api_key'] !== $this->apiKey) {
            $this->errors[] = 'Invalid API Key';
            return false;
        }
        if (!isset($_GET['expires']) || !ctype_digit((string) $_GET['expires'])) {
            $this->errors[] = 'Invalid Expiration';
            return false;
        }
        //@note the route here is not using the leading or trailing slash, so strip the leading and trailing slashes
        $route = trim($this->services->get('Request')->getPathInfo(),  '/');
        if (!isset($_GET['signature'])
          || $_GET['signature'] !== ($sig = $this->getSignature($raw = sprintf('%s:%s:%s:%s', $this->apiKey, 'POST', $route, $_GET['expires'])))) {
            $this->errors[] = 'Signature Mismatch';
            return false;
        }
        return true;
    }

    public function handleBadRoute()
    {
        $this->errors[] = 'Invalid Service Request';
        $this->sendErrorResponse();
        return true;
    }

    public function mapInputFields($data, $fields)
    {
        $map = [];
        foreach ($fields as $key => $value) {
            $map[$key] = (isset($data[$value['key']])) ? $data[$value['key']] : '';
        }
        return $map;
    }

    public function getLastError()
    {
        return empty($this->errors) ? false : end($this->errors);
    }

    public function getService($name = null)
    {
        if (!isset($this->apiServices)) {
            $this->apiServices = new ApiServices($this->services);//@note pass the entire container, may make sense to eventually make this class the service container 
        }
        if (isset($name)) {
            return $this->apiServices->get($name);
        }
        return $this->apiServices;
    }

    public function sendTestMail()
    {
        $mailer = $this->services->get('Swift');
        $message = \Swift_Message::newInstance()
            ->setSubject('Campus Cooks Notification')
            ->setFrom('app@campuscooks.com')
            ->setReplyTo('app@campuscooks.com')
            ->setBody('A report has been included with this message. Please view this message in an email client that supports HTML rendering.')
            ->addPart('<p><strong>Test</strong> the transport for new messages</p>', 'text/html');
        //$message->getHeaders()->addTextHeader('X-Mailer', sprintf('X-Mailer: PHP/%s', phpversion()));
        $result = $mailer->send($message->setTo(['schuyler@gatordev.com' => 'Schuyler']));
        $this->sendResponse(['success' => $result]);
    }

    protected function validateSignature($signature, $content)
    {
        $hash = base64_encode(hash_hmac('sha256', $content, $this->hmacKey));
        return $hash === $signature;
    }

    protected function getSignature($string)
    {
        return base64_encode(hash_hmac('sha1', $string, $this->privateKey, true));
    }

    protected function sendResponse($content = null, $status = 200)
    {
        if ($hasContent = isset($content)) {
            $content['status'] = $status;
        }
        $this->services->get('Response')->setContentType('application/json')->setContent($hasContent ? json_encode($content) : '')->setStatusCode($status)->send();
    }

    protected function sendResponseObjects($data, $status = 200)
    {
        $this->services->get('Response')->setContentType('application/json')->setContent(json_encode(array_values($data)))->setStatusCode($status)->send();//\JSON_FORCE_OBJECT
    }

    protected function sendErrorResponse()
    {
        //@note the app doesn't implement the field name so just pass a generic one with the string message
        $content = ['status' => 200, 'response' => ['is_valid' => false, 'validation_messages' => ['fieldname' => $this->getLastError()]]];
        $this->services->get('Response')->setContentType('application/json')->setContent(json_encode($content))->send();
    }

    protected function getController($name)
    {
        // var_dump(House::getLateplateFields());
        $name = '\\CampusCooks\\Controllers\\' . $name;
        try{
            $controller = new $name($this->getService());
        }
        catch (\Exception $e) {
            return false;
        }
        return $controller;
    }
}
