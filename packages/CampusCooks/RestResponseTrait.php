<?php
/**
 * Campus Cooks API
 */
namespace CampusCooks;

trait RestResponseTrait
{
    public function sendResponse($content = null, $status = 200)
    {
        if ($hasContent = isset($content)) {
            $content['status'] = $status;
        }
        $this->services->get('Services')->get('Response')->setContentType('application/json')->setContent($hasContent ? json_encode($content) : '')->setStatusCode($status)->send();
    }

    public function sendResponseObjects($data, $status = 200)
    {
        $this->services->get('Services')->get('Response')->setContentType('application/json')->setContent(json_encode(array_values($data)))->setStatusCode($status)->send();//\JSON_FORCE_OBJECT
    }

    public function sendErrorResponse()
    {
        //@note the app doesn't implement the field name so just pass a generic one with the string message
        $content = ['status' => 200, 'response' => ['is_valid' => false, 'validation_messages' => ['fieldname' => $this->getLastError()]]];
        $this->services->get('Services')->get('Response')->setContentType('application/json')->setContent(json_encode($content))->send();
    }

    public function sendTextResponse($text)
    {
        $this->services->get('Services')->get('Response')->setContentType('text/html')->setContent(json_encode($text))->send();
    }

    public function getLastError()
    {
        return empty($this->errors) ? false : end($this->errors);
    }

    public function mapFormFields($data, $fields)
    {
        $map = [];
        foreach ($fields as $key => $value) {
            $map[$key] = (isset($data[$value['key']])) ? $data[$value['key']] : '';
        }
        return $map;
    }
}
