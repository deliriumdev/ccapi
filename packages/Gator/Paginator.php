<?php
/**
 * Paginator
 * 
 * Copyright(c) 2012 Schuyler Langdon
 *      
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * any later version.
 *      
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */
namespace Gator;

class Paginator
{
	public $count = 0;
	public $items = 0;
	public $last = 1;
	public $row_count = 0;
	public $offset = 0;
	public $page = 1;
	public $pageValid = true;
	public $next = 0;
	public $prev = 0;
	public $start = 1;
	public $finish = 1;
	public $range = '';

/**
 * Constructor of class Paginator.
 *
 * @return void
 */
	public function __construct($items_per_page = 0, $count = 0)
	{
		$this->items = (int) $items_per_page;
        $this->count = (int) $count;
		/*if (empty($this->items)) {
			throw new \InvalidArgumentException('Pagination Error: items per page cannot be empty');
		}*/
        if (0 !== $this->items && 0 !== $this->count) {
            $this->last = (int) ceil($this->count/$this->items);
        }
	}
	
	public function paginate($page = '1')
    {
		//reset
		$this->page_invalid = false;
		$this->row_count = $this->items;
		//set
		$this->page = (int) $page;
		if($this->page > $this->last){//this won't work
			$this->page = $this->last;
			$this->pageValid = false;//in case you'd want to redirect
		}
		//the last page may have less that ipp items
		if($this->last === $this->page && 0 < ($mod = $this->count % $this->items)){
			$this->row_count = $mod;
		}
		//some view stuff
		$this->prev = 1 === $this->page ? 0 : $this->page - 1;
		$this->next = $this->page === $this->last ? 0 : $this->page + 1;
		//some db stuff
		$this->offset = ($this->page - 1) * $this->items;
		$this->start = $this->offset + 1;
		$this->finish = $this->offset + $this->row_count;
		$this->range = $this->start.' - ' . $this->finish;
		return ' limit ' . $this->offset . ',' . $this->row_count;
	}

    public function setCount($count)
    {
        $this->count = $count;
        if (0 !== $this->items) {
            $this->last = (int) ceil($this->count/$this->items);
        }
        return $this;
    }

    public function setItemsPerPage($items)
    {
        $this->items = (int) $items;
        return $this;
    }
}
