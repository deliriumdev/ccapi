<?php
/**
 * PDO wrapper class
 * Copyright(c) 2012-2014 Schuyler Langdon
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */
namespace Gator;

class Pdo
{
    private $dbh;
    protected $rows_affected = 0;
    protected $config;
    protected $logger;
    protected $err;

    public function __construct($config, $logger = null)
    {
        $this->config = ($config instanceof Gator_Config) ? $config->toArray() : $config;
        $this->logger = isset($logger) ? $logger : new NullLogger();
        $this->dbh = $this->connect();
        if ($this->dbh) {
            $this->dbh->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        }
    }

    public function __get($key)
    {
        if ('err' === $key) {
            return $this->err;
        }
        return null;
    }

    public function __destruct()
    {
        $this->dbh=null;
    }

    public function __call($func, $args)
    {
        if ('do' === $func) {
            //reserved keyword
            return $this->execute($args[0], $args[1]);
        } else {
            throw new BadMethodCallException(sprintf('Method "%s" not found.', $func));
        }
    }

/**
 * get params
 *
 * @return array with re-keyed pdo param style keys, ie ":key"
 *
 * @param $data array of keyed values
 */

    public function isConn()
    {
        if (!isset($this->dbh)) {
            return false;
        }
        return true;
    }

    public function quote($str)
    {
        return $this->dbh->quote(trim($str));
    }

    public function escape($str)
    {
        //just the string w/o quotes
        return preg_replace("/^'|'$/", "", $this->dbh->quote($str));
    }

/**
 * get count
 *
 * Gets count of query without returning all data or modifying query,
 * works on the notion that fetchcolumn will ignore additional select params
 *
 * @paramater string $key Should contain table name if applicable
 */
    public function get_count($sql, array $params=null, $key=null)
    {
        if (!isset($key)) {
            $key = '*';
        }
        return $this->get_column(preg_replace('~^select~i', 'select count(' . $key . '), ', $sql), $params);
    }

    public function get_column($sql, array $params = null)
    {
        $sth = $this->do_query($sql, $params);
        if (empty($sth)) {
            return false;
        }
        try {
            $ret=$sth->fetchColumn();//returns false when there are no rows
        } catch (\PDOException $e) {
            $this->logger->error(sprintf('PDO fetch error: %s', $this->err = $e->getMessage()));
            $ret = null;
        }
        $sth = null;
        return $ret;
    }
/**
 * get_row
 *
 * @returns array single row of data
 */
    public function get_row($sql, array $params = null, $fetch = \PDO::FETCH_ASSOC)
    {
        $sth=$this->do_query($sql, $params);
        if (empty($sth)) {
            return null;
        }
        try { 
            $ret = $sth->fetch($fetch);
        } catch (\PDOException $e){
            $this->logger->error(sprintf('PDO fetch error: %s', $this->err = $e->getMessage()));
            $ret = null;
        }
        $sth = null;
        return $ret;
    }

/**
 * Returns an array (stack) of single values instead of array of array
 */
    public function get_stack($sql, array $params = null, $key = false, $fetch = \PDO::FETCH_NUM, $n=0)
    {
        $sth=$this->do_query($sql, $params);
        if (empty($sth)) {
            return null;
        }
        try {
            $ret = array();
            if (\PDO::FETCH_ASSOC === $fetch) {
                //keyed stack
                while ($row=$sth->fetch($fetch)) {
                    $key = key($row);
                    $ret[$key]=$row[$key];
                }
            } else {
                while ($col = $sth->fetchColumn($n)) {
                    false === $key ? $ret[] = $col : $ret[$col] = $col;
                }
            }
        } catch (\PDOException $e) {
            $this->logger->error(sprintf('PDO fetch error: %s', $this->err = $e->getMessage()));
            unset($ret);
        }
        $sth = null;
        return $ret;
    }

    public function get_array($sql, array $params = null, $key = false, $stack = false, $fetch = \PDO::FETCH_ASSOC)
    {
        $sth = $this->do_query($sql, $params);
        if (empty($sth)) {
            return null;
        }
        try {
            $ret = array();
            while ($row = $sth->fetch($fetch)) {
                if (false === $key) {
                    $ret[] = $row;
                } else {
                    $stack ? $ret[$row[$key]][] = $row : $ret[$row[$key]] = $row;
                }
            }
        } catch (\PDOException $e) {
            $this->logger->error(sprintf('PDO fetch error: %s', $this->err = $e->getMessage()));
            unset($ret);
        }
        $sth = null;
        return $ret;
    }

    public function get_object($sql, array $params=null, $class=null, array $args=null)
    {
        $sth = $this->do_query($sql, $params);
        if (!$sth) {
            return null;
        }
        $ret = (empty($class) || 'stdClass' === $class) ? $sth->fetch(\PDO::FETCH_OBJ) : $sth->fetch(\PDO::FETCH_CLASS, $class);

    //doesn't work on this version php || pdo
    //$ret = $sth->fetchObject($class);

    $sth = null;
        return ($ret === false) ? null : $ret;
    }

    public function get_collection($sql, array $params=null, $key=null, $class=null, $callback = null)
    {
        $sth = $this->do_query($sql, $params);
        if (!$sth) {
            return null;
        }
        $ret = array();
        if (isset($callback) && $callback instanceof Closure) {
            while ($row = $sth->fetch(\PDO::FETCH_ASSOC)) {
                if (isset($key)) {
                    $ret[$row[$key]] = $callback($row);
                } else {
                    $ret[] = $callback($row);
                }
            }
        }
    //again fetchObject doesn't work this version php || pdo
    elseif (empty($class) || 'stdClass' === $class) {
        while ($obj = $sth->fetch(\PDO::FETCH_OBJ)) {
            if (isset($key)) {
                $ret[$obj->{$key}] = $obj;
            } else {
                $ret[] = $obj;
            }
        }
    } else {
        $opts = \PDO::FETCH_CLASS;
        // this just calls the constructor before setting props, does not pass row to constructor
        /*if($late){
            $opts = $opts | \PDO::FETCH_PROPS_LATE;
        }*/
        $sth->setFetchMode($opts, $class);
        while ($obj = $sth->fetch()) {
            if (isset($key)) {
                $ret[$obj->{$key}] = $obj;
            } else {
                $ret[] = $obj;
            }
        }
    }
        
        $sth = null;
        return $ret;
    }

    public function version()
    {
        return $this->dbh->getAttribute(\PDO::ATTR_CLIENT_VERSION);
    }

    public function rowCount()
    {
        return $this->rows_affected;
    }

    public function execute($sql, array $params=null)
    {
        if (!isset($params)) {
            try {
                $this->dbh->exec($sql);
            } catch (\PDOException $e) {
                $this->logger->error(sprintf('PDO execute error: %s', $this->err = $e->getMessage()));
                return false;
            }
            return true;
        }
        $sth = $this->do_query($sql, $params);
        if (empty($sth)) {
            return false;
        }
        $id = $this->dbh->lastInsertId();
        $this->rows_affected = $sth->rowCount();
        $ret = empty($id) ? true : $id;
        $sth = null;
        return $ret;
    }

    private function setMode($sth, $obj=null, $mode=\PDO::FETCH_INTO)
    {
        try {
            //should be the following, but it will probably only be used for fetch into
        //(isset($obj)) ? ($sth->setFetchMode($mode, $obj)) : $sth->setFetchMode($mode);
        $sth->setFetchMode($mode, $obj);
        } catch (\PDOException $e) {
            $this->logger->error(sprintf('PDO execute error: %s', $this->err = $e->getMessage()));
            return false;
        }
        return true;
    }

    private function connect()
    {
        $cn_str = 'mysql:host=' . $this->config['db_host'] . ';dbname='. $this->config['db_name'];// . ';charset=utf8';
        try {
            $dbh = new \PDO($cn_str, $this->config['db_user'], $this->config['db_pass'], array(\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        } catch (\PDOException $e) {
            $this->logger->emergency(sprintf('Db connection error: %s', $this->err = $e->getMessage()));
            $dbh = null;
        }
        return $dbh;
    }

/**
 * do_query
 *
 * Does the actual query and returns statement handle object
 */
    private function do_query($sql, array $params=null)
    {
        //reset info
        $this->rows_affected=0;
        $this->err = '';
        if (empty($params)) {
            try {
                $sth=$this->dbh->query($sql);
            } catch (\PDOException $e) {
                $this->logger->error(sprintf('PDO query error [%s] on query [%s]', $this->err = $e->getMessage(), $sql));
                /*$err=$this->dbh->errorInfo();
                $this->logger->error(var_export($err));*/
                return null;
            }
        } else {
            try {
                $sth = $this->dbh->prepare($sql);
            } catch (\PDOException $e) {
                $this->logger->error(sprintf('PDO prepare error [%s] on query [%s]', $this->err = $e->getMessage(), $sql));
                return null;
            }
            try {
                $sth->execute($params);
            } catch (\PDOException $e) {
                $this->logger->error(sprintf('PDO execute error [%s] on query [%s]', $this->err = $e->getMessage(), $sql));
                // $this->logger->error(sprintf('params: %s', print_r($params, true));
                return null;
            }
        }
        //$sth->errorInfo();
        $this->rows_affected = $sth->rowCount();
        return $sth;
    }
}

class NullLogger
{
    public function error($message, $context = null)
    {
        return;
    }

    public function emergency($message, $context = null)
    {
        return;
    }
}
