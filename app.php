<?php
/**
 * Campus Cooks App
 */

use Reo\Autoload\Autoloader;
use Reo\Form\FormHandler;
use Reo\DependencyInjection\ContainerEventAware;
use Reo\Routing\Route;
use Reo\Routing\RouteCollection;
use Reo\Routing\Router;
use Reo\Routing\Matcher\UrlMatcher;
use Reo\Http\Request;
use Reo\Http\Response;
use Reo\Events\EventDispatcher;
use Zend\Authentication\AuthenticationService;
use Zend\Config\Config;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Formatter\LineFormatter;

class CampusCooksApp
{
    protected $services;
    protected $responseSent = false;
    const AUTOLOADER_NAME = 'Autoloader';

    public function __construct(Autoloader $loader)
    {
        $this->services = new ContainerEventAware(array(self::AUTOLOADER_NAME => array(null, true, $loader)), array('libPaths' => $loader->getPath(), 'autoloaderName' => self::AUTOLOADER_NAME));
        $this->services->set(self::AUTOLOADER_NAME, $loader);
        $this->registerCore();
        $this->services->listen('route.matched', 'Resolver');
    }

    protected function registerCore()
    {
        $this->services->registerShared([
        'Request' => function($c)
        {
            $request =  Request::createFromGlobals();
            // $request->trustProxyData();
            return $request;
        },

        'Response' => function($c)
        {
            $response = new Response();
            return $response->prepare($c->get('Request'));
        },

        'RouteCollection' => function($c)
        {
            return new RouteCollection();
        },

        'Matcher' => function($c)
        {
            return new UrlMatcher($c->get('RouteCollection'));
        },

        'Router' => function($c)
        {
            return new Router($c->get('RouteCollection'));
        },

        'Events' => function($c)
        {
            return new EventDispatcher();
        },

        'Resolver' => function($c, $id, $event)
        {
            //echo "Do Resolver\n";
            $params = $event->get('params');
            //$params['service'] = $c;
            if ($event->get('route')->hasCallback()) {
                if (true === ($response = $event->get('route')->doCallback(array($params)))) {//@note set to array so data is passed as array when invoked
                    $this->services->get('Events')->trigger('response.sent');
                    $this->responseSent = true;
                    $this->stop();
                    //the route callback should be responsible for the Response
                    //$c->get('Response')->setContent($output);
                }
                elseif ($response instanceof \Reo\Routing\Route) {
                    // $eventDispatcher->trigger('route.forwarded');
                    $eventDispatcher->trigger('route.matched', array_merge($response, array('chainable' => false)));
                }
            }
        },

        'FormHandler' => function($c, $id, $args)
        {
            return new FormHandler(new \Reo\Validation\Validate());
        },

        'Config' => function($c, $id, $args)
        {
            return new Config($args);
        },

        'Swift' => function($c, $id)
        {
            //swift has it's own autoloader
            $c->get(CampusCooksApp::AUTOLOADER_NAME)->register('Swift', false);
            require_once $c->getParamKey('libPaths', 'lib') . '/swiftmailer/lib/swift_required.php';
            $config = $c->get('Config')->mailer;
            if (isset($config->server)) {
                // echo "SMTP\n", $config->server, "\n", $config->user, "\n", $config->pass, "\n";  
                $transport = Swift_SmtpTransport::newInstance($config->server, 25, 'tls')
                    ->setUsername($config->user)
                    ->setPassword($config->pass)
                    ->setStreamOptions(array('ssl' => array('allow_self_signed' => true, 'verify_peer' => false)));
                return Swift_Mailer::newInstance($transport);
            }
            // fallback to sendmail
            return Swift_Mailer::newInstance(Swift_SendmailTransport::newInstance('/usr/sbin/sendmail -bs'));
            return Swift_Mailer::newInstance('localhost' === $config->transport ? Swift_SmtpTransport::newInstance('localhost', 25) : Swift_NullTransport::newInstance());
        },

        'Logger' => function($c, $id, $args)
        {
            $config = $c->get('Config')->logger;
            $autoloader = $c->get('Autoloader');
            //register psr log, psr-4 autoload
            $autoloader->register('Psr', $autoloader->getPath('lib') . '/log/Psr', true);
            //register monolog, may work with psr-2
            //$autoloader->register('Monolog', $dir . '/lib/Monolog', true);
            $logger = new Logger('cc-api');
            $handler = new StreamHandler(isset($args['logDir']) ? $args['logDir'] : $config->logfile, Logger::INFO);
            // ignore empty context and extra ($ignoreEmptyContextAndExtra = true)
            $handler->setFormatter(new LineFormatter(null, null, false, true));
            $logger->pushHandler($handler);
            return $logger;
        },
        ]);
    }

    public function start()
    {
        $this->services->setEventDispatcher('Events');
        $eventDispatcher = $this->services->get('Events');
        $eventDispatcher->trigger('route.before');
        $request = $this->services->get('Request');
        $context = array('method' => $request->getMethod(), 'protocol' => $request->isSecure() ? 'https' : 'http');
        if (false !== ($match = $this->services->getMatcher()->setContext($context)->match($request->getPathInfo()))) {
            //echo "Route matched\n";
            //var_dump($match);
            //to be chainable the dispatcher doesn't need to be passed since it's a part of the service container

            // var_dump($this->services->getSubscribed('route.matched'), $this->services->get('Events'));
            $eventDispatcher->trigger('route.matched', array_merge($match, array('chainable' => false)));
        }
        elseif (true === $this->services->get('Events')->trigger('route.nomatch', $match)) {
            $this->responseSent = true;
            $this->stop();
        }
        if (!$this->responseSent) {
            $this->services->get('Events')->trigger('response.ready', $response = $this->services->get('Response'));
            $response->setContent('')->setStatusCode(400)->send();
            $this->stop();
        }
    }

    public function getServices()
    {
        return $this->services;
    }

    public function registerPackage($id, $namespace = '', $autoloadPath = null, \Closure $callback, $shared = true, array $subscriptions = null, array $deps = null)
    {
        if (isset($autoloadPath) && !empty($namespace)) {
            //var_dump($namespace, $autoloadPath);
            $this->services->get(self::AUTOLOADER_NAME)->register($namespace, $autoloadPath, true);
        }
        $this->services->register($id, $callback, $shared, $deps);
        if (isset($subscriptions)) {
            $this->services->subscribe(array_map(//map the package name
                function($event) use(&$id){
                    $eventStack = (array)$event;
                    if($event === $eventStack[0]){//its a string jim
                        array_unshift($eventStack, $id);
                        return array($eventStack);
                    }

                    return array_map(function($event) use(&$id){
                        $event = (array)$event;
                        array_unshift($event, $id);
                        return $event;
                    }, $event);
                },
            $subscriptions));
        }
    }

    public function __get($name)
    {
        if ('services' === $name) {
            return $this->services;
        }
        return null;
    }

    protected function stop()
    {
        $this->responseComplete = true;
    }
}
